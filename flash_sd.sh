#!/bin/bash
#
# Flash de uSD with the images for the SmartCam
#
# $1 : If different than zero, the uboot environment will be flashed

if ! [ -e "/dev/sda2" ]; then
	printf "uSD not found!\n"
	exit 1
fi

cd out/v459-smartcam/image/
sudo dd if=boot0_sdcard.fex of=/dev/sda bs=1k seek=8
sudo dd if=boot_package.fex of=/dev/sda bs=1k seek=16400

if [[ $1 -ne 0 ]]; then
	printf "Flashing u-boot environment...\n"
	sudo dd if=env.fex of=/dev/sda5 bs=512
else
	printf "Not flashing u-boot environment...\n"
fi

sudo dd if=boot.fex of=/dev/sda1 bs=512
sudo dd if=rootfs.fex of=/dev/sda2 bs=512
sudo dd if=usr.fex of=/dev/sda3 bs=512
cd -
