/*
 * Copyright (c) 2008-2016 Allwinner Technology Co. Ltd.
 * All rights reserved.
 *
 * File : cdx_plugin.h
 * Description : cdx_plugin
 * History :
 *
 */

#ifndef CDX_PLUGIN_MANAGER_H
#define CDX_PLUGIN_MANAGER_H

#ifdef __cplusplus
extern "C"
{
#endif

void CdxPluginInit(void);

int CdxTestLib(int a);

#ifdef __cplusplus
}
#endif

#endif
