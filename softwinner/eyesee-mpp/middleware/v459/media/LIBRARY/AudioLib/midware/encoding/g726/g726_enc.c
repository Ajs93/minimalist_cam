#define LOG_TAG "g711u_enc.c"

#include <stdlib.h>
#include <string.h>
#include "aenc_sw_lib.h"
#include "alib_log.h"
#include "g726.h" 

#define ENCODEEND   (ERR_AUDIO_ENC_ABSEND)
#define ERROR       (ERR_AUDIO_ENC_UNKNOWN)
#define NOENOUGHPCM (ERR_AUDIO_ENC_PCMUNDERFLOW)
#define SUCCESS     (ERR_AUDIO_ENC_NONE)

int G726EncInit(struct __AudioENC_AC320 *p)
{
    g726_state_t *g_state726_16 = NULL; //for g726_16 
    g_state726_16 = (g726_state_t *)malloc(sizeof(g726_state_t));
	g_state726_16 = g726_init(g_state726_16, 8000*2);

    p->EncoderCom->pEncInfoSet = (unsigned int *)g_state726_16;
	return 0;
} 

int G726EncExit(struct __AudioENC_AC320 *p)
{
    if(p->EncoderCom->pEncInfoSet)
	{
		free(p->EncoderCom->pEncInfoSet);
		p->EncoderCom->pEncInfoSet = 0;
	}
	return 0;
}

int G726FrameEnc(struct __AudioENC_AC320 *p,/*BsInFor *bsinfo,Ac320FileRead *FileInfo,*/ char *OutBuffer,int *OutBuffLen )
{ 
 	int	retVal 	 = 0;
 	int read_sum = 0;
    unsigned char InBuffer[640];
    int iRet = 0;

    alib_loge("g726_enc");
	*OutBuffLen  = 0;
	//init static data;
	if(!p->Encinitedflag)
	{
		if(((p->AudioBsEncInf->SamplerBits != 8) & (p->AudioBsEncInf->SamplerBits != 16)
			& (p->AudioBsEncInf->SamplerBits != 24)& (p->AudioBsEncInf->SamplerBits != 32)) | 
			 ((p->AudioBsEncInf->InChan < 0) |(p->AudioBsEncInf->InChan > 6)))return ERROR; 

		p->Encinitedflag = 1;
	}
	
	//flash time
     p->EncoderCom->ulNowTimeMS = (unsigned int)((double)p->EncoderCom->framecount * MAXDECODESAMPLE *1000 /(double)(p->AudioBsEncInf->InSamplerate));

	read_sum = GetPcmDataSize(p->pPcmBufManager);
	if(read_sum<MAXDECODESAMPLE * (p->AudioBsEncInf->SamplerBits>>3)*p->AudioBsEncInf->InChan)
	{ 
		if(p->EncoderCom->ulEncCom == 5)
		{
			return ENCODEEND;//stop
		}
		else
		{
			return NOENOUGHPCM;//wait enough data	
		}
	}
    read_sum = ReadPcmDataForEnc((void *)InBuffer,MAXDECODESAMPLE * (p->AudioBsEncInf->SamplerBits>>3)*p->AudioBsEncInf->InChan,p->pPcmBufManager);	

    iRet = g726_encode(p->EncoderCom->pEncInfoSet, OutBuffer, (short*)InBuffer, read_sum/2);
	*OutBuffLen =iRet;

	p->EncoderCom->framecount++;
		
	return retVal; 
} 

struct __AudioENC_AC320 *AudioG726EncInit(void)
{
	struct  __AudioENC_AC320  *AEncAC320 ;

	AEncAC320 = (struct __AudioENC_AC320 *)malloc(sizeof(struct __AudioENC_AC320));
    if(!AEncAC320)
    		return 0;
    memset(AEncAC320, 0, sizeof(struct __AudioENC_AC320));
    
	AEncAC320->EncInit = G726EncInit;
	AEncAC320->EncFrame = G726FrameEnc;
	AEncAC320->EncExit = G726EncExit;

    alib_loge("g726_enc_init");

	return AEncAC320;
} 

int	AudioG726EncExit(struct __AudioENC_AC320 *p)
{ 
	free(p);
    
    alib_loge("g726_enc_exit");
	return 0;
}


