#! /bin/bash

#tips: this script is used to add new configuration and search path for new library to
# fix compile error.

add_string1='ifeq ($(MPPCFG_AGC),Y)\
LOCAL_STATIC_LIBS += \\\
    libAgc \
else ifeq ($(MPPCFG_AI_AGC),Y)\
LOCAL_STATIC_LIBS += \\\
    libAgc \
endif'

add_string2='$(PACKAGE_TOP)/media/LIBRARY/agc_lib/out \\'


dirs=`find ./ -name 'sample_*' -type d ! -name sample_ai ! -name sample_ao ! -name sample_ao2ai_aec ! -name sample_UVC`

for dir in $dirs
do
	echo processing dir:$dir
	sed -i -e '/MPPCFG_ANS/i\' -e "${add_string1}" ${dir}/tina.mk
	sed -i -e '/drc_lib\/out/a\' -e "    ${add_string2}" ${dir}/tina.mk

done

