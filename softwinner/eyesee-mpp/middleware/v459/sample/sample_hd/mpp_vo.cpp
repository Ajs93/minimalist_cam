#include "mpp_vo.h"


#if 1
static ERRORTYPE SampleVO_VOCallbackWrapper(void *cookie, MPP_CHN_S *pChn, MPP_EVENT_TYPE event, void *pEventData)
{
    ERRORTYPE ret = SUCCESS;
    SampleVOContext *pContext = (SampleVOContext*)cookie;
    if(MOD_ID_VOU == pChn->mModId) {
        //printf("VO callback: VO Layer[%d] chn[%d] event:%d", pChn->mDevId, pChn->mChnId, event);
        switch(event) {
        case MPP_EVENT_RELEASE_VIDEO_BUFFER: {
            VIDEO_FRAME_INFO_S *pFrameInfo = (VIDEO_FRAME_INFO_S*)pEventData;
            //printf("vo layer[%d] release frame id[0x%x]!\n", pChn->mDevId, pFrameInfo->mId);
            break;
        }
        case MPP_EVENT_SET_VIDEO_SIZE: {
            SIZE_S *pDisplaySize = (SIZE_S*)pEventData;
            //printf("vo layer[%d] report video display size[%dx%d]\n", pChn->mDevId, pDisplaySize->Width, pDisplaySize->Height);
            break;
        }
        case MPP_EVENT_RENDERING_START: {
            //printf("vo layer[%d] report rendering start\n", pChn->mDevId);
            break;
        }
        default: {
            //postEventFromNative(this, event, 0, 0, pEventData);
            //printf("fatal error! unknown event[0x%x] from channel[0x%x][0x%x][0x%x]!\n", event, pChn->mModId, pChn->mDevId, pChn->mChnId);
            ret = ERR_VO_ILLEGAL_PARAM;
            break;
        }
        }
    } else {
        printf("fatal error! why modId[0x%x]?\n", pChn->mModId);
        ret = FAILURE;
    }
    return ret;
}
#endif

int create_vo(VO_Params* pVOParams)
{
    printf("\n\***********\n");
    ERRORTYPE result = SUCCESS;
    ERRORTYPE eRet = SUCCESS;
    /* enable vo layer */
    int hlay0 = 0;
    int hwDispChn = 0;
    pVOParams->mVoDev = 0;
    AW_MPI_VO_Enable(pVOParams->mVoDev);
    //AW_MPI_VO_AddOutsideVideoLayer(pVOParams->mUILayer);
    //AW_MPI_VO_CloseVideoLayer(pVOParams->mUILayer); /* close ui layer. */
    if (!strcmp(pVOParams->pStrDispType, "hdmi")) {
        pVOParams->mDispType = VO_INTF_HDMI;
        if (pVOParams->mDisplayWidth > 1920)
            pVOParams->mDispSync = VO_OUTPUT_3840x2160_30;
        else if (pVOParams->mDisplayWidth > 1280)
            pVOParams->mDispSync = VO_OUTPUT_1080P30;
        else
            pVOParams->mDispSync = VO_OUTPUT_720P60;
    } else if (!strcmp(pVOParams->pStrDispType, "lcd")) {
        pVOParams->mDispType = VO_INTF_LCD;
        pVOParams->mDispSync = VO_OUTPUT_NTSC;
    } else if (!strcmp(pVOParams->pStrDispType, "cvbs")) {
        pVOParams->mDispType = VO_INTF_CVBS;
        pVOParams->mDispSync = VO_OUTPUT_NTSC;
    }
    VO_PUB_ATTR_S spPubAttr;
    AW_MPI_VO_GetPubAttr(pVOParams->mVoDev, &spPubAttr);
    spPubAttr.enIntfType = pVOParams->mDispType;
    spPubAttr.enIntfSync = pVOParams->mDispSync;
    AW_MPI_VO_SetPubAttr(pVOParams->mVoDev, &spPubAttr);

    while(hlay0 < VO_MAX_LAYER_NUM) {
        if(SUCCESS == AW_MPI_VO_EnableVideoLayer(hlay0)) {
            break;
        }
        hwDispChn++;
        hlay0 = HLAY(hwDispChn, 0);
    }
    if(hlay0 >= VO_MAX_LAYER_NUM) {
        printf("fatal error! enable video layer fail!");
    }

    pVOParams->mVoLayer = hlay0;
    AW_MPI_VO_GetVideoLayerAttr(pVOParams->mVoLayer, &pVOParams->mLayerAttr);
    pVOParams->mLayerAttr.stDispRect.X = pVOParams->mDisplayX;
    pVOParams->mLayerAttr.stDispRect.Y = pVOParams->mDisplayY;
    pVOParams->mLayerAttr.stDispRect.Width = pVOParams->mDisplayWidth;
    pVOParams->mLayerAttr.stDispRect.Height = pVOParams->mDisplayHeight;
    AW_MPI_VO_SetVideoLayerAttr(pVOParams->mVoLayer, &pVOParams->mLayerAttr);

    /* create vo channel and clock channel.
    (because frame information has 'pts', there is no need clock channel now)
    */
    BOOL bSuccessFlag = FALSE;
    pVOParams->mVoChn = 0;
    while(pVOParams->mVoChn < VO_MAX_CHN_NUM) {
        eRet = AW_MPI_VO_EnableChn(pVOParams->mVoLayer, pVOParams->mVoChn);
        if(SUCCESS == eRet) {
            bSuccessFlag = TRUE;
            printf("create vo channel[%d] success!", pVOParams->mVoChn);
            break;
        } else if(ERR_VO_CHN_NOT_DISABLE == eRet) {
            printf("vo channel[%d] is exist, find next!", pVOParams->mVoChn);
            pVOParams->mVoChn++;
        } else {
            printf("fatal error! create vo channel[%d] ret[0x%x]!", pVOParams->mVoChn, eRet);
            break;
        }
    }
    if(FALSE == bSuccessFlag) {
        pVOParams->mVoChn = MM_INVALID_CHN;
        printf("fatal error! create vo channel fail!");
        result = FAILURE;
        goto _err1;
    }
    MPPCallbackInfo cbInfo;
    cbInfo.cookie = (void*)&pVOParams->mVoContext;
    cbInfo.callback = (MPPCallbackFuncType)&SampleVO_VOCallbackWrapper;
    AW_MPI_VO_RegisterCallback(pVOParams->mVoLayer, pVOParams->mVoChn, &cbInfo);

    AW_MPI_VO_SetChnDispBufNum(pVOParams->mVoLayer, pVOParams->mVoChn, 2);

    AW_MPI_VO_StartChn(pVOParams->mVoLayer, pVOParams->mVoChn);
    return result;

_err1:
    eRet = AW_MPI_VO_DisableVideoLayer(pVOParams->mVoLayer);
    if(eRet != SUCCESS) {
        printf("fatal error! VO DisableVideoLayer fail!");
    }
_err0:
    return result;
}

int destroy_vo(VO_Params* pVOParams)
{
    AW_MPI_VO_DisableVideoLayer(pVOParams->mVoLayer);
    AW_MPI_VO_StopChn(pVOParams->mVoLayer, pVOParams->mVoChn);
    AW_MPI_VO_DisableChn(pVOParams->mVoLayer, pVOParams->mVoChn);
    AW_MPI_VO_Disable(pVOParams->mVoDev);
    return 0;
}


static int fillColorToRGBFrame(VIDEO_FRAME_INFO_S *pFrameInfo)
{
    if(pFrameInfo->VFrame.mPixelFormat != MM_PIXEL_FORMAT_RGB_8888) {
        printf("fatal error! pixelFormat[0x%x] is not argb8888!");
        return -1;
    }
    int nFrameBufLen = pFrameInfo->VFrame.mStride[0];
    //memset(pFrameInfo->VFrame.mpVirAddr[0], 0xFF, nFrameBufLen);

    int *add = (int *)pFrameInfo->VFrame.mpVirAddr[0];
    int i;
    for(i=0; i<nFrameBufLen/4; i++) {
        if(i<nFrameBufLen/8) {
            *add = 0x5fff0000;
        } else {
            *add = 0x5f0000ff;
        }
        add++;
    }
    return 0;
}

int PrepareTestFrame4VO(VO_Params* pVOParams, VIDEO_FRAME_INFO_S *pFrameInfo)
{
    PIXEL_FORMAT_E ePixelFormat = MM_PIXEL_FORMAT_RGB_8888;
    unsigned int nPhyAddr = 0;
    void *pVirtAddr = NULL;
    unsigned int nFrameBufLen = 0;
    switch(ePixelFormat) {
    case MM_PIXEL_FORMAT_RGB_8888:
        nFrameBufLen = pVOParams->mDisplayWidth * pVOParams->mDisplayHeight*4;
        break;
    case MM_PIXEL_FORMAT_RGB_1555:
    case MM_PIXEL_FORMAT_RGB_565:
        nFrameBufLen = pVOParams->mDisplayWidth * pVOParams->mDisplayHeight*2;
        break;
    default:
        printf("fatal error! unsupport pixel format:0x%x", ePixelFormat);
        break;
    }

    ERRORTYPE ret = AW_MPI_SYS_MmzAlloc_Cached(&nPhyAddr, &pVirtAddr, nFrameBufLen);
    if(ret!=SUCCESS) {
        printf("fatal error! ion malloc fail:0x%x",ret);
        return -1;
    }

    pFrameInfo->mId = 0x21;
    pFrameInfo->VFrame.mWidth = pVOParams->mDisplayWidth;
    pFrameInfo->VFrame.mHeight = pVOParams->mDisplayHeight;
    pFrameInfo->VFrame.mField = VIDEO_FIELD_FRAME;
    pFrameInfo->VFrame.mPixelFormat = ePixelFormat;
    pFrameInfo->VFrame.mPhyAddr[0] = nPhyAddr;
    pFrameInfo->VFrame.mpVirAddr[0] = pVirtAddr;
    pFrameInfo->VFrame.mStride[0] = nFrameBufLen;
    pFrameInfo->VFrame.mOffsetTop = 0;
    pFrameInfo->VFrame.mOffsetBottom = pFrameInfo->VFrame.mHeight;
    pFrameInfo->VFrame.mOffsetLeft = 0;
    pFrameInfo->VFrame.mOffsetRight = pFrameInfo->VFrame.mWidth;

    //fill color in frame.
    fillColorToRGBFrame(pFrameInfo);
}


int MallocVideoFrame(VIDEO_FRAME_INFO_S *pFrameInfo, PIXEL_FORMAT_E ePixelFormat, int width, int height)
{
    unsigned int nPhyAddr = 0;
    void *pVirtAddr = NULL;
    unsigned int nFrameBufLen = 0;

    switch(ePixelFormat) {
    case MM_PIXEL_FORMAT_RGB_8888:
        nFrameBufLen = width*height*4;
        break;
    case MM_PIXEL_FORMAT_RGB_1555:
    case MM_PIXEL_FORMAT_RGB_565:
        nFrameBufLen = width*height*2;
        break;
    case MM_PIXEL_FORMAT_YUV_PLANAR_420:
    case MM_PIXEL_FORMAT_YVU_PLANAR_420:
    case MM_PIXEL_FORMAT_YUV_SEMIPLANAR_420:
    case MM_PIXEL_FORMAT_YVU_SEMIPLANAR_420:
        nFrameBufLen = width*height*3/2;
        break;
    default:
        printf("fatal error! unsupport pixel format:0x%x", ePixelFormat);
        nFrameBufLen = width*height;
        break;
    }

    ERRORTYPE ret = AW_MPI_SYS_MmzAlloc_Cached(&nPhyAddr, &pVirtAddr, nFrameBufLen);
    if(ret!=SUCCESS) {
        printf("fatal error! ion malloc fail:0x%x",ret);
        return -1;
    }

    pFrameInfo->mId = 0x21;
    pFrameInfo->VFrame.mWidth = width;
    pFrameInfo->VFrame.mHeight = height;
    //pFrameInfo->VFrame.mField = VIDEO_FIELD_TOP;
    pFrameInfo->VFrame.mField = VIDEO_FIELD_FRAME;
    pFrameInfo->VFrame.mPixelFormat = ePixelFormat;
    pFrameInfo->VFrame.mPhyAddr[0] = nPhyAddr;
    pFrameInfo->VFrame.mpVirAddr[0] = pVirtAddr;
    pFrameInfo->VFrame.mStride[0] = nFrameBufLen;
    pFrameInfo->VFrame.mOffsetTop = 0;
    pFrameInfo->VFrame.mOffsetBottom = pFrameInfo->VFrame.mHeight;
    pFrameInfo->VFrame.mOffsetLeft = 0;
    pFrameInfo->VFrame.mOffsetRight = pFrameInfo->VFrame.mWidth;
    return 0;
}

int FreeVideoFrame(VIDEO_FRAME_INFO_S *pFrameInfo)
{
    AW_MPI_SYS_MmzFree(pFrameInfo->VFrame.mPhyAddr[0], pFrameInfo->VFrame.mpVirAddr[0]);

    return 0;
}

int SendFrame2VO(VO_Params* pVOParams, VIDEO_FRAME_INFO_S *pFrameInfo, int timeout_ms)
{
    AW_MPI_VO_SendFrame(pVOParams->mVoLayer, pVOParams->mVoChn, pFrameInfo, timeout_ms);
    return 0;
}



