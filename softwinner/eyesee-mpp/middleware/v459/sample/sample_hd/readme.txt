2020-07-10
1、此次修改主要是增加了录像功能，保存为文件，文件名为nna_800_480_1.h264，nna_800_480_2.h264等，每次启动序列号加一。
2、请在/mnt/extsd目录下创建26x_file_count文件。
3、版本号为1.01.01，可以在LCD显示屏上显示。
4、sample_insi.conf文件中可以修改配置，默认resolution=0，是800x480的分辨率，如果把resolution设置为2，就是1920x1080的分辨率，
不知道是否有影响？这时保存的录像文件名称格式为nna_1920_1080_1.h264等。
5、确保两个板子的face_id和IQ参数相同。

2020-07-11
1、再次改进多人情况下的中间人脸的选择方法，判断更加准确，希望能解决中间识别框与周围框重合时出现漏识现象。
2、版本号为1.02.01，版本号的含义是1为主版本号，02为sample版本号，主要是针对sample程序修改，01是nna版本号，
主要是针对nna程序的修改，包括nna lib等。
3、修改在保存文件中图片中识别失败，人脸还是绿框的bug。
4、同时发了保存文件和不保存文件两个版本。

2020-07-13
1、版本号为1.02.01，保存视频文件但是却没有人脸框，测试用。

2020-07-13
1、版本号为1.02.01，读取/mnt/extsd/jpg/目录下的图像进行检测和识别，输出信息在控制台显示。格式如下：测试用。
count 91, file name ./jpg/face_171.jpg,  Time 550.74,  INSI_VIP Name : 1.06, ******
./sample_nna_mtv2_cap_pic

2020-07-16
1、版本号为1.03.01，最大支持50000个人脸ID的读取和录入，LCD屏上显示NNA运行总时间，log分别打印
MTV2 face num : 0,  run time 	mtv2检测到人脸个数和运行时间。
g2d_resize_format time 	g2d图像缩放时间
face_align1 time 	脸对齐时间
vip_preprocess time 	insightface预处理时间
vip_run time 	insightface运行时间
pstprocess time 	insightface后处理时间
INSI compare time 	人脸比对时间

2020-07-17
1、版本号为1.03.01，读取/mnt/extsd/jpg/目录下的图像进行检测和识别，最大支持50000个人脸ID的读取和录入，输出信息在控制台显示。格式如下：测试用。
count 91, file name ./jpg/face_171.jpg,  Time 550.74,  INSI_VIP Name : 1.06, ******
./sample_nna_mtv2_cap_pic

2020-07-17
1、版本号为1.04.01，改进输入图片的识别方法，省略rgb->yuv->rgb的过程，直接用rgb图片缩放识别，识别成功率有所提高，得分在0.95情况下从以前的识别9张到目前的41张。
得分在1.05情况下可以识别到75张。

2020-07-18
1、版本号为1.04.01，修改读取图片在1600张左右时系统出错的错误，原因是内存溢出，DecodeImageFromFile分配内存没有被释放导致。经测试可以正常读取2000张图片并识别。

2020-07-20
1、版本号为1.04.01，增加打印选中人脸的图片x1坐标（box.x1）和在LCD显示屏上的x1坐标（lcd.x1），系统运行不依赖摄像头和LCD显示屏。

2020-07-22
1、版本号为1.05.01，把检测人脸前的图像缩放到1600x900，老版本是768x432，缩放过大可能会造成信息丢失过多。把/mnt/extsd/sample_insi.conf中的resolution = 2。

2020-07-23
1、版本号为1.06.01，把由宏定义控制的编译选项改为配置文件来控制，修改配置文件/mnt/extsd/sample_insi.conf中的变量值使能控制。其中几个常用的变量为
cap_pic_recg， 0为不使能，正常视频识别；1为使能，图片识别。
support_rtsp， 0为不使能网络显示；1为使能网络显示。
face_upload，  0为不使能视频截图；1为使能视频截图，图片命名规则和以前一样，没有变化。
save_h26x_file 0为不使能保存视频文件；1为使能保存视频文件，保存的文件命名由resolution的值决定，0为800x480，1为1280x720，2为1920x1080。

2020-07-27
1、版本号为1.06.02，根据吕工的nna算法库的升级做出修改，主要修改点为根据摄像头的数据格式的差异，转换为的图像格式从RGB888-->BGR888。人脸对齐接口从opencv
库的调用改为仿射调用，缩小了demo的大小。
2、需要替换的文件有/mnt/extsd/net/insi_vip/data_quantized/wt_bs.bin和/mnt/extsd/net/mtv2/data_quantized/wt_bs.bin。

2020-07-29
1、版本号为1.06.02，修正输入1920x1080大小的图片程序异常的错误，原因是分配的内存小了。

2020-07-30
1、版本号为1.06.03--1.02.03，1.06.03为sample版本， 1.02.03为nna lib版本。修改fm 内存排列方式，并将量化图片修改为原来张旦使用的图片。
2、需要替换的文件有：/mnt/extsd/demo_insightface, /mnt/extsd/net/insi_vip/data_quantized/wt_bs.bin。需重新输入人脸ID。

2020-07-30
1、版本号为1.06.03--1.02.03，更新libinsi_vip.a库，其余无更改。

2020-08-03
1、版本号为1.07.03--1.02.03.01，NNA版本库更新为1.2.3.1，测试要求保存添加id log和对比log等。需要替换的文件有/mnt/extsd/demo_insightface。
sample取消一些不必要的线程，如音频和编码，可以缩减NNA运行时间。在SD、HD、FHD分辨率下的NNA运行时间有较大差异。
2、修改差值显示为1.0的错误。

2020-08-03
1、版本号为1.08.03--1.02.03.01，修改的地方有可以保存检测到却未识别出的人脸图像，包括yuv420图像（800x480），RGB888图像（800x480），BGR888仿射后图像（112x112），存放目录
为/mnt/extsd/img，命名格式为YUV_000.bin，RGB888_000.bin，BGR888_000.bin，相同的数字表示为同一帧图像的不同格式数据。
采集图像的配置为在sample_insi.conf文件中设置cap_pic_recg = 0和fd_no_fr = 1。
2、如果程序要运行图像模式，需修改sample_insi.conf文件中的cap_pic_recg = 1。并把图像文件放到/mnt/extsd/jpg目录下，比如把test.jpg，YUV_000.bin，RGB888_000.bin，
BGR888_000.bin放入jpg目录下运行程序即可，但不建议把不同格式的文件混合使用，最好一次使用一种格式。

2020-08-06
1、版本号为1.08.03--1.02.03.02，更新libinsi_vip.a库，修复了由于svd随机初始化导致的0.1误差的问题，
/mnt/extsd/net/insi_vip/data_quantized/wt_bs.bin使用1.02.03版本无更改。
需要替换的文件有：/mnt/extsd/demo_insightface和/mnt/extsd/sample_nna_mtv2。

2020-08-13
1、版本号为1.08.04--1.02.03.02，修改的地方为合并mtcnn和mtcnnv2 onet lnet，可以把NNA在camera状态下的运行总时间大幅减少到410ms左右。wt_bs使用的是v1.2.3.2。
需要替换的文件有：/mnt/extsd/demo_insightface和/mnt/extsd/sample_nna_mtv2。

2020-08-13
1、版本号为1.08.04--1.03.00.00，修改统计NNA运行时间没有包括nna_fd_run的错误。现在的NNA运行时间大约在430ms左右。

2020-08-19
1、版本号为1.08.04--1.03.01.00，此次nna算法库的修改较大，很多接口调用被合并。合并onet,lnet,人脸对齐，人脸识别为一个接口slfr_run，所有输入均为yuv格式。
2、由于变化比较大，主要是使用了nna_fd_run做为人脸检测，所以还需严格测试。
需要替换或增加的文件有：/mnt/extsd/sample_nna_mtv2，/mnt/extsd/slfr/data_quantized/wt_bs.bin。
/mnt/extsd/slfr/face_id/face_id.bin，可以把/mnt/extsd/insi_vip/face_id/face_id.bin文件拷贝过来即可。

2020-08-20
1、版本号为1.08.04--1.03.02.00，更改FD检测阈值，提高人脸检出率。
需要替换的文件有：/mnt/extsd/demo_insightface和/mnt/extsd/sample_nna_mtv2。

2020-08-24
1、版本号为1.09.04--1.03.02.00，在运行图片模式时，图片需做补边对齐处理，运行脚本padding_pics_class.py。
解决未识别到但显示绿框的问题。需要替换的文件有：/mnt/extsd/sample_nna_mtv2。

2020-08-25
1、版本号为1.09.04--1.03.03.00，修改：fd onet threshold 0.7->0.9，解决nna_fd_run函数在输入图片时由于分辨率不一导致出错问题，
修改了g2d在yuv转rgb时色彩转换明显差异的问题。
需要替换的文件有：/mnt/extsd/demo_insightface和/mnt/extsd/sample_nna_mtv2。

2020-08-27
1、版本号为1.09.04--1.03.04.00，具体修改如下：
A) 回退了g2d;  
B) 修改mtv2 onet阈值0.9->0.0，但会记录该阈值;  
C) 修改slfr,函数接口会提供一个score参数,当mtv2 onet阈值大于这个score才会进行人脸对齐及识别
D) fdnet 阈值60,70,90
2、需要替换的文件有：/mnt/extsd/demo_insightface和/mnt/extsd/sample_nna_mtv2。

2020-08-28
1、版本号为1.09.04--1.03.04.01，修改为由于回退了g2d，所以libnna.a的版本也要回退。
2、需要替换的文件有：/mnt/extsd/sample_nna_mtv2。

2020-08-31
1、版本号为1.09.04--1.03.04.02，weight等承接v1.3.4.0，修复了因内存泄露导致在某些情况下的weight数据被污染的bug，mtv2 onet阈值由90-->79。
2、需要替换的文件有：/mnt/extsd/demo_insightface和/mnt/extsd/sample_nna_mtv2。

2020-08-31
1、版本号为1.09.04--1.03.04.03，上一版本的库调试打印信息太多，故屏蔽。
2、需要替换的文件有：/mnt/extsd/demo_insightface和/mnt/extsd/sample_nna_mtv2。

2020-08-31
1、版本号为1.09.04--1.03.04.04，把net_fhd的内存分配放到最后。
2、需要替换的文件有：/mnt/extsd/sample_nna_mtv2。

2020-09-02
1、版本号为1.09.04--1.03.06.00，修复fd 检测框过多时出现问题。
2、需要替换的文件有：/mnt/extsd/demo_insightface和/mnt/extsd/sample_nna_mtv2。

2020-09-17
1、版本号为1.09.04--1.04.01.00，更新了g2d相关，更新了neon加速的人脸比对，且该版本为汇编优化，针对大容量ID库时，查询人脸ID时加速约5.9倍。
2、需要替换的文件有：/mnt/extsd/demo_insightface和/mnt/extsd/sample_nna_mtv2。

2020-09-22
1、版本号为1.09.04--1.04.02.00，解决了由于代码死锁导致的问题，ID不需要重新录入。
2、需要替换的文件有：/mnt/extsd/demo_insightface和/mnt/extsd/sample_nna_mtv2。

2020-09-22
1、版本号为1.09.05--1.04.02.00，增加打印NNA各部分运行时间功能，分别为nna_fd_run、net_slfr_run、net_insi_vip_compare_neon时间等。
在sample_insi.conf文件中设置time_print = 1使能该功能。
2、需要替换的文件有：/mnt/extsd/sample_nna_mtv2。

2020-09-23
1、版本号为1.09.06--1.04.02.00，把nna fd的阈值stl和ntl改为在配置文件中设置，分别有3个参数，默认分别为60,70,90和70,70,70，具体含义请咨询吕工。
2、需要替换的文件有：/mnt/extsd/sample_nna_mtv2和/mnt/extsd/sample_insi.conf。

2020-09-23
1、版本号为1.09.06--1.04.03.00，更新了demo_insightface可以添加人脸检测的运行参数，具体格式如下：
图片：./demo_insightface XXX.jpg 60 70 90   70 70 70，数字为可选项，默认为这几个
视频：./demo_insightface NULL    60 70 90   70 70 70,  NULL及后续数字为可选项，默认为这几个
更新了MTV2，以及nna，确保MTV2的API使用为最新的库形式
2、需要替换的文件有：/mnt/extsd/demo_insightface和/mnt/extsd/sample_nna_mtv2。


/*************************************************************************************************/


2020-10-28
1、PD01版本号为1.09.06--1.04.03.00，此demo是从sample_mtv2_net移植而来，主要修改了SL_nna.c文件。
2、需要替换的文件有：/mnt/extsd/sample_yolo3_pd，/mnt/extsd/sample_insi.conf，net/yolov3/data_quantized/wt_bs_pd.bin文件。

2020-10-28
1、PD01版本号为1.09.06--1.04.03.01，增加的功能有可以输入图片，图片名字为.jpg，格式为rgb888，可以正常输出识别结果。
在视频模式下可以保存原图数据，名字为x1_y1_x2_y2__x1_y1_x2_y2__x1_y1_x2_y2.......   .bin，所有识别到的人名字都会连上。
原图尺寸为800*480，坐标也是此尺寸下的。
开放阈值conf_thres和nms_thres，在配置sample_insi.conf文件中，默认值分别为0.3和0.45。
2、需要替换的文件有：/mnt/extsd/sample_yolo3_pd，/mnt/extsd/sample_insi.conf。wt文件请参考上一版。

2020-11-17
1、PD01版本号为1.09.06--2.00.00.00，增加的功能有可以保存原图和resieze成288的图片，配置sample_insi.conf文件中fd_no_fr = 1使能。
保存的目录在原图在img目录下，resize的图片在img80/img目录下，以上目录均需手动创建。
2、需要替换的文件有：/mnt/extsd/sample_yolo3_pd。

2020-12-07
1、PD01版本号为1.09.06--2.01.00.00，修改的功能有把算法需要的图像大小从288变为320，使误判的概率下降了不少。
因为sample同时集成了fd检测，motion检测，hd检测，所以通过配置文件sample_insi.conf中的detect_type来控制，
detect_type = 0 fd检测，detect_type = 1 motion检测，detect_type = 2 hd检测。
2、需要替换的文件有：/mnt/extsd/sample_yolo3_pd， net/hd/data_quantized/net_hd_wt_bs320.bin，其中wt名改为net_hd_wt_bs320.bin
方便运行以前的288 sample测试程序。

2020-12-28
1、版本号为1.09.06--2.01.00.02，新的量化算法，新的fm内存分配函数，在集成了人脸检测功能的情况下fm内存占用为2.3M左右，比上一版本
多了300K。wt文件大小从以前的319K变为了326K。
2、需要替换的文件有：/mnt/extsd/sample_yolo3_pd， net/hd/data_quantized/net_hd_wt_bs320.bin。
