#ifndef __DECODER__
#define __DECODER__
#ifdef __cplusplus
#include "zbar.h"

#include <zxing/common/Counted.h>
#include <zxing/Binarizer.h>
#include <zxing/MultiFormatReader.h>
#include <zxing/Result.h>
#include <zxing/ReaderException.h>
#include <zxing/common/GlobalHistogramBinarizer.h>
#include <zxing/common/HybridBinarizer.h>
#include <zxing/common/GreyscaleLuminanceSource.h>

#include <exception>
#include <zxing/Exception.h>
#include <zxing/common/IllegalArgumentException.h>
#include <zxing/BinaryBitmap.h>
#include <zxing/DecodeHints.h>

#include <zxing/qrcode/QRCodeReader.h>
#include <zxing/multi/qrcode/QRCodeMultiReader.h>
#include <zxing/multi/ByQuadrantReader.h>
#include <zxing/multi/MultipleBarcodeReader.h>
#include <zxing/multi/GenericMultipleBarcodeReader.h>


extern "C" {
#endif /* End of #ifdef __cplusplus */


extern bool DecoderZbarScanGray(void *pGrayData, int width, int height, char *pScanResultInfo);
extern bool DecoderZxingScanGray(void *pGrayData, int width, int height, char *pScanResultInfo);

extern void DecoderInit();

extern void DecoderDeInit();
#ifdef __cplusplus
}
#endif /* End of #ifdef __cplusplus */

#endif
