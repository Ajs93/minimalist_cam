#include <stdlib.h>
#include <iostream>
#include "decoder.h"

using namespace zbar;
using namespace std;
using namespace zxing;

static ImageScanner zbar_scanner;

void DecoderInit()
{
    printf("DecoderInit\r\n");
    zbar_scanner.set_config(ZBAR_QRCODE, ZBAR_CFG_ENABLE, 1);
}

bool DecoderZbarScanGray(void *pGrayData, int width, int height, char *pScanResultInfo)
{
    bool ret = false;
    //ImageScanner scanner;
    //scanner.set_config(ZBAR_NONE, ZBAR_CFG_ENABLE, 1);

    Image imageZbar(width, height, "Y800", pGrayData, width * height);
    zbar_scanner.scan(imageZbar); //扫描条码
    Image::SymbolIterator symbol = imageZbar.symbol_begin();
    if (imageZbar.symbol_begin() == imageZbar.symbol_end()) {
        //printf("DecoderScanGray, No QrData\r\n");
    }
    for (; symbol != imageZbar.symbol_end(); ++symbol) {
        printf("DecoderZbarScanGray, scan type[%d]:%s\r\n", symbol->get_type(), symbol->get_type_name().c_str());
        printf("DecoderZbarScanGray, scan content:%s\r\n", symbol->get_data().c_str());
        memcpy(pScanResultInfo, symbol->get_data().c_str(), symbol->get_data().length());
        ret = true;
        break;
        //cout << "DecoderZbarScanGray, scan type:" << symbol->get_type_name() << endl;
        //cout << "DecoderZbarScanGray, scan content:" << symbol->get_data() << endl << endl;
    }
    imageZbar.set_data(NULL, 0);
    return ret;
}

bool DecoderZxingScanGray(void *pGrayData, int width, int height, char *pScanResultInfo)
{
    try {
        // Convert the buffer to something that the library understands.
        ArrayRef<char> data((char*)pGrayData, width*height);
        //Ref<LuminanceSource> source (width, height);
        //source.buffer = pGrayData;
        GreyscaleLuminanceSource source (data, width, height, 0, 0, width, height);
        // Turn it into a binary image.
        Ref<Binarizer> binarizer (new GlobalHistogramBinarizer(source.crop(0, 0, width, height)));
        Ref<BinaryBitmap> image(new BinaryBitmap(binarizer));

        // Tell the decoder to try as hard as possible.
        DecodeHints hints(DecodeHints::QR_CODE_HINT);
        //hints.setTryHarder(true);

        // Perform the decoding.
        qrcode::QRCodeReader reader;
        Ref<Result> result(reader.decode(image, hints));

        // Output the result.
        BarcodeFormat format = result->getBarcodeFormat();
        printf("DecoderZxingScanGray, scan type[%d]:%s\r\n", format.value, format.barcodeFormatNames[format.value]);
        printf("DecoderZxingScanGray, scan content:%s\r\n", result->getText()->getText().c_str());
        //cout << result->getText()->getText().c_str << endl;
        memcpy(pScanResultInfo, result->getText()->getText().c_str(), result->getText()->getText().length());
        return true;
    }

    catch (zxing::Exception& e) {
        //cerr << "Error: " << e.what() << endl;
    }
    return false;
}

void DecoderDeInit()
{
    printf("DecoderDeInit\r\n");
}


