#include "mpp_audio.h"

int create_ai(AUDIO_Params* pAudioParams)
{
    int ret;
    AW_MPI_AI_SetPubAttr(pAudioParams->mAiDev, &pAudioParams->mAiAttr);
    AW_MPI_AI_Enable(pAudioParams->mAiDev);
    ret = AW_MPI_AI_CreateChn(pAudioParams->mAiDev, pAudioParams->mAiChn);
    if (SUCCESS == ret)
        printf("create ai channel[%d] success\n", pAudioParams->mAiChn);
    else
        printf("create ai channel[%d] ret[0x%x]\n", pAudioParams->mAiChn, ret);
    return 0;
}

int create_aenc(AUDIO_Params* pAudioParams)
{
    int ret;
    ret = AW_MPI_AENC_CreateChn(pAudioParams->mAencChn, (AENC_CHN_ATTR_S*)&pAudioParams->mAencAttr);
    if (SUCCESS == ret)
        printf("create aenc channel[%d] success\n", pAudioParams->mAencChn);
    else
        printf("create aenc channel[%d] ret[0x%x]\n", pAudioParams->mAencChn, ret);

    MPPCallbackInfo cbInfo;
    cbInfo.cookie = (void*)pAudioParams;
    cbInfo.callback = NULL;
    AW_MPI_AENC_RegisterCallback(pAudioParams->mAencChn, &cbInfo);
    return 0;
}

int destroy_ai(AUDIO_Params* pAudioParams)
{
    AW_MPI_AI_ResetChn(pAudioParams->mAiDev, pAudioParams->mAiChn);
    AW_MPI_AI_DestroyChn(pAudioParams->mAiDev, pAudioParams->mAiChn);
    AW_MPI_AI_Disable(pAudioParams->mAiDev);
    return 0;
}

int destroy_aenc(AUDIO_Params* pAudioParams)
{
    AW_MPI_AENC_ResetChn(pAudioParams->mAencChn);
    AW_MPI_AENC_DestroyChn(pAudioParams->mAencChn);
    return 0;
}

int bind_audio(AUDIO_Params* pAudioParams)
{
    MPP_CHN_S AiChn = {MOD_ID_AI, 0, pAudioParams->mAiChn};
    MPP_CHN_S AencChn = {MOD_ID_AENC, 0, pAudioParams->mAencChn};
    AW_MPI_SYS_Bind(&AiChn, &AencChn);
}

int start_ai_aenc(AUDIO_Params* pAudioParams)
{
    AW_MPI_AI_EnableChn(pAudioParams->mAiDev, pAudioParams->mAiDev);
    AW_MPI_AENC_StartRecvPcm(pAudioParams->mAencChn);
    return 0;
}

int stop_ai_aenc(AUDIO_Params* pAudioParams)
{
    AW_MPI_AENC_StopRecvPcm(pAudioParams->mAencChn);
    AW_MPI_AI_DisableChn(pAudioParams->mAiDev, pAudioParams->mAiChn);
    return 0;
}
