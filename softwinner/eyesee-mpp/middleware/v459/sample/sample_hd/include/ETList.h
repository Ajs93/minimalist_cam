#ifndef __ETLIST_H__
#define __ETLIST_H__

#include <string.h>
#include <stdlib.h>
#include <assert.h>
#ifdef __cplusplus
extern "C"
{
#endif
typedef void*    ETLISTPOS;

class ETList
{
public:
    ETList(unsigned int blocksize);
    virtual ~ETList(void);
protected:
    typedef struct LISTNode {
        struct LISTNode*    next;
        struct LISTNode*    prev;
        void*                data;
    } LISTNODE, *PLISTNODE;
    typedef struct LISTBlock {
        struct LISTBlock*    next;
    } LISTBLOCK, *PLISTBLOCK;

protected:
    PLISTNODE    m_head;
    PLISTNODE    m_tail;
    PLISTNODE    m_free;
    PLISTBLOCK    m_block;
    unsigned int        m_blocksize;
    unsigned int        m_count;
    bool        m_autodel;    //是否自动删除队列数据

protected:
    virtual void OnDataFree(void* data);
    int            AllocFreeNode(void);
    PLISTNODE    NewNode(PLISTNODE prev, PLISTNODE next);
    void        FreeNode(PLISTNODE node);

public:
    void        SetAutoDel(bool enable)
    {
        m_autodel = enable;
    }
    bool        IsAutoDel(void)
    {
        return m_autodel;
    }
    bool        IsEmpty(void) const
    {
        return m_count == 0;
    }
    unsigned int        GetCount(void) const
    {
        return m_count;
    }
    void*        GetAtHead(void) const
    {
        if( m_head == NULL) return NULL;
        return m_head->data;
    }
    void*        GetAtTail(void) const
    {
        if( m_tail == NULL) return NULL;
        return m_tail->data;
    }
    void*        GetAt(ETLISTPOS pos) const
    {
        PLISTNODE node = (PLISTNODE)pos;
        if( node==NULL) return NULL;
        return node->data;
    }
    ETLISTPOS    GetHead(void) const
    {
        return m_head;
    }
    ETLISTPOS    GetTail(void) const
    {
        return m_tail;
    }
    ETLISTPOS    GetNext(ETLISTPOS pos) const
    {
        PLISTNODE node = (PLISTNODE)pos;
        if( node==NULL ) return NULL;
        return node->next;
    }
    ETLISTPOS    GetPrev(ETLISTPOS pos) const
    {
        PLISTNODE node = (PLISTNODE)pos;
        if( node==NULL ) return NULL;
        return node->prev;
    }

public:
    void        RemoveAll(void);
    ETLISTPOS    AddHead(void* data);
    ETLISTPOS    AddTail(void* data);
    ETLISTPOS    InsertNext(ETLISTPOS pos, void* data);
    ETLISTPOS    InsertPrev(ETLISTPOS pos, void* data);
    ETLISTPOS    Find(void* data);
    void*        RemoveHead(void);
    void*        RemoveTail(void);
    void*        RemoveAt(ETLISTPOS pos);
    unsigned int RemoveFromTailNum(unsigned int num);

public:
    int            GetIDX(void* data) const;

};

#ifdef __cplusplus
}
#endif
#endif

