#ifndef __MPP_OSD__
#define __MPP_OSD__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <assert.h>
#include <time.h>

#ifdef USE_SYSTEM_CHAR_CONVERT
#include <iconv.h>
#else
#include "char_conversion.h"
#endif

#include <utils/plat_log.h>
#include "media/mpi_sys.h"
#include "osd_helper.h"
#include "mpi_region.h"
#include "BITMAP_S.h"
#include "mm_comm_sys.h"
#include "mm_common.h"

#ifdef __cplusplus
extern "C" {
#endif
typedef struct _OSD_Params {
    RGN_HANDLE     mOverlayHandle;

    unsigned char* pTextBuffer;
    RGN_ATTR_S     stRegionDate;
    BITMAP_S       stBmpDate;
    unsigned char  szText[256];
    MPP_CHN_S      mMppChn;
    unsigned int   x;
    unsigned int   y;
} OSD_Params;

typedef struct _Ych_OSD_Params {
	// video frame Y channel memory address
    char*			imgSrc;
	// video frame width
    int 			imgWidth;
	// video frame height
    int 			imgHeight;
	// osd coordinate
    unsigned int   	x;
    unsigned int   	y;
	// osd foreground(font) weight: 0~255, black~white
    unsigned char	ftWeight;
	// osd background weight: 0~255, black~white
	unsigned char	bkWeight;
	// osd background enable or not
	unsigned char	bkEnable;
} Ych_OSD_Params;


int sl_create_osd(OSD_Params* pOSDParams, int layerId);
int sl_update_osd(OSD_Params* pOSDParams, char* osdString, int len, int layerId);
// write osd to Y channel
int sl_update_osd_y(Ych_OSD_Params* pYchOSDParams, char* osdString, int len);
int sl_update_pos(OSD_Params* pOSDParams, int layerId);

int create_osd(OSD_Params* pOSDParams, int layerId);
int update_osd(OSD_Params* pOSDParams, void* pTime, int len, int layerId);
int destroy_osd(OSD_Params* pOSDParams);

#ifdef __cplusplus
}
#endif
#endif
