//
//  XMMute.hpp
//  testMute
//
//  Created by Ethan on 2018/7/12.
//  Copyright © 2018年 Ethan. All rights reserved.
//

#ifndef XMMute_hpp
#define XMMute_hpp

#include <stdio.h>
#include <pthread.h>
#ifdef __cplusplus
extern "C"
{
#endif
class XMMutex
{
public:
    XMMutex(void);
    virtual ~XMMutex(void);
public:
    bool    Enter(void);
    bool    Leave(void);

private:
    pthread_mutex_t m_mutex;
};

class XMGuard
{
public:
    XMGuard(XMMutex& mutex) :m_mutex(mutex)
    {
        m_mutex.Enter();
    }
    virtual ~XMGuard(void)
    {
        m_mutex.Leave();
    }
private:
    XMMutex&    m_mutex;
};
#ifdef __cplusplus
}
#endif
#endif /* XMMute_hpp */
