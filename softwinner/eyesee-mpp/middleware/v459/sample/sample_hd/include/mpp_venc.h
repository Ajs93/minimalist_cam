#ifndef __MPP_VENC__
#define __MPP_VENC__
#ifdef __cplusplus
extern "C" {
#endif /* End of #ifdef __cplusplus */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <assert.h>
#include <time.h>

#include <utils/plat_log.h>
#include "media/mpi_sys.h"
#include "vencoder.h"
#include "mm_comm_sys.h"
#include "mm_comm_video.h"
#include "mpi_venc.h"
#include "mm_common.h"
#include "mpi_mux.h"
#include "sample_config.h"

typedef enum _CODEC_FORMAT {
    CODEC_H264 = 0x00,
    CODEC_H265,
} CODEC_FORMAT;

typedef enum _RESOLUTION_TYPE {
    RESOLUTION_SD = 0x00,   //800*480
    RESOLUTION_HD,          //1280*720
    RESOLUTION_FHD          //1920*1080
    //标清:SD 高清:HD 超清:FHD
} RESOLUTION_TYPE;

typedef struct _VENC_Params {
    int iFrameNum;
    char szOutputFile[256];
    int srcWidth;
    int srcHeight;
    int srcSize;
    int srcPixFmt;

    int dstWidth;
    int dstHeight;
    int dstSize;
    int dstPixFmt;

    PAYLOAD_TYPE_E mVideoEncoderFmt;
    int mEncUseProfile;
    int mField;
    int mVideoMaxKeyItl;
    int mVideoBitRate;
    int mVideoFrameRate;
    int maxKeyFrame;
    int mTimeLapseEnable;
    int mTimeBetweenFrameCapture;

    int mRcMode;
    ROTATE_E rotate;

    int VeDev;
    int VeChnH26x;
    int VeChnMux;
} VENC_Params;

typedef struct _MUX_Params {
    char szOutputFile[256];
    int mFileDuration;
} MUX_Params;

int create_venc(VENC_Params* pVENCParams, VENC_CHN chn, int type);
int destroy_venc(VENC_CHN chn);
ERRORTYPE create_mux(VENC_Params* pVENCParams, MUX_GRP muxGrp, VENC_CHN veChn, int fd);
int save_jpeg_venc(VENC_CHN mVeChn, VENC_CROP_CFG_S* pCrop_cfg, VIDEO_FRAME_INFO_S *pVideoFrame, char* zsPicPath);
ERRORTYPE destroy_mux(MUX_GRP muxGrp);
#ifdef __cplusplus
}
#endif /* End of #ifdef __cplusplus */

#endif
