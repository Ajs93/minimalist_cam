#ifndef __MPP_VI__
#define __MPP_VI__
#ifdef __cplusplus
extern "C" {
#endif /* End of #ifdef __cplusplus */
#include <utils/plat_log.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <assert.h>
#include <time.h>

#include "mm_common.h"
#include "media/mm_comm_vi.h"
#include "media/mpi_sys.h"
#include "media/mpi_vi.h"
#include "media/mpi_isp.h"
#include "media/mpi_ise.h"
#include "media/mpi_ao.h"
#include "sample_config.h"

typedef enum _VIPP_INDEX {
    VIPP_0 = 0,
    VIPP_1,
    VIPP_2,
    VIPP_3,
    VIPP_NUM,
} VIPP_INDEX;

typedef enum _CHN_INDEX {
    VirViChn_0 = 0,
    VirViChn_1,
    VirViChn_2,
    VirViChn_3,
} CHN_INDEX;

typedef enum _STREAM_INDEX {
    VirVi_VENC = 0,
    VirVi_NNA = 1,
    VirVi_LCD = 2,
    VirVi_MAX,
} STREAM_INDEX;

typedef enum _PROC_INDEX {
    PROC_VENC = 0,
    PROC_NNA,
    PROC_AUDIO,
    PROC_LCD,
    PROC_AO,
    PROC_MAX,
} PROC_INDEX;

typedef struct _VIPP_Config {
    int            bInit;
    int            width;
    int            height;
    PIXEL_FORMAT_E eformat;
    int            frame_rate;
    int            bMirror;
    int            bFlip;
	int 			use;
} VIPP_Config;

typedef struct _VirVi_Params {
    VI_DEV      iViDev;
    VI_CHN      iViChn;
} VirVi_Params;

#if 0
typedef struct _ISE_PortCap_S {
    MPP_CHN_S ISE_Port_S;
    FILE *OutputFilePath;
    ISE_CHN_ATTR_S PortAttr;
} ISE_PortCap_S;

typedef struct _ISE_GroupCap_S {
    ISE_GRP ISE_Group;
    MPP_CHN_S ISE_Group_S;
    ISE_GROUP_ATTR_S pGrpAttr;
    ISE_CHN ISE_Port[4];
    ISE_PortCap_S *PortCap_S[4];
} ISE_GroupCap_S;
#endif

typedef struct _ISE_PortCap_S {
    ISE_GRP  ISE_Group;
    ISE_CHN  ISE_Port;
    int width;
    int height;
    char *OutputFilePath;
    ISE_CHN_ATTR_S PortAttr;
    AW_S32   s32MilliSec;
    pthread_t thread_id;
} ISE_PortCap_S;

typedef struct _ISE_pGroupCap_S {
    ISE_GRP ISE_Group;
    ISE_GROUP_ATTR_S pGrpAttr;
    ISE_PortCap_S PortCap_S[4];
} ISE_GroupCap_S;

typedef struct _ISE_Params {
    ISE_GroupCap_S iseGroupCap[ISE_MAX_GRP_NUM];
} ISE_Params;

int create_vi(VirVi_Params* pVirViParams);
int destroy_vi(VirVi_Params* pVirViParams);
int create_ise(VirVi_Params* pVirViParams, ISE_Params* pISEParams);
int destroy_ise(ISE_Params* pISEParams);
int start_vi_ise(VirVi_Params* pVirViParams, ISE_Params* pISEParams);
int stop_vi_ise(VirVi_Params* pVirViParams, ISE_Params* pISEParams);

extern VIPP_Config g_VIPP_map[VIPP_NUM];
#ifdef __cplusplus
}
#endif /* End of #ifdef __cplusplus */

#endif
