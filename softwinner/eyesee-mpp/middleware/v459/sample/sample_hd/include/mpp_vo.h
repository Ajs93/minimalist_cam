#ifndef __MPP_VO__
#define __MPP_VO__
#ifdef __cplusplus
extern "C" {
#endif /* End of #ifdef __cplusplus */

#include <utils/plat_log.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <assert.h>
#include <time.h>

#include "mm_common.h"
#include "media/mm_comm_vo.h"
#include "media/mpi_sys.h"
#include "media/mpi_vo.h"
#include <vo/hwdisplay.h>
#include <plat_type.h>
#include <tsemaphore.h>

typedef struct SampleVOContext {
    cdx_sem_t mSemExit;
} SampleVOContext;

typedef struct _VO_Params {
    int mDisplayX;
    int mDisplayY;
    int mDisplayWidth;
    int mDisplayHeight;
    char pStrDispType[8];
    int mVoDev;
    VO_LAYER mUILayer;
    VO_LAYER mVoLayer;
    VO_VIDEO_LAYER_ATTR_S mLayerAttr;
    VO_CHN mVoChn;
    VO_INTF_TYPE_E mDispType; // VO_INTF_LCD;
    VO_INTF_SYNC_E mDispSync; //

    VIDEO_FRAME_INFO_S mVoFrame;
    SampleVOContext mVoContext;
} VO_Params;


extern int create_vo(VO_Params* pVOParams);
extern int destroy_vo(VO_Params* pVOParams);
extern int PrepareTestFrame4VO(VO_Params* pVOParams, VIDEO_FRAME_INFO_S *pFrameInfo);
extern int SendFrame2VO(VO_Params* pVOParams, VIDEO_FRAME_INFO_S *pFrameInfo, int timeout_ms);
extern int MallocVideoFrame(VIDEO_FRAME_INFO_S *pFrameInfo, PIXEL_FORMAT_E ePixelFormat, int width, int height);
extern int FreeVideoFrame(VIDEO_FRAME_INFO_S * pFrameInfo);

#ifdef __cplusplus
}
#endif /* End of #ifdef __cplusplus */

#endif
