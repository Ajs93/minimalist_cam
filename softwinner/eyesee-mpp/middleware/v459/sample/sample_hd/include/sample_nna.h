#ifndef __SAMPLE_FACE_DETECT__
#define __SAMPLE_FACE_DETECT__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#include <assert.h>
#include <time.h>
#include <utils/plat_log.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>

#include "mpp_vo.h"
#include "mpp_vi.h"
#include "mpp_venc.h"
#include "osd_helper.h"
#include "mpp_audio.h"
#include "mpp_ao.h"

#include "media/mpi_sys.h"
#include "mm_comm_sys.h"
#include "mm_common.h"
#include "tmessage.h"
#include "tsemaphore.h"

#include "show.h"
#include "image_utils.h"
#include "timing.h"

#include "SL_nna.h"


#ifdef __cplusplus
extern "C" {
#endif /* End of #ifdef __cplusplus */


#define DISPLAY_WITDH              800	//(480);	//800;
#define DISPLAY_HEIGHT             480	//(640);	//480;


#define TEST_FRAME_NUM          (2000)  // 测试帧数

#define MAX(x, y)   ((x) > (y) ? (x) : (y))
#define MIN(x, y)   ((x) < (y) ? (x) : (y))


#define SAMPLE_INSI_CONF_PATH           "/mnt/UDISK/sample_insi.conf"

#define SAMPLE_INSI_MIN_SCORE      		"insi_min_score"
#define SAMPLE_INSI_MODE_SELECT			"mode_select"
#define SAMPLE_INSI_ENCODE_265   		"en_265"
#define SAMPLE_INSI_RESOLUTION			"resolution"
#define SAMPLE_INSI_BITRATE 		    "bitrate"
#define SAMPLE_INSI_FRAME_RATE			"frame_rate"
#define SAMPLE_INSI_PIC_RECG			"cap_pic_recg"
#define SAMPLE_INSI_IMG_ROTATE			"img_rotate"
#define SAMPLE_INSI_SUPPORT_RTSP		"support_rtsp"
#define SAMPLE_INSI_H26x_FILE			"save_h26x_file"
#define SAMPLE_INSI_USE_RNG_ATTR		"use_rng_attr"
#define SAMPLE_INSI_FACE_UPLOAD			"face_upload"
#define SAMPLE_INSI_MP4_FILE			"save_mp4_file"
#define SAMPLE_INSI_ENABLE_ISE			"enable_ise"
#define SAMPLE_INSI_SUPPORT_KEYBOARD	"support_keyboard"
#define SAMPLE_INSI_PWM_TEST			"pwm_test"
#define SAMPLE_INSI_TIME_PRINT			"time_print"
#define SAMPLE_INSI_RECT_KEEP			"rect_keep"
#define SAMPLE_INSI_FD_NO_FR			"fd_no_fr"
#define SAMPLE_INSI_IS_BLACKWHITE		"is_blackwhite"
#define SAMPLE_INSI_IS_CONF_THRES		"conf_thres"
#define SAMPLE_INSI_IS_NMS_THRES		"nms_thres"
#define SAMPLE_INSI_Y_ADD_NUM           "y_add_num"
#define SAMPLE_INSI_DETECT_TYPE         "detect_type"
#define SAMPLE_INSI_SAVE_ARGB_BIN       "save_argb"




typedef enum _MSG_TYPE {
    MSG_H264 = 0x0,
    MSG_H265,
    MSG_SD,
    MSG_HD,
    MSG_FHD,
    MSG_LIGHT,
    MSG_HFLIP,
    MSG_VFLIP,
    MSG_CAP,
    MSG_STOP,
    MSG_UNKNOWN
} MSG_TYPE;

typedef enum MPP_STATETYPE {
    MPP_StateIdle = 0X70000000,
    MPP_StateFilled,
    MPP_StateMax = 0X7FFFFFFF
} MPP_STATETYPE;

typedef struct _Picture_Params {
    MPP_STATETYPE              ePicState;
    VIDEO_FRAME_INFO_S         stVideoFrame;
    pthread_mutex_t            lockPicture;

} Picture_Params;


typedef void* (*ProcessFuncType)(void *pThreadData);
typedef struct _THREAD_DATA {
    ProcessFuncType ProcessFunc;
    pthread_t   mThreadID;
    void*       pPrivateData;

} THREAD_DATA;

typedef struct _NNA_Params {
    int            iFrameNum;

    PIXEL_FORMAT_E ePixFmt;
    int            iPicWidth;
    int            iPicHeight;
    int            iFrmRate;

    AW_HANDLE      pEVEHd;
} NNA_Params;

typedef struct _UserConfig {
    VirVi_Params   stVirViParams[VirVi_MAX];
    ISE_Params     stISEParams;
    VENC_Params    stVENCParams;
    VO_Params      stVOParams;
    NNA_Params     stNNAParams;
    MUX_Params     stMUXParams;
    AUDIO_Params   stAUDIOParams;
	AO_Params      stAOParams;
    THREAD_DATA    stThreadData[PROC_MAX];
    cdx_sem_t      mSemExit;
} UserConfig;


typedef struct _load_insi_Config {
	double insi_min_score;
	int mode_select;
	int bitrate;		// 10 15 20 25 30 35 40
	int en_265;			// 0 264; 1 265
	int resolution;		// 0 SD;	1 HD; 2 FHD
	int frame_rate;		// 20 25 30 
	int cap_pic_recg;
	int img_rotate;
	int support_rtsp;
	int save_h26x_file;
	int use_rng_attr;
	int face_upload;
	int save_mp4_file;
	int enable_ise;
	int support_keyboard;
	int pwm_test;
	int time_print;
	int rect_keep;
	int fd_no_fr;
	int is_blackwhite;

	double conf_thres;
	double nms_thres;
	int y_add_num;
	int detect_type;
	int save_argb;
}load_insi_Config;


typedef struct _version_t{
	unsigned char nna_v;
	unsigned char sample_v;
	unsigned char main_v;
	unsigned char res;

	unsigned char nna_m;
	unsigned char nna_fir;
	unsigned char nna_sec;
	unsigned char nna_thr;
}version_t;


void saveImage(const char *filename, const char *suffix, int Width, int Height,
               int Channels, unsigned char *Output, int frameIndex, int subIndex, int score);

void NV21_T_RGB(unsigned char *yuyv , unsigned char *rgb, unsigned int width , unsigned int height);


extern int64_t CDX_GetSysTimeUsMonotonic();
#ifdef __cplusplus
}
#endif /* End of #ifdef __cplusplus */

#endif

