#ifndef __MPP_AO__
#define __MPP_AO__

#ifdef __cplusplus
extern "C" {
#endif /* End of #ifdef __cplusplus */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <mm_common.h>
#include "media/mpi_sys.h"
#include "mpi_ao.h"
#include <ClockCompPortIndex.h>
#include <confparser.h>
#include "cdx_list.h"
#include <mpi_clock.h>
#include <tsemaphore.h>
#include "plat_log.h"


#define MAX_FILE_PATH_SIZE (256)

typedef struct AOConfig
{
    char	mPcmFilePath[MAX_FILE_PATH_SIZE];
    int 	mSampleRate;
    int 	mChannelCnt;
    int 	mBitWidth;
    int 	mFrameSize;
}AOConfig;

typedef struct SampleAOFrameNode
{
    AUDIO_FRAME_S 		mAFrame;
    struct list_head 	mList;
}AOFrameNode;

typedef struct AOFrameManager
{
    struct list_head 	mIdleList;
    struct list_head 	mUsingList;
    int 				mNodeCnt;
    pthread_mutex_t 	mLock;
    AUDIO_FRAME_S* (*PrefetchFirstIdleFrame)(void *pThiz);
    int (*UseFrame)(void *pThiz, AUDIO_FRAME_S *pFrame);
    int (*ReleaseFrame)(void *pThiz, unsigned int nFrameId);
}AOFrameManager;

typedef struct _AO_Params
{
    AOConfig 			mConfigPara;

    FILE 				*mFpPcmFile;
    AOFrameManager 		mFrameManager;
    pthread_mutex_t 	mWaitFrameLock;
    int 				mbWaitFrameFlag;
    cdx_sem_t 			mSemFrameCome;
    cdx_sem_t 			mSemEofCome;

    MPP_SYS_CONF_S 		mSysConf;
    AUDIO_DEV 			mAODev;
    AO_CHN 				mAOChn;
    AIO_ATTR_S 			mAIOAttr;
    CLOCK_CHN 			mClockChn;
    CLOCK_CHN_ATTR_S 	mClockChnAttr;
}AO_Params;

int create_ao(AO_Params* pAOParams);
int destroy_ao(AO_Params* pAOParams);

#ifdef __cplusplus
}
#endif /* End of #ifdef __cplusplus */

#endif
