#ifndef __MPP_AUDIO__
#define __MPP_AUDIO__
#ifdef __cplusplus
extern "C" {
#endif /* End of #ifdef __cplusplus */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <assert.h>
#include <time.h>

#include <utils/plat_log.h>
#include <plat_type.h>
#include <mm_comm_sys.h>
#include <mm_common.h>
#include <mpi_sys.h>
#include <mm_comm_aenc.h>
#include <mm_comm_aio.h>
#include <mpi_ai.h>
#include <mpi_aenc.h>

typedef struct _AUDIO_Params {
    AIO_ATTR_S mAiAttr;
    AENC_ATTR_S mAencAttr;
    int mAiDev;
    int mAiChn;
    int mAencChn;
} AUDIO_Params;

int create_ai(AUDIO_Params* pAudioParams);
int create_aenc(AUDIO_Params* pAudioParams);
int destroy_ai(AUDIO_Params* pAudioParams);
int destroy_aenc(AUDIO_Params* pAudioParams);
int start_ai_aenc(AUDIO_Params* pAudioParams);
int stop_ai_aenc(AUDIO_Params* pAudioParams);
int bind_audio(AUDIO_Params* pAudioParams);

#ifdef __cplusplus
}
#endif /* End of #ifdef __cplusplus */

#endif
