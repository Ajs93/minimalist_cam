#ifndef __SAMPLE_CONFIG__
#define __SAMPLE_CONFIG__
#ifdef __cplusplus
extern "C" {
#endif /* End of #ifdef __cplusplus */



#define DEMO_PIC_DATA_FORMAT_NV21 0
#define DEMO_PIC_DATA_FORMAT_YU12 1
//////////////////////////////////////////////////////////
#define DEMO_PIC_DATA_FORMAT DEMO_PIC_DATA_FORMAT_NV21
//#define DEMO_PIC_DATA_FORMAT DEMO_PIC_DATA_FORMAT_YU12


#if (DEMO_PIC_DATA_FORMAT == DEMO_PIC_DATA_FORMAT_NV21)
//NV21
#define DEMO_MM_PIXEL_FORMAT  MM_PIXEL_FORMAT_YVU_SEMIPLANAR_420
#define DEMO_V4L2_PIX_FMT      V4L2_PIX_FMT_NV21M
#else
//YU12
#define DEMO_MM_PIXEL_FORMAT  MM_PIXEL_FORMAT_YUV_PLANAR_420
#define DEMO_V4L2_PIX_FMT      V4L2_PIX_FMT_YUV420M
#endif


#define	H26X_FILE_COUNT		"./26x_file_count"
#define	PIC_DIR_LIST		"./jpg/"

//Notice that zbar decoder works only with image no flip or mirror at all.

//#define CAP_PIC_RECG
//#define ROTATE
//#define SUPPORT_RTSP           	//支持RTSP
//#define SAVE_H26x_FILE        //支持H264 or H265录像
//#define USE_RNG_ATTR           	//使用VI画线
//#define SUPPORT_FACE_UPLOAD
//#define SAVE_MP4_FILE
//#define ENABLE_ISE
//#define SUPPORT_KEYBOARD		// support ADC key

#define   RUN_IN_A_CHIP

#ifdef __cplusplus
}
#endif /* End of #ifdef __cplusplus */

#endif
