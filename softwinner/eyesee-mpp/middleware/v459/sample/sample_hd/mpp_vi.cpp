#include "mpp_vi.h"

VIPP_Config g_VIPP_map[VIPP_NUM];

static void GetVIPPAttr(VIPP_Config* pVIPPConfig, VI_ATTR_S* pViAttr)
{
    assert(pVIPPConfig->eformat == DEMO_MM_PIXEL_FORMAT);

    memset(pViAttr, 0, sizeof(VI_ATTR_S));
    pViAttr->type               = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
    pViAttr->memtype            = V4L2_MEMORY_MMAP;
    pViAttr->format.field       = V4L2_FIELD_NONE;
    pViAttr->format.width       = pVIPPConfig->width;      // 宽
    pViAttr->format.height      = pVIPPConfig->height;     // 高
    pViAttr->format.pixelformat = DEMO_V4L2_PIX_FMT;
    pViAttr->nbufs              = 3;
    pViAttr->nplanes            = 2;
    pViAttr->fps                = pVIPPConfig->frame_rate;
    pViAttr->capturemode        = V4L2_MODE_VIDEO;
    pViAttr->use_current_win    = 0;
    pViAttr->wdr_mode           = 0;
    //pViAttr->antishake_enable   = 0;
    //memset(&pViAttr->antishake_attr, 0, sizeof(ANTISHAKE_ATTR_S));
}

/*
    绑定情况下以下API不可用，数据在组件直接内部传递
    AW_MPI_VI_GetFrame 获取视频帧
    AW_MPI_VI_ReleaseFrame 释放视频帧
*/
int create_vi(VirVi_Params* pVirViParams)
{
    printf("\n\***********\n");
    for (int VirViIndex = 0; VirViIndex < VirVi_MAX; VirViIndex++) {
        printf("\n----\n");
        VIPP_INDEX VIPPIndex = (VIPP_INDEX)pVirViParams[VirViIndex].iViDev;  //TODO:type convert
        printf("VIPPIndex:%d, bInit:%d, %d ===\n",VIPPIndex, g_VIPP_map[VIPPIndex].bInit, g_VIPP_map[VIPPIndex].use);
		if(g_VIPP_map[VIPPIndex].use == 0) {
			continue;
		}
        // Create VIPP
        VI_ATTR_S stVIPP_Attr;
        GetVIPPAttr(&g_VIPP_map[VIPPIndex], &stVIPP_Attr);

        AW_MPI_VI_CreateVipp(VIPPIndex); //创建VIPP物理设备
        AW_MPI_VI_SetVippAttr(VIPPIndex, &stVIPP_Attr);//设置VIPP物理设备属性
        AW_MPI_VI_SetVippFlip(VIPPIndex, g_VIPP_map[VIPPIndex].bFlip);
        AW_MPI_VI_SetVippMirror(VIPPIndex, g_VIPP_map[VIPPIndex].bMirror);
        AW_MPI_VI_EnableVipp(VIPPIndex);//启动VIPP物理设备
        g_VIPP_map[VIPPIndex].bInit = 1;

        // 启动VirVi通道
        AW_MPI_VI_CreateVirChn(pVirViParams[VirViIndex].iViDev, pVirViParams[VirViIndex].iViChn, NULL);
        //基于某个VIPP，创建虚通道
        AW_MPI_VI_SetVirChnAttr(pVirViParams[VirViIndex].iViDev, pVirViParams[VirViIndex].iViChn, NULL);
#ifdef ENABLE_ISE
        //设置虚通道属性
        //AW_MPI_VI_EnableVirChn(pVirViParams[VirViIndex].iViDev, pVirViParams[VirViIndex].iViChn);
#else
        AW_MPI_VI_EnableVirChn(pVirViParams[VirViIndex].iViDev, pVirViParams[VirViIndex].iViChn);
#endif
        //启动虚通道
        printf("\n----\n");
    }
    printf("\n***********\n");

    //AW_MPI_VI_SetVippFlip(0, 1);
    //AW_MPI_VI_SetVippFlip(1, 1);
    //AW_MPI_VI_SetVippFlip(2, 1);

    // Start isp server
    AW_MPI_ISP_Init();//初始化 ISP 设备
    AW_MPI_ISP_Run(0);//运行ISP设备
#if 0
    ISP_MODULE_ONOFF ispModules;
    AW_MPI_ISP_GetModuleOnOff(0, &ispModules);
    printf("\n**********************************************\n");
    printf("lca = %d, gca = %d, lsc = %d, msc = %d\n", ispModules.lca, ispModules.gca, ispModules.lsc, ispModules.msc);
    printf("\n**********************************************\n");

    ispModules.lca = 0;
    ispModules.gca = 0;
    ispModules.lsc = 0;
    ispModules.msc = 0;
    ispModules.awb = 0;
    ispModules.wb = 0;

    AW_MPI_ISP_SetModuleOnOff(0, &ispModules);
#endif
    return 0;
}

int destroy_vi(VirVi_Params* pVirViParams)
{
    // Stop isp server
    AW_MPI_ISP_Stop(0);
    AW_MPI_ISP_Exit();

    for (int VirViIndex = 0; VirViIndex < VirVi_MAX; VirViIndex++) {
        VIPP_INDEX VIPPIndex = (VIPP_INDEX)pVirViParams[VirViIndex].iViDev;  //TODO:type convert
#ifdef ENABLE_ISE
        // 销毁VirVi通道
        //AW_MPI_VI_DisableVirChn(pVirViParams[VirViIndex].iViDev, pVirViParams[VirViIndex].iViChn);
#else
        AW_MPI_VI_DisableVirChn(pVirViParams[VirViIndex].iViDev, pVirViParams[VirViIndex].iViChn);
#endif
        AW_MPI_VI_DestoryVirChn(pVirViParams[VirViIndex].iViDev, pVirViParams[VirViIndex].iViChn);

        if (g_VIPP_map[VIPPIndex].bInit == 1) {
            AW_MPI_VI_DisableVipp(VIPPIndex);
            AW_MPI_VI_DestoryVipp(VIPPIndex);
            g_VIPP_map[VIPPIndex].bInit = 0;
        } else {
            g_VIPP_map[VIPPIndex].bInit--;
        }
    }
    return 0;
}

static int iseport_creat(ISE_GRP IseGrp, ISE_CHN IsePort, ISE_CHN_ATTR_S *PortAttr)
{
    int ret = -1;
    ret = AW_MPI_ISE_CreatePort(IseGrp,IsePort,PortAttr);
    if(ret < 0) {
        printf("Create ISE Port failed,IseGrp= %d,IsePort = %d",IseGrp,IsePort);
        return ret ;
    }
    ret = AW_MPI_ISE_SetPortAttr(IseGrp,IsePort,PortAttr);
    if(ret < 0) {
        printf("Set ISE Port Attr failed,IseGrp= %d,IsePort = %d",IseGrp,IsePort);
        return ret ;
    }
    return 0;
}

static int iseport_destory(ISE_GRP IseGrp, ISE_CHN IsePort)
{
    int ret = -1;
    ret = AW_MPI_ISE_DestroyPort(IseGrp,IsePort);
    if(ret < 0) {
        printf("Destory ISE Port failed, IseGrp= %d,IsePort = %d",IseGrp,IsePort);
        return ret ;
    }
    return 0;
}

static int isegroup_creat(ISE_GRP IseGrp, ISE_GROUP_ATTR_S *pGrpAttr)
{
    int ret = -1;
    ret = AW_MPI_ISE_CreateGroup(IseGrp, pGrpAttr);
    if(ret < 0) {
        printf("Create ISE Group failed, IseGrp= %d",IseGrp);
        return ret ;
    }
    ret = AW_MPI_ISE_SetGrpAttr(IseGrp, pGrpAttr);
    if(ret < 0) {
        printf("Set ISE GrpAttr failed, IseGrp= %d",IseGrp);
        return ret ;
    }
    return 0;
}

static int isegroup_destory(ISE_GRP IseGrp)
{
    int ret = -1;
    ret = AW_MPI_ISE_DestroyGroup(IseGrp);
    if(ret < 0) {
        printf("Destroy ISE Group failed, IseGrp= %d",IseGrp);
        return ret ;
    }
    return 0;
}

int create_ise(VirVi_Params* pVirViParams, ISE_Params* pISEParams)
{
    ISE_GroupCap_S *pISEGroupCap = pISEParams->iseGroupCap;
    ISE_PortCap_S *pISEPortCap;
    int ret = 0;
    for (int i = 0; i < VirVi_MAX; i++) {
        VIPP_INDEX VIPPIndex = (VIPP_INDEX)pVirViParams[i].iViDev;
        /*ise port bind channel attr*/
        memset(&pISEGroupCap[i].PortCap_S[0], 0, sizeof(ISE_PortCap_S));
        pISEGroupCap[i].PortCap_S[0].ISE_Group = i;
        pISEGroupCap[i].PortCap_S[0].ISE_Port = 0;
        pISEGroupCap[i].PortCap_S[0].thread_id = 0;
        pISEGroupCap[i].PortCap_S[0].s32MilliSec = 500;

        pISEPortCap = &pISEGroupCap[i].PortCap_S[0];
        /*Set ISE Port Attribute*/
        ISE_CFG_PARA_GDC *ise_gdc_cfg = &pISEPortCap->PortAttr.mode_attr.mFish.ise_gdc_cfg;
        ise_gdc_cfg->rectPara.warpMode = Warp_LDC;
        ise_gdc_cfg->srcImage.width = g_VIPP_map[VIPPIndex].width;
        ise_gdc_cfg->srcImage.height = g_VIPP_map[VIPPIndex].height;
        ise_gdc_cfg->srcImage.img_format = PLANE_YUV420; // YUV420
        ise_gdc_cfg->srcImage.stride[0] = g_VIPP_map[VIPPIndex].height;
        alogd("warpMode:Warp_LDC");
        ise_gdc_cfg->undistImage.img_format = PLANE_YUV420; // YUV420
        ise_gdc_cfg->undistImage.width = ise_gdc_cfg->srcImage.width;
        ise_gdc_cfg->undistImage.height = ise_gdc_cfg->srcImage.height;
        ise_gdc_cfg->undistImage.stride[0] = ise_gdc_cfg->srcImage.stride[0];
        //镜头参数
        ise_gdc_cfg->rectPara.LensDistPara.fx = 1112.81f;
        ise_gdc_cfg->rectPara.LensDistPara.fy = 1103.35f;
        ise_gdc_cfg->rectPara.LensDistPara.cx = 1206.34f;
        ise_gdc_cfg->rectPara.LensDistPara.cy = 709.01f;
        ise_gdc_cfg->rectPara.LensDistPara.fx_scale = 583.49f;
        ise_gdc_cfg->rectPara.LensDistPara.fy_scale = 578.52f;
        ise_gdc_cfg->rectPara.LensDistPara.cx_scale = 1211.90f;
        ise_gdc_cfg->rectPara.LensDistPara.cy_scale = 705.31f;

        //畸变系数
        ise_gdc_cfg->rectPara.LensDistPara.distCoef_fish_k[0] = -0.015305f;
        ise_gdc_cfg->rectPara.LensDistPara.distCoef_fish_k[1] = -0.019270f;
        ise_gdc_cfg->rectPara.LensDistPara.distCoef_fish_k[2] =  0.017113f;
        ise_gdc_cfg->rectPara.LensDistPara.distCoef_fish_k[3] = -0.007132f;

        //广角径向畸变系数
        ise_gdc_cfg->rectPara.LensDistPara.distCoef_wide_ra[0] = 0.00;
        ise_gdc_cfg->rectPara.LensDistPara.distCoef_wide_ra[1] = 0.00;
        ise_gdc_cfg->rectPara.LensDistPara.distCoef_wide_ra[2] = 0.00;

        //广角切向畸变系数
        ise_gdc_cfg->rectPara.LensDistPara.distCoef_wide_ta[0] = 0.00;
        ise_gdc_cfg->rectPara.LensDistPara.distCoef_wide_ta[1] = 0.00;
        //LDC校正参数
        ise_gdc_cfg->rectPara.LensDistPara.zoomH = 100;//[0,200]
        ise_gdc_cfg->rectPara.LensDistPara.zoomV = 100;//[0,200]
        ise_gdc_cfg->rectPara.LensDistPara.centerOffsetX = 0;
        ise_gdc_cfg->rectPara.LensDistPara.centerOffsetY = 0;
        ise_gdc_cfg->rectPara.LensDistPara.rotateAngle   = 0;//[0,360]

        ise_gdc_cfg->rectPara.warpPara.radialDistortCoef    = 0; //[-255,255]
        ise_gdc_cfg->rectPara.warpPara.trapezoidDistortCoef = 0; //[-255,255]
        //ise_gdc_cfg->rectPara.LensDistPara.distModel = DistModel_WideAngle;
        ise_gdc_cfg->rectPara.LensDistPara.distModel = DistModel_FishEye;

        /* creat ise component */
        ret = isegroup_creat(pISEGroupCap[i].ISE_Group, &pISEGroupCap[i].pGrpAttr);
        if(ret < 0) {
            printf("ISE Group%d create failed\n", pISEGroupCap[i].ISE_Group);
        }
        ret = iseport_creat(pISEGroupCap[i].ISE_Group, pISEGroupCap[i].PortCap_S[0].ISE_Port, &pISEPortCap->PortAttr);
        if(ret < 0) {
            printf("ISE Port%d creat failed\n", pISEGroupCap[i].PortCap_S[0].ISE_Port);
        }
    }
    return 0;
}

int destroy_ise(ISE_Params* pISEParams)
{
    ISE_GroupCap_S *pISEGroupCap = pISEParams->iseGroupCap;
    for (int i = 0; i < VirVi_MAX; i++) {
        iseport_destory(pISEGroupCap[i].ISE_Group, pISEGroupCap[i].PortCap_S[0].ISE_Port);
        isegroup_destory(pISEGroupCap[i].ISE_Group);
    }
    return 0;
}

int start_vi_ise(VirVi_Params* pVirViParams, ISE_Params* pISEParams)
{
    ISE_GroupCap_S *pISEGroupCap = pISEParams->iseGroupCap;
    int ret = -1;
    for (int i = 0; i < VirVi_MAX; i++) {
        MPP_CHN_S ViChn = {MOD_ID_VIU, i, pVirViParams[i].iViChn};
        MPP_CHN_S IseChn = {MOD_ID_ISE, i, 0};
        ret = AW_MPI_SYS_Bind(&ViChn,&IseChn);
        if(ret != SUCCESS) {
            printf("error!!! VI dev%d can not bind ISE Group%d!!!\n",
                   ViChn.mDevId,IseChn.mDevId);
        }
        AW_MPI_VI_EnableVirChn(pVirViParams[i].iViDev, pVirViParams[i].iViChn);
        AW_MPI_ISE_Start(pISEGroupCap[i].ISE_Group);
    }
    return 0;
}
int stop_vi_ise(VirVi_Params* pVirViParams, ISE_Params* pISEParams)
{
    ISE_GroupCap_S *pISEGroupCap = pISEParams->iseGroupCap;
    int ret = -1;

    for (int i = 0; i < VirVi_MAX; i++) {
        AW_MPI_ISE_Stop(pISEGroupCap[i].ISE_Group);
        AW_MPI_VI_DisableVirChn(pVirViParams[i].iViDev, pVirViParams[i].iViChn);
        MPP_CHN_S ViChn = {MOD_ID_VIU, i, pVirViParams[i].iViChn};
        MPP_CHN_S IseChn = {MOD_ID_ISE, i, 0};
        ret = AW_MPI_SYS_UnBind(&ViChn,&IseChn);
        if(ret != SUCCESS) {
            printf("error!!! Unbind VI dev%d ISE Group%d failed!!!\n",
                   ViChn.mDevId,IseChn.mDevId);
        }
    }
    return 0;
}
void GetYUVSize(unsigned int width
                ,unsigned int height
                ,unsigned int pixelformat
                ,int* mYsize
                ,int* mUsize
                ,int* mVsize
               )
{
    alogd("GetYUVSize width[%d], height[%d], pixelformat[%d]", width, height, pixelformat);

    if ((pixelformat == V4L2_PIX_FMT_YUV420M) || (pixelformat == V4L2_PIX_FMT_YVU420M)) {
        // yu12 && yv12
        *mYsize = width * height;
        *mVsize = width * height / 4;
        *mUsize = width * height / 4;
    } else if ((pixelformat == V4L2_PIX_FMT_NV12M) || (pixelformat == V4L2_PIX_FMT_NV21M)) {
        // nv12 && nv21
        *mYsize = width * height;
        *mUsize = width * height / 2;
        *mVsize = 0;
    } else {
        // unsupported
        alogd("GetYUVSize failed! Unsupported Pixel format %d", pixelformat);
    }
}

