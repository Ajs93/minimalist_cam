#ifndef __SL_NNA_H__
#define __SL_NNA_H__
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>
#include <memory.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

#include "lib/include/nna.h"
#include "lib/include/nna_g2d.h"
#include "lib/include/net_common.h"
#include "image_utils.h"
#include "lib/include/sl_net_hd.h"
#include "lib/include/sl_net_motion.h"


#ifdef __cplusplus
extern "C" {
#endif

#define    NET_DEBUG
#ifdef    NET_DEBUG
#define    PRINTF(...)        printf(__VA_ARGS__)
#else
#define    PRINTF(...)
#endif


#define    NAME_MAX_VALUE           256

#define    RESIZE_HD                320     //320

#if (RESIZE_HD == 320)
#define MAX_VALUE  10*10*48+20*20*24+40*40*8        //320
#define    HD_WT_BS_PATH        "/mnt/UDISK/net/hd/data_quantized/net_hd_wt_bs320.bin"
#else
#define MAX_VALUE  9*9*48+18*18*24+36*36*8            //288
#define    HD_WT_BS_PATH        "/mnt/UDISK/net/hd/data_quantized/net_hd_wt_bs288.bin"
#endif



typedef enum {
    NNA_MODE_FR = 0,    // 0 : face recognition
    NNA_MODE_FD,        // 1 : face detection

    NNA_MODE_MAX,
} NNA_MODE;


typedef struct osd_xybox_t {
    // detected face num
    int osd_box_num;
    // face boxes coordinate
    draw_box      boxRect[200];
    // face boxes score
    int           boxScore[200];
    // face boxes color
    unsigned int  clrRect[200];
    double   roundTime;
    char insi_name[NAME_MAX_VALUE];
    
    unsigned int frame_cnt;
    char    c;
} OSD_XYBOX_T;

typedef struct net_attdef_t {
    NNA_MEM_T         fm_mem;
    NNA_MEM_T         wt_mem;
    unsigned char*    wt_path;
} NET_ATTDEF_T;

typedef struct _slnna_info_t {
    nna_freq      nna_clk;
    NNA_MEM_T       mem_img; 
    NET_ATTDEF_T    net_hd;
    NET_ATTDEF_T    net_fhd;
} SLNNA_Info;


int SL_AllocNNA(SLNNA_Info *nnaMemInfo);

int SL_InitNNA(SLNNA_Info *nnaMemInfo);

/**
 执行检测

 @param nnaMemInfo        	神经网络信息
 @param imgSrc             	图像数据YUV的Y分量
 @param imgWidth         	图像的宽
 @param imgHeight         	图像的高
 @param detectBoxInfo     	检测对象的(属性)集合
 @param net_mode			运行的网络模型
 @return                 	检测到的对象的个数
 */
int SL_RunNNA( SLNNA_Info *nnaMemInfo,
               char *imgSrc,
               int imgWidth,
               int imgHeight,
               OSD_XYBOX_T *detectBoxInfo,
                NNA_MODE net_mode );
               //int net_mode );

int SL_FreeNNA(SLNNA_Info *nnaMemInfo);

int SL_CloseNNA(SLNNA_Info *nnaMemInfo);

#ifdef __cplusplus
}
#endif
#endif
