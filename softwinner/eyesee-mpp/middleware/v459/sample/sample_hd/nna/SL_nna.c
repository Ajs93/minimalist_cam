#include <sys/times.h>
#include "SL_nna.h"
#include "timing.h"
#include "../include/sample_nna.h"


extern load_insi_Config insi_Config;


static uint8_t vectors[MAX_VALUE];


void nna_soft_wait_event(void)
{
    nna_wait_event_done();
}

static void nna_motion_init()
{
  net_motion_init(320, 240);
  net_motion_set_nightview(0);
  net_motion_set_sensitivity(0);
}


void net_memAlloc( SLNNA_Info *nnaMemInfo )
{
    char          version[256];    
    uint32_t     max_fm_size = 1024 * 1024 * 2;         //1638400;     //1024 * 1024 * 2;         //fd need 2M
    NNA_MEM_T    shared_fm_mem;

    nna_mem_open();

    {
        NET_QUREY_T       hd_pqurey_param;
        //slfr_qurey_param.version = version;
        net_hd_query(&hd_pqurey_param);
        nnaMemInfo->net_hd.fm_mem.size = hd_pqurey_param.fm_size;
        nnaMemInfo->net_hd.wt_mem.size = hd_pqurey_param.wt_bs_size;
        max_fm_size = (hd_pqurey_param.fm_size > max_fm_size) ? hd_pqurey_param.fm_size : max_fm_size;
    }
    
    // TODO    
    PRINTF("\nmax_fm_size : %x", max_fm_size);    
    // -------------------------------------------------------------------------------    
    // allocate net memery     
    // -------------------------------------------------------------------------------    
    
    if( max_fm_size )    {        
        shared_fm_mem.size = max_fm_size;        
        nna_mem_alloc( &shared_fm_mem );        
        PRINTF("\nshare fm phy : %x", shared_fm_mem.addr_phy);        
        PRINTF("\nshare fm vir : %x", shared_fm_mem.addr_vir);    
    }    

    {
        nnaMemInfo->net_fhd.fm_mem.size    = max_fm_size;       
        nnaMemInfo->net_fhd.wt_mem.size    = 0;
        nnaMemInfo->net_fhd.wt_path        = NULL;
        nnaMemInfo->net_fhd.fm_mem.addr_phy        = shared_fm_mem.addr_phy;
        nnaMemInfo->net_fhd.fm_mem.addr_vir        = shared_fm_mem.addr_vir;
        PRINTF("\nfhd fm phy : %x", nnaMemInfo->net_fhd.fm_mem.addr_phy);
        PRINTF("\nfhd fm vir : %x", nnaMemInfo->net_fhd.fm_mem.addr_vir);
    }
    
    {
        nnaMemInfo->net_hd.fm_mem.addr_phy        = shared_fm_mem.addr_phy;
        nnaMemInfo->net_hd.fm_mem.addr_vir        = shared_fm_mem.addr_vir;
        printf("\nwt_mem %x",nnaMemInfo->net_hd.wt_mem.size);
        nna_mem_alloc( &nnaMemInfo->net_hd.wt_mem );
        PRINTF("\nhd_pd wt phy : %x", nnaMemInfo->net_hd.wt_mem.addr_phy);
        PRINTF("\nhd_pd wt vir : %x", nnaMemInfo->net_hd.wt_mem.addr_vir);
    }

    {
        nnaMemInfo->mem_img.size = 1920 * 1080 * 3;		// max malloc
        nna_mem_alloc( &nnaMemInfo->mem_img );
        PRINTF("mem img phy : %x\n", nnaMemInfo->mem_img.addr_phy);
        PRINTF("mem img vir : %x\n", nnaMemInfo->mem_img.addr_vir);
        PRINTF("mem img size: %x\n", nnaMemInfo->mem_img.size);
    }       
}


void net_nnaInit( SLNNA_Info *nnaMemInfo )
{    
    // -------------------------------------------------------------------------------    
    // net init    
    // -------------------------------------------------------------------------------    
    NNA_INIT_T    nna_init_param;    
    // default 400MHz    
    if(nnaMemInfo->nna_clk < NNA_CLK_100M || nnaMemInfo->nna_clk > NNA_CLK_600M || nnaMemInfo->nna_clk % 100 != 0) {
        nnaMemInfo->nna_clk = NNA_CLK_400M;
    }
    nna_init_param.nna_clock = nnaMemInfo->nna_clk;    
    nna_init( &nna_init_param );

    nna_setup_buffer(nnaMemInfo->net_fhd.fm_mem.addr_phy, nnaMemInfo->net_fhd.fm_mem.addr_vir, nnaMemInfo->net_fhd.fm_mem.size);
    int stl[3]={60,70,90};
    int ntl[3]={70,70,70};
    nna_fd_set_factor(79);
    nna_fd_set_threshold(stl,ntl);

    { 
        NET_INIT_T            hd_init_param;
        hd_init_param.mem_fm.addr_phy            = nnaMemInfo->net_hd.fm_mem.addr_phy;
        hd_init_param.mem_fm.addr_vir            = nnaMemInfo->net_hd.fm_mem.addr_vir;
        hd_init_param.mem_wt.addr_phy            = nnaMemInfo->net_hd.wt_mem.addr_phy;
        hd_init_param.mem_wt.addr_vir            = nnaMemInfo->net_hd.wt_mem.addr_vir;

        hd_init_param.mem_wt.size = nnaMemInfo->net_hd.wt_mem.size;
        hd_init_param.mem_wt.offset = 0x0;
        // slfr_init_param.wt_bs_path            = SLFR_WT_BS_PATH;
        strcpy(hd_init_param.wt_bs_path, HD_WT_BS_PATH);
        net_hd_init( &hd_init_param );
    }

    nna_motion_init();
}


// allocate memery: fm & wt_bs
int SL_AllocNNA(SLNNA_Info *nnaMemInfo)
{
    net_memAlloc( nnaMemInfo );

    return 0;
}

int SL_InitNNA(SLNNA_Info *nnaMemInfo)
{
    if (NULL == nnaMemInfo) {
        printf("[%s] Error:Input parameters is invalid!!!\n", __FUNCTION__);
        return -1;
    }

    net_nnaInit( nnaMemInfo );
	
    // init G2D, used to capture and resize image
    nna_g2d_open();

    nna_setup_wait_event_handler(nna_soft_wait_event);

    return 0;
}


int SL_FreeNNA(SLNNA_Info *nnaMemInfo)
{
    if (NULL == nnaMemInfo) {
        printf("[%s] Error:Input parameters is invalid!!!\n", __FUNCTION__);
        return -1;
    }

    {
        nna_mem_free( &nnaMemInfo->mem_img );
    }

    {
        nna_mem_free( &nnaMemInfo->net_hd.fm_mem );
        nna_mem_free( &nnaMemInfo->net_hd.wt_mem );
    }
    
    nna_mem_close();

    return 0;
}

int SL_CloseNNA(SLNNA_Info *nnaMemInfo)
{
    if (NULL == nnaMemInfo) {
        printf("[%s] Error:Input parameters is invalid!!!\n", __FUNCTION__);
        return -1;
    }

    net_hd_close();

    nna_close();
    nna_g2d_close();

    return 0;
}



//format 0 yuv; 1 RGB888 ; 2 BGR888 112x112
static void data_save_fun(char *addr, int size, int format, int count)
{
	char buff1[64] = {0, };
	if(format == 0) {
	    sprintf(buff1, "./img/YUV_%03d.bin", count);
	}else if(format == 1) {
        sprintf(buff1, "./img/RGB888_%03d.bin", count);
	}else if(format == 2) {
	    sprintf(buff1, "./img/BGR888_%03d.bin", count);
	}else {
        return;
	}
	nna_save_buf(addr, size, buff1);	
}


//行 Row 列 Column
/**
* 边界检查少不了
*/
static int rgb888_draw_line(char *addr, int width, int height, int x1, int y1, int x2, int y2, unsigned char color)
{
    int i, j, start;

    //top
    start = (y1 * width + x1) * 3;
    for(i=x1;i<x2;i++,start+=3) {
        *(addr+start+0) = 0xFF;
        *(addr+start+1) = 0x00;
        *(addr+start+2) = 0x00;;
    }
    //bottom
    start = (y2 * width + x1) * 3;
    for(i=x1;i<x2;i++,start+=3) {
        *(addr+start+0) = 0xFF;
        *(addr+start+1) = 0x00;;
        *(addr+start+2) = 0x00;;
    }
    //left
    start = (y1 * width + x1) * 3;
    for(i=y1;i<y2;i++,start+=width*3) {
        *(addr+start+0) = 0xFF;
        *(addr+start+1) = 0x00;;
        *(addr+start+2) = 0x00;;
    }
    //right
    start = (y1 * width + x2) * 3;
    for(i=y1;i<y2;i++,start+=width*3) {
        *(addr+start+0) = 0xFF;
        *(addr+start+1) = 0x00;;
        *(addr+start+2) = 0x00;;
    }

    return 0;
}


static char re_buf[800*480*3] = {0, };
static int netrun_hd( SLNNA_Info *nnaMemInfo, char *imgSrc, int imgWidth, int imgHeight, OSD_XYBOX_T *detectBoxInfo )
{
    unsigned int j = 0, k, box_list[4];
    double startTime = 0, closeTime = 0;
    struct nna_fd_bbox boxfd[200];
    int out_num = 0;
    NNA_G2D_FORMAT_ENUM nna_g2d_format;
    NNA_G2D_FRAME_T frame;
    NNA_G2D_RECT_T roi;
    char fname[1256] = {0, }, fname1[1256] = {0, };
    static unsigned int frame_cnt = 0;
    
    detectBoxInfo->roundTime = 0;
    startTime	= now();
    roi.x = box_list[0] = 0;
    roi.y = box_list[1] = 0;
    roi.w = box_list[2] = imgWidth - 1;
    roi.h = box_list[3] = imgHeight - 1;

    if(insi_Config.detect_type == 0) {
        /*************************************face detect***********************************/
        out_num = nna_fd_run(imgSrc, imgWidth, imgHeight, boxfd, 200);
        //printf("detect num %d\n", out_num);
        detectBoxInfo->osd_box_num    = out_num;
        for (int i = 0; i < out_num; i++) {
            detectBoxInfo->boxRect[i].x1    = boxfd[i].x1;
            detectBoxInfo->boxRect[i].x2    = boxfd[i].x2;
            detectBoxInfo->boxRect[i].y1    = boxfd[i].y1;
            detectBoxInfo->boxRect[i].y2    = boxfd[i].y2;
            detectBoxInfo->clrRect[i] = 0x00FF00;
        }
    }else if(insi_Config.detect_type == 1) {
        /*************************************motion detect***********************************/
        nna_g2d_format = NNA_G2D_FORMAT_YUV420UVC_V1U1V0U0;
        nna_g2d_resize_format_list(nnaMemInfo->net_hd.fm_mem.addr_phy, 320, 240, NNA_G2D_FORMAT_Y8, \
                                   (uint32_t)imgSrc, imgWidth, imgHeight, nna_g2d_format, box_list, 1);
        nna_g2d_frame_init(&frame, &nnaMemInfo->net_hd.fm_mem, imgWidth, imgHeight, NNA_G2D_FORMAT_Y8);
        net_motion_run(&frame, &roi, &out_num);
        if (out_num > 0)
            printf("w h:[%d %d] out_result: %d\n", imgWidth, imgHeight, out_num);
    }else {
        /*************************************human detect***********************************/
        out_num = 0;
        if(detectBoxInfo->c == 'V') {
            memset(detectBoxInfo->insi_name, 0, NAME_MAX_VALUE);
            nna_g2d_format = NNA_G2D_FORMAT_YUV420UVC_V1U1V0U0;
        }else if(detectBoxInfo->c == 'W') {
            nna_g2d_format = NNA_G2D_FORMAT_YUV420UVC_V1U1V0U0;         //if use bgr888, format NNA_G2D_FORMAT_BGR888
        }else if(detectBoxInfo->c == 'J') {
            nna_g2d_format = NNA_G2D_FORMAT_RGB888;         //if use bgr888, format NNA_G2D_FORMAT_BGR888
            
            //nna_save_buf(nnaMemInfo->mem_img.addr_vir, imgWidth*imgHeight*3, "./test1111.bin");
            
        }else if(detectBoxInfo->c == 'Y') {
            //printf("detectBoxInfo->c == Y\n");
            nna_g2d_format = NNA_G2D_FORMAT_YUV420_PLANAR;     // NNA_G2D_FORMAT_Y8
        }else if(detectBoxInfo->c == 'R') {
            printf("detectBoxInfo->c == R\n");
            nnaMemInfo->net_hd.fm_mem.addr_vir = nnaMemInfo->mem_img.addr_vir;
        }else {
            printf("detectBoxInfo->c %c no surport now\n", detectBoxInfo->c);
            return 0;
        }
        if(detectBoxInfo->c == 'V' || detectBoxInfo->c == 'J' || detectBoxInfo->c == 'W') {
            nna_g2d_resize_format_list( nnaMemInfo->net_hd.fm_mem.addr_phy, RESIZE_HD, RESIZE_HD, NNA_G2D_FORMAT_ARGB8888, \
                                        (uint32_t)imgSrc, imgWidth, imgHeight, nna_g2d_format, box_list,1 );
        }else {
            if(detectBoxInfo->c == 'Y') {       //RGB->YUV420PLANER->RGB时，G2D bug，需写成 BGR888
                nna_g2d_resize_format_list( nnaMemInfo->net_hd.fm_mem.addr_phy, RESIZE_HD, RESIZE_HD, NNA_G2D_FORMAT_BGR888, \
                                            (uint32_t)imgSrc, imgWidth, imgHeight, nna_g2d_format, box_list,1 );
            }
        }
        //printf("src %p, vir %p, fm_mem.addr_phy %p, fm_mem.addr_vir %p\n", (uint32_t)imgSrc, nnaMemInfo->mem_img.addr_vir, \
        //                    nnaMemInfo->net_hd.fm_mem.addr_phy, nnaMemInfo->net_hd.fm_mem.addr_vir);

        closeTime 	= 1000.0 * calcElapsed(startTime, now());
        //printf("g2d_resize_format time: %.3f ms\n", closeTime);
        detectBoxInfo->roundTime += closeTime;

        //nna_save_buf((char *)nnaMemInfo->net_hd.fm_mem.addr_vir, RESIZE_HD*RESIZE_HD*3, "./resize288rgb.bin");
        if(insi_Config.fd_no_fr == 1 && (detectBoxInfo->c == 'V' || detectBoxInfo->c == 'J' || detectBoxInfo->c == 'W')) {
            memcpy(re_buf, (char *)nnaMemInfo->net_hd.fm_mem.addr_vir, RESIZE_HD * RESIZE_HD * 4);
        }
        
        startTime   = now();
        net_hd_preprocess(&nnaMemInfo->net_hd.fm_mem, &nnaMemInfo->net_hd.fm_mem, NULL);
        closeTime 	= 1000.0 * calcElapsed(startTime, now());
        //printf("hd_preprocess time: %.3f ms\n", closeTime);
        detectBoxInfo->roundTime += closeTime;

        startTime   = now();
        net_hd_run(vectors);
        closeTime 	= 1000.0 * calcElapsed(startTime, now());
        //printf("hd_run time: %.3f ms\n", closeTime);
        detectBoxInfo->roundTime += closeTime;

        startTime   = now();
        out_num = 200;
        net_hd_pstprocess(vectors, boxfd, &out_num, insi_Config.conf_thres, insi_Config.nms_thres);       //0.3,  0.45
        closeTime 	= 1000.0 * calcElapsed(startTime, now());
    	detectBoxInfo->roundTime += closeTime;
    	detectBoxInfo->osd_box_num = out_num;
        //printf("hd pstprocess %.3f, total time %.3f ms, num %d\n", closeTime, detectBoxInfo->roundTime, out_num);

    	for (int i = 0; i < out_num; i++) {
    	    if(i == 0) {
        	    detectBoxInfo->frame_cnt = frame_cnt++;
    	        j += sprintf(&fname[j], "img/%s_", detectBoxInfo->insi_name);
    	    }else {
                //j += sprintf(&fname[j], "__");
    	    }
    	    detectBoxInfo->boxRect[i].x1    = (int)((float)(boxfd[i].x1) * imgWidth / RESIZE_HD);
            detectBoxInfo->boxRect[i].x2    = (int)((float)(boxfd[i].x2) * imgWidth / RESIZE_HD);
            detectBoxInfo->boxRect[i].y1    = (int)((float)(boxfd[i].y1) * imgHeight / RESIZE_HD);
            detectBoxInfo->boxRect[i].y2    = (int)((float)(boxfd[i].y2) * imgHeight / RESIZE_HD);
            detectBoxInfo->clrRect[i] = 0x00FF00;
            //printf("boxfd x1 %d y1 %d x2 %d y2 %d\n", boxfd[i].x1, boxfd[i].y1, boxfd[i].x2, boxfd[i].y2);
            
            //j += sprintf(&fname[j], "%d_%d_%d_%d", detectBoxInfo->boxRect[i].x1, detectBoxInfo->boxRect[i].y1, detectBoxInfo->boxRect[i].x2, detectBoxInfo->boxRect[i].y2);
            int wid_len = 0, hei_len = 1;
            double wid_hei_scale;
            wid_len = detectBoxInfo->boxRect[i].x2 - detectBoxInfo->boxRect[i].x1;
            hei_len = detectBoxInfo->boxRect[i].y2 - detectBoxInfo->boxRect[i].y1;
            wid_hei_scale = (double)wid_len / hei_len; 
            if(detectBoxInfo->c == 'W') {       // V
                NV21_T_RGB((unsigned char*)nnaMemInfo->mem_img.addr_vir, re_buf, imgWidth, imgHeight);
                rgb888_draw_line((char *)re_buf, imgWidth, imgHeight, \
                        detectBoxInfo->boxRect[i].x1, detectBoxInfo->boxRect[i].y1, detectBoxInfo->boxRect[i].x2, detectBoxInfo->boxRect[i].y2, 0x00);
            } else if(detectBoxInfo->c == 'J') {
                rgb888_draw_line((char *)nnaMemInfo->mem_img.addr_vir, imgWidth, imgHeight, \
                        detectBoxInfo->boxRect[i].x1, detectBoxInfo->boxRect[i].y1, detectBoxInfo->boxRect[i].x2, detectBoxInfo->boxRect[i].y2, 0x00);
    	    }
    	    //printf("x1 %d, y1 %d, x2 %d, y2 %d, score %d, wid_len %d, hei_len %d, wid_hei_scale %.3f\n", detectBoxInfo->boxRect[i].x1, detectBoxInfo->boxRect[i].y1, \
                   detectBoxInfo->boxRect[i].x2, detectBoxInfo->boxRect[i].y2, boxfd[i].score, wid_len, hei_len, wid_hei_scale);
	    }

        if(insi_Config.fd_no_fr == 1) {
            if(detectBoxInfo->c == 'V') {
                j += sprintf(&fname[j], ".bin");
                nna_save_buf(nnaMemInfo->mem_img.addr_vir, imgWidth*imgHeight*3/2, fname);
                k = sprintf(&fname1[0], "img80/");
                memcpy(&fname1[k], fname, j);
                //printf("%s\n", fname1);
                nna_save_buf(re_buf, RESIZE_HD*RESIZE_HD*3, fname1);
            }else if(detectBoxInfo->c == 'J') {
                j += sprintf(&fname[j], ".jpg");
                if (!tje_encode_to_file(fname, imgWidth, imgHeight, 3, true, nnaMemInfo->mem_img.addr_vir)) {
                    printf("save JPEG fail. 1111\n");
                }                
            }else if(detectBoxInfo->c == 'W') {
                if(insi_Config.save_argb == 1) {
                    j += sprintf(&fname[j], "_argb.bin");
                    nna_save_buf(re_buf, RESIZE_HD*RESIZE_HD*4, fname);                    
                }
            }
            //system("sync");
        }    
    }
	sprintf(detectBoxInfo->insi_name, "HD PD %d", out_num);
    
    return out_num;
}


int SL_RunNNA( SLNNA_Info *nnaMemInfo,
               char *imgSrc,
               int imgWidth,
               int imgHeight,
               OSD_XYBOX_T *detectBoxInfo,
               NNA_MODE net_mode )
{
    int detectNum = 0;
    switch(net_mode) {
    
    case NNA_MODE_FD:   // human face detection
        detectNum = netrun_hd( nnaMemInfo, imgSrc, imgWidth, imgHeight, detectBoxInfo);
        break;
        
    default:
        break;
    }

    return detectNum;
}


