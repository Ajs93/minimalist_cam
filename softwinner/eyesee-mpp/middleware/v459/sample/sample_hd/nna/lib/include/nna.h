/**
* @file
* NNA API header file.
*/

#ifndef _NNA_H_
#define _NNA_H_

#include <ctype.h>
#include <stdio.h>
#include <inttypes.h>

#include "nna_version.h" 

#include "nna_memory.h"
#include "nna_g2d.h"
#include "nna_api_v1.h" 
//#include "utility.h"

#ifdef __cplusplus
extern "C"
{
#endif

#define NNA_NULL_ADDR			0xFFFFFFFF
#define NNA_ADDR_MASK_FM        ((0<<31)&0xFFFFFFFF)
#define NNA_ADDR_MASK_WT_BS     ((1<<31)&0xFFFFFFFF)

/** image layer input format  
*/
enum NNA_API_CFG_IMG_FORMAT {

    /*! enable dw mode */
    NNA_API_CFG_DW_FORMAT_ENABLE  = 2,

    /*! enable image mode */
    NNA_API_CFG_IMG_FORMAT_ENABLE  = 1,

    /*! disable image mode */
    NNA_API_CFG_IMG_FORMAT_DISABLE = 0
};

/** bias, batchnorm, scale  
*/
enum NNA_API_CFG_BIAS_TERM {
    /*! NULL */
    NNA_API_CFG_BIAS_TERM_NONE = 0,
     
    /*! bias */
    NNA_API_CFG_BIAS_TERM_BIAS = 1, 
    
    /*! bias -> scale */
    NNA_API_CFG_BIAS_TERM_BIAS_SCALE = 2, 
    
     /*! scale */
    NNA_API_CFG_BIAS_TERM_SCALE = 3,
    
    /*! scale -> bias */
    NNA_API_CFG_BIAS_TERM_SCALE_BIAS = 4 
};

/** activation type 
*/
enum NNA_API_CFG_ACT_TYPE {
    /*! NULL */
    NNA_API_CFG_ACT_TYPE_NONE,
    
    /*! ReLU */
    NNA_API_CFG_ACT_TYPE_RELU,
    
    /*! Leaky ReLU */
    NNA_API_CFG_ACT_TYPE_LRELU,
    
    /*! PReLU */
    NNA_API_CFG_ACT_TYPE_PRELU,
    
    /*! Sigmoid */
    NNA_API_CFG_ACT_TYPE_SIGMOID,
    
    /*! Swish */
    NNA_API_CFG_ACT_TYPE_SWISH,
    
    /*! Tanh */
    NNA_API_CFG_ACT_TYPE_TANH,
    
    /*! BGR to 8-ch Feature map : channel-wise padding*/
    NNA_API_CFG_ACT_TYPE_UINT2INT,
	
	/*!  RELU6 */
    NNA_API_CFG_ACT_TYPE_RELU6,
    
	/*!  MISH */
    NNA_API_CFG_ACT_TYPE_MISH,
};

/** Pooling type 
*/
enum NNA_API_CFG_POOL_TYPE {
    /*! Average */
    NNA_API_CFG_POOL_TYPE_AVERAGE = 0,
    
    /*! Max */
    NNA_API_CFG_POOL_TYPE_MAX = 1,
    
    /*! Min */
    NNA_API_CFG_POOL_TYPE_MIN = 2,
    
    /*! None */
    NNA_API_CFG_POOL_TYPE_NONE = 3,
};

/* Element-wise operation type 
*/
enum NNA_API_CFG_EW_TYPE {
    /*! sum */
    NNA_API_CFG_EW_TYPE_SUM = 0,
    
    /*! max */
    NNA_API_CFG_EW_TYPE_MAX = 1,
    
    /*! multiplication */
    NNA_API_CFG_EW_TYPE_MUL = 2,

    /*! min */
    NNA_API_CFG_EW_TYPE_MIN = 3,

	/*! crop */
	NNA_API_CFG_EW_TYPE_CROP = 0xd,

    /*! insert */
    NNA_API_CFG_EW_TYPE_INSERT = 0xe,

    /*! NONE */
    NNA_API_CFG_EW_TYPE_NONE = 0xf
};

/* Element-wise insert mode
*/

enum NNA_API_CFG_EW_INSERT_MODE {
    /*! zero */
    NNA_API_CFG_EW_INSERT_MODE_ZERO = 0,

    /*! same */
    NNA_API_CFG_EW_INSERT_MODE_SAME = 1,

};

/**
 * @brief NNA API Config
*/

typedef struct {
    /*! @brief DDR offset for output feature map */
    unsigned int output_addr;

    /*! @brief DDR offset for input feature map */
    unsigned int input_addr;
    
    /*! @brief DDR offset for input weight data */
    unsigned int weight_addr;
    
    /*! @brief DDR offset for input bias/scale/mult data */
    unsigned int bias_addr;
    
    /*! @brief DDR offset for input 2nd bias/scale/mult data */
    unsigned int bias_addr_x2;
    
    /*! @brief DDR offset for input Element-wise feature map */
    unsigned int input_ew_addr;
    
    /*! @brief DDR offset for weight matrix data  */
    unsigned int weight_matrix_addr;
    
    /*! @brief output feature map quantization bits */
    /*! @details 
        - positive : right shift
        - negative : left shift
     */
    int output_shift;

    /*! @brief output feature map scale */
    int output_scale;
    
    /*! @brief bias shift bits */
    /*! @details 
        - positive : left shift
        - negative : Not defined
     */
    int bias_shift;
    
    /*! @brief image mode */
    /*! @see NNA_API_CFG_IMG_FORMAT
     */
    int img_format;
    
    /*! @brief mean values, only valid when image mode enabled */
    /*! @see NNA_API_CFG_T::img_format
     */
    int img_mean_op;
    
    /*! @brief image scale */
    int img_scale;
    
    /*! @brief image truncate(right shift) bits */
    int img_truncate;
    
    /*! @brief pixel data type : 1=int8, 0=unsigned int8, only valid when image mode enabled */
    int img_data_type;
    
    /*! @brief input feature map : width */
    int input_w;
    
    /*! @brief input feature map : height */
    int input_h;
    
    /*! @brief input feature map : channel */
    int input_c;
    
    /*! @brief element-wise input feature map : channel */
    int input_ew_c;
    
    /*! @brief convolution  : width */
    /*! @details range [0~32] */
    int num_output;

    /*! @brief convolution kernel size */
    /*! @details 
        | bit  | description |
        |----- | ------------|
        | 15~8 | kernel_h    |
        |  7~0 | kernel_w    |
    */
    int kernel;

    /*! @brief convolution dilation size */
    /*! @details 
        | bit  | description |
        |----- | ------------|
        | 15~8 | dilation_h    |
        |  7~0 | dilation_w    |
    */    
    int dilation;

    /*! @brief convolution stride size */
    /*! @details 
        | bit  | description |
        |----- | ------------|
        | 15~8 | stride_h    |
        |  7~0 | stride_w    |
    */    
    int stride;

    /*! @brief convolution padding size */
    /*! @details 
        | bit  | description |
        |----- | ------------|
        | 31~24| pad_bottom  |
        | 23~16| pad_top    |
        | 15~8 | pad_right  |
        |  7~0 | pad_left   |
    */    
    int pad;
    
    /*! @brief bias/scale */
    /*! @see NNA_API_CFG_BIAS_TERM */    
    int bias_term;    
    
    /*! @brief activation type */
    /*! @see NNA_API_CFG_ACT_TYPE */    
    int act_type;
    
    /*! @brief activation operand */
    /*! @details
       | act_type | act_op |
       | ---------| -------|
       | NONE     | 0, not used |
       | ReLU     | 0, not used |
       | PReLU(Leaky) | (mult_value_in_16bit)<<8 \| (truncation_in_8bit) |
       | RReLU    | (1<<31) \| (mult_value_in_16bit)<<8 \| (truncation_in_8bit) |
    */ 
    /*! @see NNA_API_CFG_ACT_TYPE */   
    int act_op;
    
    /*! @brief Element-wise operation type */
    /*! @see NNA_API_CFG_EW_TYPE */    
	int ew_operation;
	
	/*! @brief Element-wise operation A-coeff */
	int ew_a_coeff;
	
	/*! @brief Element-wise operation B-coeff */
	int ew_b_coeff;
	
	/*! @brief Element-wise operation B-shift */
	int ew_b_shift;

	/*! @brief Element-wise operation insert size */
    int ew_insert_size;

	/*! @brief Element-wise operation insert size */
    int ew_crop_size;

	/*! @brief Element-wise operation insert size */
    int ew_insert_mode;

    /*! @brief Group-norm ratio */
    int gn_ratio;

    /*! @brief Group-norm variation (right) shift bits */
    int gn_var_shift;
	
    /*! @brief Pooling type */
    /*! @see NNA_API_CFG_POOL_TYPE */    
    int pool_type;
    
    /*! @brief Pooling kernel size  */
    /*! @details 
        | bit  | description |
        |----- | ------------|
        | 15~8 | kernel_h    |
        |  7~0 | kernel_w    |
    */
    int pool_kernel;
    
    
    /*! @brief Pooling stride size */
    /*! @details 
        | bit  | description |
        |----- | ------------|
        | 15~8 | stride_h    |
        |  7~0 | stride_w    |
    */
    int pool_stride;

    /*! @brief Pooling padding size */
    /*! @details 
        | bit  | description |
        |----- | ------------|
        | 31~24| pad_bottom  |
        | 23~16| pad_top    |
        | 15~8 | pad_right  |
        |  7~0 | pad_left   |
    */    
    int pool_pad;

    /*! @brief mult operation : operand */
	int mult_operand;
	
    /*! @brief mult operation : shift bits */
	int mult_shift;

    /*! @brief LRN : size */
    int lrn_size;
    
    /*! @brief LRN : ifm scale */
    int lrn_in_scale;
    
    /*! @brief LRN : ifm offset */
    int lrn_in_offset;

    /*! @brief LRN : ifm shift */
    int lrn_in_shift;

    /*! @brief LRN : ofm scale */
    int lrn_out_scale;

    /*! @brief LRN : ofm offset */
    int lrn_out_offset;

    /*! @brief LRN : ofm shift */
    int lrn_out_shift;
    	
} NNA_API_CFG_T;

/*! 
* @brief convolution - bias/scale - activation
* @param[in] p_cfg API_CFG
*
* @snippet net_api_test.cpp api_test_tc_001
*/
void nna_api_conv_act( NNA_API_CFG_T *p_cfg);

/*! 
* @brief convolution - bias/scale - activation - pooling
* @param[in] p_cfg NNA_API_CFG_T
*
* @snippet net_api_test.cpp api_test_tc_001
* 
*/
void nna_api_conv_act_pool(NNA_API_CFG_T *p_cfg);

/*! 
* @brief convolution - bias/scale - activation - ElementWise 
* @param[in] p_cfg NNA_API_CFG_T
*
* @snippet net_api_test.cpp api_test_tc_002
*/
void nna_api_conv_act_eltwise(NNA_API_CFG_T *p_cfg);

/*! 
* @brief inner production ( FC layer)
* @param[in] p_cfg NNA_API_CFG_T
*
* @snippet net_api_test.cpp api_test_tc_010
*/
void nna_api_inner_product(NNA_API_CFG_T *p_cfg);


/*! 
* @brief ElementWise 
* @param[in] p_cfg NNA_API_CFG_T
*
* @snippet net_api_test.cpp api_test_tc_013
*/
void nna_api_eltwise(NNA_API_CFG_T *p_cfg);

/*! 
* @brief ElementWise with Pad Channel
* @param[in] p_cfg NNA_API_CFG_T
*
* @snippet net_api_test.cpp api_test_tc_032
*/
void nna_api_eltwise_pad_channel(NNA_API_CFG_T *p_cfg);


/*! 
* @brief bias/scale - activation
* @param[in] p_cfg NNA_API_CFG_T
*
* @snippet net_api_test.cpp api_test_tc_014
*/
void nna_api_bias_act(NNA_API_CFG_T *p_cfg);

/*! 
* @brief convolution - mult - activation - ElementWise
* @param[in] p_cfg NNA_API_CFG_T
*
* @snippet net_api_test.cpp api_test_tc_006
*/
void nna_api_conv_mult_act_ew(NNA_API_CFG_T *p_cfg);

/*! 
* @brief convolution - mult - activation - ElementWise - pooling
* @param[in] p_cfg NNA_API_CFG_T
*
* @snippet net_api_test.cpp api_test_tc_007
*/
void nna_api_conv_mult_act_ew_pool(NNA_API_CFG_T *p_cfg);

/*! 
* @brief scale(global register) - Pooling
* @param[in] p_cfg NNA_API_CFG_T
*
* @snippet net_api_test.cpp api_test_tc_005
*/
void nna_api_scale_pool(NNA_API_CFG_T *p_cfg);

/*! 
* @brief mult - ElementWise
* @param[in] p_cfg NNA_API_CFG_T
*
* @snippet net_api_test.cpp api_test_tc_016
*/
void nna_api_mult_eltwise(NNA_API_CFG_T *p_cfg);

/*! 
* @brief bias/scale - activation
* @param[in] p_cfg NNA_API_CFG_T
*
*/
void nna_api_bias(NNA_API_CFG_T *p_cfg);

/*! 
* @brief concat - scale 
* @param[in] p_cfg NNA_API_CFG_T
*
* @snippet net_api_test.cpp api_test_tc_017
*/
void nna_api_concat_scale(NNA_API_CFG_T *p_cfg);

/*! 
* @brief convert 3-ch image to 8-ch FM format (channelwise zero-padding)
* @param[in] p_cfg NNA_API_CFG_T
*
* @snippet net_api_test.cpp api_test_tc_021
*/
void nna_api_img2bgr(NNA_API_CFG_T *p_cfg);

/*! 
* @brief depthwise convolution - bias/scale - activation
* @param[in] p_cfg NNA_API_CFG_T
*
* @snippet net_api_test.cpp api_test_tc_024
*/
void nna_api_dw_conv_act(NNA_API_CFG_T *p_cfg);

/*! 
* @brief Group norm - bias/scale - activation
* @param[in] p_cfg NNA_API_CFG_T
* @param[in] fm_addr_vir base address of FM(feature map) memory
*
* @snippet net_api_test.cpp api_test_tc_026
*/
void nna_api_gn_bias_act(NNA_API_CFG_T *p_cfg, void * fm_addr_vir);


/*!
* @brief Group norm - bias/scale - activation
* @param[in] p_cfg NNA_API_CFG_T
*
* @snippet net_api_test.cpp api_test_tc_030
*/
void nna_api_lrn(NNA_API_CFG_T *p_cfg);

/*! 
* @brief upsample : only nxn copy mode supported
* @param[in] p_cfg NNA_API_CFG_T
*
*/
void nna_api_upsample(NNA_API_CFG_T *p_cfg);
void nna_api_crop(NNA_API_CFG_T *p_cfg);


/*! 
* @brief bias/scale - activation - pooling
* @param[in] p_cfg NNA_API_CFG_T
*
* @snippet net_api_test.cpp api_test_tc_031
*/
void nna_api_bias_act_pool(NNA_API_CFG_T *p_cfg);


void nna_api_cfg_init(NNA_API_CFG_T *p_cfg);


void nna_lock(void); 
void nna_unlock(void); 
int nna_get_init_counter(void);
int nna_set_init_counter(int val);
int nna_get_init_flag(void);
int nna_get_chipid(void);
void nna_get_version(char *v);
int  nna_get_lib_version(void);
int  nna_get_lib_build(void);
int  nna_get_hw_version(void);

void nna_on(void);
void nna_off(void);

#define NNA_ASSERT(expr) \
        if (!(expr)) { fprintf(stderr, "NNA_ASSERT_ERROR: %s() [%s]\n", \
              __FUNCTION__, #expr); exit(1); }


#ifdef __cplusplus
}
#endif


//#include "nna_memory.h"
#include "nna_api_v1.h" 

#ifdef IP_CFG_SLA
#ifdef __cplusplus
extern "C" {
#endif
	int nna_op_run_from_binfile(char *path);
#ifdef __cplusplus
}
#endif
#endif
#endif // _NNA_H_
                                                                                                      
/*
                                                                                                                 
+ -----------------------------------------------------------------  unsigned int output_addr
| +----------------------------------------------------------------  unsigned int input_addr
| | +--------------------------------------------------------------  unsigned int weight_addr
| | | +------------------------------------------------------------  unsigned int bias_addr
| | | | +----------------------------------------------------------  unsigned int input_ew_addr
| | | | |                                                                                                                
| | | | |  +-------------------------------------------------------  int output_shift
| | | | |  | +-----------------------------------------------------  int bias_shift
| | | | |  | |                                                                                                              
| | | | |  | |  +--------------------------------------------------  int img_format
| | | | |  | |  | +------------------------------------------------  int img_mean_op
| | | | |  | |  | |                                                                                                          
| | | | |  | |  | |  +---------------------------------------------  int input_w
| | | | |  | |  | |  | +-------------------------------------------  int input_h
| | | | |  | |  | |  | | +-----------------------------------------  int input_c
| | | | |  | |  | |  | | |                                                                                                        
| | | | |  | |  | |  | | |  +--------------------------------------  int num_output
| | | | |  | |  | |  | | |  | +------------------------------------  int kernel
| | | | |  | |  | |  | | |  | | +----------------------------------  int dilation
| | | | |  | |  | |  | | |  | | | +--------------------------------  int stride
| | | | |  | |  | |  | | |  | | | | +------------------------------  int pad
| | | | |  | |  | |  | | |  | | | | | +----------------------------  int bias_term
| | | | |  | |  | |  | | |  | | | | | |                                                                                                
| | | | |  | |  | |  | | |  | | | | | |  +--------------------------  int act_type
| | | | |  | |  | |  | | |  | | | | | |  | +------------------------  int act_op
| | | | |  | |  | |  | | |  | | | | | |  | |                                                                                
| | | | |  | |  | |  | | |  | | | | | |  | |  +---------------------  int ew_operation
| | | | |  | |  | |  | | |  | | | | | |  | |  | +-------------------  int ew_a_coeff
| | | | |  | |  | |  | | |  | | | | | |  | |  | | +-----------------  int ew_b_coeff
| | | | |  | |  | |  | | |  | | | | | |  | |  | | | +---------------  int ew_b_shift
| | | | |  | |  | |  | | |  | | | | | |  | |  | | | |                                                                       
| | | | |  | |  | |  | | |  | | | | | |  | |  | | | |  +------------  int pool_type
| | | | |  | |  | |  | | |  | | | | | |  | |  | | | |  | +----------  int pool_kernel
| | | | |  | |  | |  | | |  | | | | | |  | |  | | | |  | | +--------  int pool_stride
| | | | |  | |  | |  | | |  | | | | | |  | |  | | | |  | | | +------  int pool_pad
| | | | |  | |  | |  | | |  | | | | | |  | |  | | | |  | | | |                                                              
| | | | |  | |  | |  | | |  | | | | | |  | |  | | | |  | | | |  +---  int mult_operand
| | | | |  | |  | |  | | |  | | | | | |  | |  | | | |  | | | |  | +-  int mult_shift
| | | | |  | |  | |  | | |  | | | | | |  | |  | | | |  | | | |  | |
x x x x -  x x  x x  x x x  x x x x x x  x x  - - - -  - - - -  - -   void nna_api_conv_act(NNA_API_CFG_T *p_cfg)            
x x x x -  x x  x x  x x x  x x x x x x  x x  - - - -  x x x x  - -   void nna_api_conv_act_pool(NNA_API_CFG_T *p_cfg)        
x x x x x  x x  x x  x x x  x x x x x x  x x  x x x x  - - - -  - -   void nna_api_conv_act_eltwise(NNA_API_CFG_T *p_cfg)     
x x x x -  x x  - -  x x x  x - - - - x  - -  - - - -  - - - -  - -   void nna_api_inner_product(NNA_API_CFG_T *p_cfg)        
x x - x -  x x  - -  x x x  - - - - - -  x x  - - - -  - - - -  - -   void nna_api_bias_act(NNA_API_CFG_T *p_cfg)             
x x - - x  x -  - -  x x x  - - - - - -  - -  x x x x  - - - -  - -   void nna_api_eltwise(NNA_API_CFG_T *p_cfg)
x x x x x  x x  x x  x x x  x x x x x x  x x  x x x x  - - - -  x x   void nna_api_conv_mult_act_ew(NNA_API_CFG_T *p_cfg)     
x x x x x  x x  x x  x x x  x x x x x x  x x  x x x x  x x x x  x x   void nna_api_conv_mult_act_ew_pool(NNA_API_CFG_T *p_cfg)
x x - - -  x -  - -  x x x  - - - - - -  - -  - - - -  x x x x  - -   void nna_api_scale_pool(NNA_API_CFG_T *p_cfg)           
x x - - x  x -  - -  x x x  - - - - - -  - -  x x x x  - - - -  x x   void nna_api_mult_eltwise(NNA_API_CFG_T *p_cfg)         
x x - x -  x x  - -  x x x  - - - - - x  - -  - - - -  - - - -  - -   void nna_api_bias(NNA_API_CFG_T *p_cfg)         

*/
