#ifndef _NNA_MEMOTY_H_
#define _NNA_MEMOTY_H_


#ifdef __cplusplus

extern "C"
{

#endif



typedef struct nna_mem_info {
	void * addr_vir;
	unsigned long addr_phy;
	int size;
	int offset;
}NNA_MEM_T;


int nna_mem_open(void);
int nna_mem_close(void);

int nna_mem_alloc(NNA_MEM_T *mem);
int nna_mem_free(NNA_MEM_T *mem);
int nna_mem_flush(NNA_MEM_T *mem);

#ifdef __cplusplus

}

#endif

#endif
