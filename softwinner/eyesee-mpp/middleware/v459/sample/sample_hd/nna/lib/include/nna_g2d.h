#ifndef _NNA_G2D_H_
#define _NNA_G2D_H_

#include "nna_memory.h"

#ifdef __cplusplus

extern "C"
{

#endif

typedef enum {
	NNA_G2D_FORMAT_ARGB8888,
	NNA_G2D_FORMAT_ABGR8888,
	NNA_G2D_FORMAT_RGBA8888,
	NNA_G2D_FORMAT_BGRA8888,
	NNA_G2D_FORMAT_XRGB8888,
	NNA_G2D_FORMAT_XBGR8888,
	NNA_G2D_FORMAT_RGBX8888,
	NNA_G2D_FORMAT_BGRX8888,
	NNA_G2D_FORMAT_RGB888,
	NNA_G2D_FORMAT_BGR888,
	NNA_G2D_FORMAT_RGB565,
	NNA_G2D_FORMAT_BGR565,
	NNA_G2D_FORMAT_ARGB4444,
	NNA_G2D_FORMAT_ABGR4444,
	NNA_G2D_FORMAT_RGBA4444,
	NNA_G2D_FORMAT_BGRA4444,
	NNA_G2D_FORMAT_ARGB1555,
	NNA_G2D_FORMAT_ABGR1555,
	NNA_G2D_FORMAT_RGBA5551,
	NNA_G2D_FORMAT_BGRA5551,
	NNA_G2D_FORMAT_ARGB2101010,
	NNA_G2D_FORMAT_ABGR2101010,
	NNA_G2D_FORMAT_RGBA1010102,
	NNA_G2D_FORMAT_BGRA1010102,

	/* invailed for UI channel */
	NNA_G2D_FORMAT_IYUV422_V0Y1U0Y0 = 0x20,
	NNA_G2D_FORMAT_IYUV422_Y1V0Y0U0,
	NNA_G2D_FORMAT_IYUV422_U0Y1V0Y0,
	NNA_G2D_FORMAT_IYUV422_Y1U0Y0V0,

	NNA_G2D_FORMAT_YUV422UVC_V1U1V0U0,
	NNA_G2D_FORMAT_YUV422UVC_U1V1U0V0,
	NNA_G2D_FORMAT_YUV422_PLANAR,

	NNA_G2D_FORMAT_YUV420UVC_V1U1V0U0 = 0x28,
	NNA_G2D_FORMAT_YUV420UVC_U1V1U0V0,
	NNA_G2D_FORMAT_YUV420_PLANAR,

	NNA_G2D_FORMAT_YUV411UVC_V1U1V0U0 = 0x2c,
	NNA_G2D_FORMAT_YUV411UVC_U1V1U0V0,
	NNA_G2D_FORMAT_YUV411_PLANAR,

	NNA_G2D_FORMAT_Y8 = 0x30,

	/* YUV 10bit format */
	NNA_G2D_FORMAT_YVU10_P010 = 0x34,

	NNA_G2D_FORMAT_YVU10_P210 = 0x36,

	NNA_G2D_FORMAT_YVU10_444 = 0x38,
	NNA_G2D_FORMAT_YUV10_444 = 0x39,
	NNA_G2D_FORMAT_MAX,
} NNA_G2D_FORMAT_ENUM;

typedef struct {
	int left;
	int top;
	int right; 
	int bottom;
} NNA_G2D_BOX_T;

typedef struct {
	int x;
	int y;
	int w;
	int h; 
} NNA_G2D_RECT_T;

typedef struct {
	int batch;
	int channels;
	int width;
	int height;
	int format;
	unsigned long addr_phy[3];
	void *addr_vir[3];
	int ppts;
} NNA_G2D_FRAME_T;

int nna_g2d_open(void);
int nna_g2d_close(void);


int nna_g2d_frame_cpy(NNA_G2D_FRAME_T *dst, NNA_G2D_FRAME_T *src, int rot_angle);
int nna_g2d_mem_cpy(unsigned long dst_addr_phy, unsigned long src_addr_phy, int size);
int nna_g2d_frame_init(NNA_G2D_FRAME_T *frame, NNA_MEM_T *mem, int w,int h, int format);
int nna_g2d_frame_resize(NNA_G2D_FRAME_T *dst, NNA_G2D_FRAME_T *src, NNA_G2D_RECT_T *rect, int rect_num);
int nna_g2d_frame_bitblt(NNA_G2D_FRAME_T *dst, NNA_G2D_RECT_T *dst_rect, NNA_G2D_FRAME_T *src, NNA_G2D_RECT_T *src_rect);

//int nna_g2d_cpy();

int nna_g2d_resize_y8_list(
				unsigned int dst_addr, 	int dstw, int dsth,
				unsigned int src_addr, 	int srcw, int srch,
				unsigned int *box_list,  int box_num);


//list order: [left top right bottom]
int nna_g2d_resize_format_list(
				unsigned int dst_addr, int dstw, int dsth, int dst_format,
				unsigned int src_addr, int srcw, int srch, int src_format,
				unsigned int *box_list, int box_num); 

void nna_g2d_resize_once_format(
				unsigned int dst_addr, int dstw, int dsth, int dst_format, 
				unsigned int src_addr, int srcw, int srch, int src_format, 
			    int left, int top, int right, int bottom);


int nna_g2d_resize_y8(
				unsigned int dst_addr, 	int dstw, int dsth,
				unsigned int src_addr, 	int srcw, int srch,
				NNA_G2D_BOX_T *boxs,  int box_num);


int nna_g2d_resize_format(
				unsigned int dst_addr, int dstw, int dsth, int dst_format,
				unsigned int src_addr, int srcw, int srch, int src_format,
				NNA_G2D_BOX_T *boxs, int box_num); 


void nna_rgb2y(unsigned char * y_img, unsigned char * rgb_img, int w, int h);
void nna_bgr2y(unsigned char * y_img, unsigned char * rgb_img, int w, int h);
int nna_rgb2yuv(unsigned char* yuvBuf, unsigned char* RgbBuf, unsigned int nWidth, unsigned int nHeight);  
int nna_rgb2yuv_swap_uv(unsigned char* yuvBuf, unsigned char* RgbBuf, unsigned int nWidth, unsigned int nHeight);  


#ifdef __cplusplus

}

#endif

#endif
