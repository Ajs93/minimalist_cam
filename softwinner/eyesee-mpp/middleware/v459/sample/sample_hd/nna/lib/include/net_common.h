#ifndef _NET_COMMON_H_
#define _NET_COMMON_H_

#include "nna.h"
#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif


typedef struct net_file_info{
	char name[256];
	int size;
	int offset;
}NNA_FILE_T;

typedef struct net_comm_api{
	void *api;
	int num_param;
}NNA_NET_API_T;

typedef struct net_comm_api_parm{
	int params[26];
}NNA_NET_API_PARAM_T;

typedef struct net_comm_info{
	NNA_MEM_T fm_mem;
	NNA_MEM_T wt_mem;
	char wt_file[1024];

	int dbg_dump;
	int dbg_diff;
	NNA_NET_API_T *net_api;
	NNA_API_CFG_T *net_api_cfg;
	NNA_NET_API_PARAM_T *net_param;
	NNA_FILE_T *fm_files;

}NNA_NET_T;


void net_comm_init(NNA_NET_T *net);
void net_comm_open(void);
void net_comm_close(void);

int net_file_read(char *path, NNA_MEM_T *mem_buf);

typedef struct net_image {
    int w;
    int h;
    int c;
    NNA_G2D_FORMAT_ENUM fmt;
} NET_IMG_T;

typedef struct net_blob {
    int n;
    int c;
    int h;
    int w;
    int mem_size;
    int q2f_bits;
    char bin_file[256];
} NET_BLOB_T;


typedef struct net_box {
    int x1;
    int y1;
    int x2;
    int y2;
} NET_BOX_T;

typedef struct net_box_f {
    float x;
    float y;
    float w;
    float h;
} NET_BOX_F_T;

typedef enum net_out_fmt {
    NET_OUT_FMT_CLS    = 0,
    NET_OUT_FMT_DET    = 1,
    NET_OUT_FMT_ID     = 2,    
    NET_OUT_FMT_VECTOR = 3
} NET_OUT_FMT_E;
   
typedef enum net_img_pre_proc_type {
    SUB_MEAN,
    SUB_MEAN_SCALE,
    TRUNCATION,
} NET_IMG_PRE_PROC_TYPE_E;

typedef enum net_img_cvtcolor_type {
	NONE,
    RGB2BGR,
} NET_IMG_CVTCOLOR_TYPE_E;

typedef struct net_pre_proc_param {    
    NET_IMG_CVTCOLOR_TYPE_E cvtcolor_type;
    NET_IMG_PRE_PROC_TYPE_E prep_type;
    int mean[4];
    int scale[4];
    int truncate[4];
} NET_PRE_PROC_PARAM_T;

typedef struct net_cfg_cls {
    int total_cls;
    char **cls_name;
    int top_k;
} NET_CFG_CLS_T;

typedef struct net_out_data_cls {
    int id;
    float prob;
} NET_OUT_DATA_CLS_T;

typedef struct net_out_data_id {
    int id_len;
	int id_is_float;
} NET_OUT_DATA_ID_T;

typedef struct net_cfg_det {
    int total_det_cls;
    char **det_cls_name;
    int max_det_num;
    int img_w;
    int img_h;
    float  conf_thres;
    float  nms_thres;
} NET_CFG_DET_T;

typedef struct net_out_data_det {
    int id;    
    int prob;
    NET_BOX_T bbox;    
} NET_OUT_DATA_DET_T;

typedef struct net_out_buf_det {
    int num;
    NET_OUT_DATA_DET_T *p_data;
} NET_OUT_BUF_DET_T;

typedef struct net_out_buf {
    int num;
    void *p_data;
} NET_OUT_BUF_T;

typedef struct net_qurey {
	uint32_t    fm_size;
	uint32_t    wt_bs_size;
	uint32_t    preproc_buf_size;
	uint32_t    pstproc_buf_size;
	uint8_t     version[64];
    uint8_t     wt_md5sum[256];
    NET_IMG_T img_param;
    NET_OUT_FMT_E out_fmt;    
} NET_QUREY_T;

typedef struct net_init_t
{
    NNA_MEM_T mem_fm;
    NNA_MEM_T mem_wt;
    void* mem_temp;
	char wt_bs_path[256];
} NET_INIT_T;

int net_img_to_nna(char* nna_img, const char* int8_img, int c, int h, int w, bool is_img_mode);
int net_get_nna_img_size(int c, int h, int w, bool is_img_mode);
void net_nna2hwc(float* out,signed char* fm,  int h, int w, int c,float shift);

#ifdef __cplusplus
}
#endif



#endif // _NET_COMMON_H_
