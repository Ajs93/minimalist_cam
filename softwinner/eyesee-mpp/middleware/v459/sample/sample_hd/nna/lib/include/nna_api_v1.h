#ifndef _NNA_API_V1_H_
#define _NNA_API_V1_H_


#define NNA_NULL_ADDR			0xFFFFFFFF


typedef enum nna_freq {
	NNA_CLK_100M   	= 100,
    NNA_CLK_200M   	= 200,
    NNA_CLK_300M   	= 300,
    NNA_CLK_400M   	= 400,
    NNA_CLK_600M   	= 600,
//    NNA_CLK_1200M  	= 1200,
} nna_freq;

typedef struct nna_init_t
{
	nna_freq	nna_clock;
}NNA_INIT_T;


#ifdef __cplusplus
extern "C"
{
#endif
// convolution + act + pooling
void nna_conv_act_pool(
    unsigned int output_addr,   // memory address of output data
    unsigned int input_addr,    // memory address of input data
    unsigned int weight_addr,   // memory address of weight data
    unsigned int bias_addr,     // memory address of bias data

    int output_shift,           // [alu_shift<<8|mul_trunc] (output_data<<alu_shift)>>mul_trunc
    int bias_shift,             // [alu_shift<<8|mul_trunc] (input_data+(bias data<<alu_shift input))>>mul_trunc
    int img_format,             // [pix_format<<8|img_mode]
    int img_mean_op,            // [A<<24|B<<16|G<<8|R_Y]
    int input_w,                // input data width
    int input_h,                // input data height
    int input_c,                // input data channel

    int num_output,             // number of output data channel
    int kernel,                 // kernel size
    int dilation,               // kernel dilation size
    int stride,                 // kernel stride size
    int pad,                    // pading size
    int bias_term,              // bias term 0:no bias 1: bias 2: batch normalization
    int act_type,               // 0: not act 1:ReLU 2:PreLU 3:sigmoid 4:swish
    int act_op,                 // prelu[scale<<8|trunc] coef = scale/(2^trunc)

    int pool_type,              // pooling type 0:avg 1:max, 1:min
    int pool_kernel,            // pooling kernel size
    int pool_stride,            // pooling stride
    int pool_pad                // pooling pad size
);

// convolution + act
void nna_conv_act(
    unsigned int output_addr,   // memory address of output data
    unsigned int input_addr,    // memory address of input data
    unsigned int weight_addr,   // memory address of weight data
    unsigned int bias_addr,     // memory address of bias data

    int output_shift,           // [alu_shift<<8|mul_trunc] (output_data<<alu_shift)>>mul_trunc
    int bias_shift,             // [alu_shift<<8|mul_trunc] (input_data+(bias_data<<alu_shift))>>mul_trunc
    int img_format,             // [pix_format<<8|img_mode]
    int img_mean_op,            // [A<<24|B<<16|G<<8|R_Y]
    int input_w,                // input data width
    int input_h,                // input data height
    int input_c,                // input data channel

    int num_output,             // number of output data channel
    int kernel,                 // kernel size
    int dilation,               // kernel dilation size
    int stride,                 // kernel stride size
    int pad,                    // pading size
    int bias_term,              // bias term 0:no bias 1: bias 2: batch normalization
    int act_type,               // 0: not act 1:ReLU 2:PreLU 3:sigmoid 4:swish
    int act_op                  // prelu[scale<<8|trunc] coef = scale/(2^trunc)
);

// convolution + act + eltwise
void nna_conv_act_eltwise(
    unsigned int output_addr,   // memory address of output data
    unsigned int input_addr,    // memory address of input data
    unsigned int input_b_addr,  // memory address of input data A  output_data = A*B or A+B or MAX(A,B)
    unsigned int weight_addr,   // memory address of weight data
    unsigned int bias_addr,     // memory address of bias data

    int output_shift,           // [alu_shift<<8|mul_trunc] (output_data<<alu_shift)>>mul_trunc
    int bias_shift,             // [alu_shift<<8|mul_trunc] (input_data+(bias_data<<alu_shift))>>mul_trunc
    int img_format,             // [pix_format<<8|img_mode]
    int img_mean_op,            // [A<<24|B<<16|G<<8|R_Y]
    int input_w,                // input data width
    int input_h,                // input data height
    int input_c,                // input data channel

    int num_output,             // number of output data channel
    int kernel,                 // kernel size
    int dilation,               // kernel dilation size
    int stride,                 // kernel stride size
    int pad,                    // pading size
    int bias_term,              // bias term 0:no bias 1: bias 2: batch normalization
    int act_type,               // 0: not act 1:ReLU 2:PreLU 3:sigmoid 4:swish
    int act_op,                 // prelu[scale<<8|trunc] coef = scale/(2^trunc)

    int operation,              // 0:PROD, 1:SUM, 2:MAX
    int a_coeff,                //  1  only is 1
    int b_coeff                 // -1:A-B 1:A+B
);



// pooling
void nna_pool(
    unsigned int output_addr,   // memory address of output data
    unsigned int input_addr,    // memory address of input data

    int input_w,                // input data width
    int input_h,                // input data height
    int input_c,                // input data channel

    int pool_type,              // 0:avg 1:max, 1:min
    int kernel,                 // pooling kernel size
    int stride,                 // pooling setide size
    int pad,                    // pooling pading size
    int pad_value,              // pooling pading value
    int pad_mode                // 0:full, 1:valid, 2:same
);


// eltwise product or sum
void nna_eltwise(
    unsigned int output_addr,   // memory address of output data
    unsigned int input_a_addr,  // memory address of input data A  output_data = A*B or A+B or MAX(A,B)
    unsigned int input_b_addr,  // memory address of input data A  output_data = A*B or A+B or MAX(A,B)

    int output_shift,           // truncate
    int input_w,                // input data width
    int input_h,                // input data height
    int input_c,                // input data channel

    int operation,              // 0:PROD, 1:SUM, 2:MAX
    int a_coeff,                // 1:  only is 1
    int b_coeff                 // -1:A-B 1:A+B
);

// detection box
struct nna_fd_bbox {
    int score;                  // bbox score
    int x1;                     // bbox left
    int y1;                     // bbox top
    int x2;                     // bbox right
    int y2;                     // bbox bottom
};

// detection box
struct nna_face_bbox {
    int score;                  // bbox score
    int x1;                     // bbox left
    int y1;                     // bbox top
    int x2;                     // bbox right
    int y2;                     // bbox bottom
    int point[10];
};

void nna_reinit(NNA_INIT_T	*init_param);


// NNA initiation
int nna_init(
    NNA_INIT_T	*init_param     // nna initialization parameter
    );

// NNA close
int nna_close(void);                // unmmap the user space address & close the NNA


//NNA Face Detection run
int nna_fd_run(                 // return the bbox number
    char * img,                 // input image data of rgb format
    int w,                      // input image width  w<=1920
    int h,                      // input image height h<=1080
    struct nna_fd_bbox  *bbox,  // output the bbox buffer
    int max_num_bbox             // bbox buffer max size
    );

int nna_diff_file(char *path, char *path2);
int nna_save_ram(unsigned int  target, int file_sz, char *path);
int nna_load_ram(unsigned int  target, char *path);

int  nna_load_buf(void * buf, int buf_size, char * path);
int  nna_save_buf(void * buf, int buf_size, char * path);
int  nna_diff_file(char * path_x, char * path_y);


typedef void (*NNA_RESET_FUNC)(void);
typedef void (*NNA_RESIZE_FUNC)(unsigned int dst_addr, int dstw, int dsth, unsigned int src_addr, int srcw, int srch, unsigned int *box_list, int box_num);
typedef void (*NNA_RESIZE_FORMAT_FUNC)(unsigned int dst_addr, int dstw, int dsth, int dst_format, unsigned int src_addr, int srcw, int srch, int src_format, unsigned int *box_list, int box_num); 

typedef void (*NNA_WAIT_EVENT_FUNC)(void);

// physical address for NNA used input & output
void nna_setup_buffer(unsigned int buf_base_nna, uint64_t buf_base_cpu,int buf_size);
void nna_setup_reset_handler(NNA_RESET_FUNC func);
void nna_setup_resize_handler(NNA_RESIZE_FUNC func);
void nna_setup_resize_format_handler(NNA_RESIZE_FORMAT_FUNC func);

void nna_setup_wait_event_handler(NNA_WAIT_EVENT_FUNC func);
void nna_clean_interrupt(void);
int nna_get_status(void);

int nna_wait_event_done(void);

void nna_resize(
    unsigned int dst_addr,
    int dstw,
    int dsth,
    unsigned int src_addr,
    int srcw,
    int srch,
    unsigned int *box_list,      //x1 y1 x2 y2
    int box_num
    );


void nna_reset(void );


unsigned int nna_get_buffer_viraddr(unsigned int buf_phyaddr);

void nna_buffer_get_cfg(NNA_MEM_T *mem_buf);
void nna_buffer_set_cfg(NNA_MEM_T *mem_buf);
void nna_fd_hd_mem_init(NNA_MEM_T *mem_buf);


void nna_fd_set_factor(int factor_level);
void nna_fd_set_minface(int minsize);
void nna_fd_set_threshold(int *score_threshold_levels, int *nms_threshold_levels);


int nna_face_detect_norefine(char * img, int w, int h, struct nna_face_bbox	*bbox, int max_num_bbox);
int nna_face_detect(char * img, int w, int h, struct nna_face_bbox	*bbox, int max_num_bbox);
void nna_fd1_big(unsigned int out_addr, unsigned int in_addr,int score_size);
void nna_fd1(unsigned int out_addr, unsigned  int in_addr, int w,int h);
void nna_fd2(unsigned int out_addr, unsigned int in_addr, int num_img);
void nna_fd3(unsigned int out_addr, unsigned int in_addr, int num_img);
void nna_hd_ddr_rom(unsigned int out_addr, unsigned int in_addr);


void nna_hd(unsigned int out_addr, unsigned int in_addr);
void nna_hd_tail(unsigned int out_addr, unsigned int in_addr, unsigned int in_b_addr);
int  nna_nms(unsigned int out_addr, unsigned int in_addr, int box_num, int mode, int nms_threshold_level);
// mode 			   : 0/1 ,Union/min 
// nms_threshold_level : 0~100 ,0.1~1.0
// in_addr	data format: score of box ,it order is from big to small, if box num=100, int input[4*100]; input[0]/input[1]/input[2]/input[3]:left/top/right/bottom
// out_addr data format: box index : score of box ,it order is from big to small

#ifdef __cplusplus
}
#endif

#endif

