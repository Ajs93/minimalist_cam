#ifndef __SL_NET_HD_H__
#define __SL_NET_HD_H__

#include "net_common.h" 

#ifdef __cplusplus
extern "C"
{
#endif

int net_hd_query(NET_QUREY_T *pqurey_param);

int net_hd_init(NET_INIT_T *pinit_param);
int net_hd_close(void);

int net_hd_get_preprocess_cfg(void *p_cfg);
int net_hd_set_preprocess_cfg(void *p_cfg);
int net_hd_preprocess(NNA_MEM_T *p_dst, NNA_MEM_T *p_src, void *p_temp);

// int net_hd_run(void *p_dst);
int net_hd_run(uint8_t  *p_dst);

int net_hd_get_pstprocess_cfg(void *p_cfg);
int net_hd_set_pstprocess_cfg(void *p_cfg);
// int net_hd_pstprocess(void *p_dst, void *p_src, void *p_temp);
int net_hd_pstprocess( uint8_t *p_dst, struct nna_fd_bbox *p_out, int * box_num, float conf_thres, float nms_thres);

#ifdef __cplusplus
}
#endif

#endif // __NET_HD_H__

