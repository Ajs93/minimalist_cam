#ifndef _NNA_PLATFORM_H_
#define _NNA_PLATFORM_H_


#define MAP_SIZE 		(64*1024UL)
#define NNA_CLK_RST_REG	0x03001000


#ifdef __cplusplus
extern "C"
{
#endif

typedef enum nna_cmd
{
    NNA_CMD_ENABLE_IRQ	= 0x100,
    NNA_CMD_DISABLE_IRQ	= 0x101,
	NNA_CMD_SET_CLOCK  	= 0x102,
    NNA_CMD_RESET_NNA  	= 0x103,
    NNA_CMD_SHUT_NNA    = 0x104,
    NNA_CMD_OPEN_NNA    = 0x105,
} nna_cmd;

typedef enum clock_freq {
	NNA_CLOCK_100M   	= 100,
    NNA_CLOCK_200M   	= 200,
    NNA_CLOCK_300M   	= 300,
    NNA_CLOCK_400M   	= 400,
    NNA_CLOCK_600M   	= 600,
    NNA_CLOCK_800M   	= 800,
    NNA_CLOCK_1200M  	= 1200,
} clock_freq;


void nna_reset_hw(void);
int	platform_nna_setting( nna_cmd cmd, clock_freq clk = NNA_CLOCK_400M );


#ifdef __cplusplus
}
#endif

#endif
