#ifndef _SL_NET_MOTION_H_
#define _SL_NET_MOTION_H_

#include "net_common.h" 
#include "nna_g2d.h"

#ifdef __cplusplus
extern "C"
{
#endif


typedef enum {
	NIGHTVIEW_NIGHT = 0,
	NIGHTVIEW_DAY   = 1

} NIGHTVIEW_E;

typedef enum {
	SENSITIVITY_LEVEL_0 = 0,
	SENSITIVITY_LEVEL_1 = 1,
	SENSITIVITY_LEVEL_2 = 2
} SENSITIVITY_LEVEL_E;




int net_motion_init( const uint32_t width, const uint32_t height);

int net_motion_uninit(void);

int net_motion_run(NNA_G2D_FRAME_T *img, NNA_G2D_RECT_T *roi, int *resulet );

int net_motion_set_nightview(NIGHTVIEW_E mode);

int net_motion_set_sensitivity(SENSITIVITY_LEVEL_E level);


#ifdef __cplusplus
}
#endif


#endif
