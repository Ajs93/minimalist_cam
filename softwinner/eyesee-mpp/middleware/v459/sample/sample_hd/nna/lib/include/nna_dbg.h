#ifndef  _NNA_DBG_H_
#define  _NNA_DBG_H_


#define __DEBUG__
#ifdef  __DEBUG__

//debug<info<warn<Error<Fatal

#define D_ERR(fmt, args...) printf("[%s:%s:%d]" fmt" \r\n",__FILE__, __func__, __LINE__,  ##args)
#define D_INFO(fmt, args...) printf("[%s:%d]" fmt"  \r\n", __func__, __LINE__, ##args)
#define D_MSG(fmt, args...) printf(fmt"  \r\n",  ##args)
#define D_LINE() printf("[%s:%s:%d] \r\n",__FILE__, __func__, __LINE__)
#else
#define D_LINE()
#define D_ERR(fmt, ...)
#define D_INFO(fmt, ...)
#define D_MSG(fmt, ...)

#endif




#endif

