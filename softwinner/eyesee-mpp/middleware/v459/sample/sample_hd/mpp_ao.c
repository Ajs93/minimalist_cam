#include "mpp_ao.h"

// --------------------------------------------------------------------------------
// AO 
// these code comes from sample_ao
// --------------------------------------------------------------------------------

AUDIO_FRAME_S* AOFrameManager_PrefetchFirstIdleFrame(void *pThiz)
{
    AOFrameManager *pFrameManager = (AOFrameManager*)pThiz;
    AOFrameNode *pFirstNode;
    AUDIO_FRAME_S *pFrameInfo;
    pthread_mutex_lock(&pFrameManager->mLock);
    if(!list_empty(&pFrameManager->mIdleList))
    {
        pFirstNode = list_first_entry(&pFrameManager->mIdleList, AOFrameNode, mList);
        pFrameInfo = &pFirstNode->mAFrame;
    }
    else
    {
        pFrameInfo = NULL;
    }
    pthread_mutex_unlock(&pFrameManager->mLock);
    return pFrameInfo;
}

int AOFrameManager_UseFrame(void *pThiz, AUDIO_FRAME_S *pFrame)
{
    int ret = 0;
    AOFrameManager *pFrameManager = (AOFrameManager*)pThiz;
    if(NULL == pFrame)
    {
        aloge("fatal error! pNode == NULL!");
        return -1;
    }
    pthread_mutex_lock(&pFrameManager->mLock);
    AOFrameNode *pFirstNode = list_first_entry_or_null(&pFrameManager->mIdleList, AOFrameNode, mList);
    if(pFirstNode)
    {
        if(&pFirstNode->mAFrame == pFrame)
        {
            list_move_tail(&pFirstNode->mList, &pFrameManager->mUsingList);
        }
        else
        {
            aloge("fatal error! node is not match [%p]!=[%p]", pFrame, &pFirstNode->mAFrame);
            ret = -1;
        }
    }
    else
    {
        aloge("fatal error! idle list is empty");
        ret = -1;
    }
    pthread_mutex_unlock(&pFrameManager->mLock);
    return ret;
}

int AOFrameManager_ReleaseFrame(void *pThiz, unsigned int nFrameId)
{
    int ret = 0;
    AOFrameManager *pFrameManager = (AOFrameManager*)pThiz;
    pthread_mutex_lock(&pFrameManager->mLock);
    int bFindFlag = 0;
    AOFrameNode *pEntry, *pTmp;
    list_for_each_entry_safe(pEntry, pTmp, &pFrameManager->mUsingList, mList)
    {
        if(pEntry->mAFrame.mId == nFrameId)
        {
            list_move_tail(&pEntry->mList, &pFrameManager->mIdleList);
            bFindFlag = 1;
            break;
        }
    }
    if(0 == bFindFlag)
    {
        aloge("fatal error! frameId[%d] is not find", nFrameId);
        ret = -1;
    }
    pthread_mutex_unlock(&pFrameManager->mLock);
    return ret;
}

int initAOFrameManager(AOFrameManager *pFrameManager, int nFrameNum, AOConfig *pConfigPara)
{
    memset(pFrameManager, 0, sizeof(AOFrameManager));
    int err = pthread_mutex_init(&pFrameManager->mLock, NULL);
    if(err!=0)
    {
        aloge("fatal error! pthread mutex init fail!");
    }
    INIT_LIST_HEAD(&pFrameManager->mIdleList);
    INIT_LIST_HEAD(&pFrameManager->mUsingList);

    int i;
    AOFrameNode *pNode;
    for (i=0; i<nFrameNum; i++)
    {
        pNode = malloc(sizeof(AOFrameNode));
        memset(pNode, 0, sizeof(AOFrameNode));
        pNode->mAFrame.mId = i;
        pNode->mAFrame.mBitwidth = (AUDIO_BIT_WIDTH_E)(pConfigPara->mBitWidth/8 - 1);
        pNode->mAFrame.mSoundmode = (pConfigPara->mChannelCnt==1)?AUDIO_SOUND_MODE_MONO:AUDIO_SOUND_MODE_STEREO;
        pNode->mAFrame.mLen = pConfigPara->mChannelCnt * pConfigPara->mBitWidth/8 * pConfigPara->mFrameSize;
        pNode->mAFrame.mpAddr = malloc(pNode->mAFrame.mLen);
        list_add_tail(&pNode->mList, &pFrameManager->mIdleList);
    }
    pFrameManager->mNodeCnt = nFrameNum;

    pFrameManager->PrefetchFirstIdleFrame = AOFrameManager_PrefetchFirstIdleFrame;
    pFrameManager->UseFrame = AOFrameManager_UseFrame;
    pFrameManager->ReleaseFrame = AOFrameManager_ReleaseFrame;
    return 0;
}

int destroyAOFrameManager(AOFrameManager *pFrameManager)
{
    if(!list_empty(&pFrameManager->mUsingList))
    {
        aloge("fatal error! why using list is not empty");
    }
    int cnt = 0;
    struct list_head *pList;
    list_for_each(pList, &pFrameManager->mIdleList)
    {
        cnt++;
    }
    if(cnt != pFrameManager->mNodeCnt)
    {
        aloge("fatal error! frame count is not match [%d]!=[%d]", cnt, pFrameManager->mNodeCnt);
    }
    AOFrameNode *pEntry, *pTmp;
    list_for_each_entry_safe(pEntry, pTmp, &pFrameManager->mIdleList, mList)
    {
        free(pEntry->mAFrame.mpAddr);
        list_del(&pEntry->mList);
        free(pEntry);
    }
    pthread_mutex_destroy(&pFrameManager->mLock);
    return 0;
}

int initAOContext(AO_Params *pContext)
{
    //memset(pContext, 0, sizeof(AO_Params));
    int err = pthread_mutex_init(&pContext->mWaitFrameLock, NULL);
    if(err!=0)
    {
        aloge("fatal error! pthread mutex init fail!");
    }
    err = cdx_sem_init(&pContext->mSemFrameCome, 0);
    err = cdx_sem_init(&pContext->mSemEofCome, 0);
    if(err!=0)
    {
        aloge("cdx sem init fail!");
    }
    return 0;
}

int destroyAOContext(AO_Params *pContext)
{
    pthread_mutex_destroy(&pContext->mWaitFrameLock);
    cdx_sem_deinit(&pContext->mSemFrameCome);
    cdx_sem_deinit(&pContext->mSemEofCome);
    return 0;
}

static ERRORTYPE AOCallbackWrapper(void *cookie, MPP_CHN_S *pChn, MPP_EVENT_TYPE event, void *pEventData)
{
    ERRORTYPE ret = SUCCESS;
    AO_Params *pContext = (AO_Params*)cookie;
    if(MOD_ID_AO == pChn->mModId)
    {
        if(pChn->mChnId != pContext->mAOChn)
        {
            //aloge("fatal error! AO chnId[%d]!=[%d]", pChn->mChnId, pContext->mAOChn);
            printf("fatal error! AO chnId[%d]!=[%d]\n", pChn->mChnId, pContext->mAOChn);
        }
        switch(event)
        {
            case MPP_EVENT_RELEASE_AUDIO_BUFFER:
            {
                AUDIO_FRAME_S *pAFrame = (AUDIO_FRAME_S*)pEventData;
                pContext->mFrameManager.ReleaseFrame(&pContext->mFrameManager, pAFrame->mId);
                pthread_mutex_lock(&pContext->mWaitFrameLock);
                if(pContext->mbWaitFrameFlag)
                {
                    pContext->mbWaitFrameFlag = 0;
                    cdx_sem_up(&pContext->mSemFrameCome);
                }
                pthread_mutex_unlock(&pContext->mWaitFrameLock);
                break;
            }
            case MPP_EVENT_NOTIFY_EOF:
            {
                //alogd("AO channel notify APP that play complete!");
                printf("AO channel notify APP that play complete!\n");
                cdx_sem_signal(&pContext->mSemEofCome);
                break;
            }
            default:
            {
                //postEventFromNative(this, event, 0, 0, pEventData);
                //aloge("fatal error! unknown event[0x%x] from channel[0x%x][0x%x][0x%x]!", event, pChn->mModId, pChn->mDevId, pChn->mChnId);
                printf("fatal error! unknown event[0x%x] from channel[0x%x][0x%x][0x%x]!\n", event, pChn->mModId, pChn->mDevId, pChn->mChnId);
                ret = ERR_AO_ILLEGAL_PARAM;
                break;
            }
        }
    }
    else
    {
        //aloge("fatal error! why modId[0x%x]?", pChn->mModId);
        printf("fatal error! why modId[0x%x]?\n", pChn->mModId);
        ret = FAILURE;
    }
    return ret;
}

static ERRORTYPE AO_CLOCKCallbackWrapper(void *cookie, MPP_CHN_S *pChn, MPP_EVENT_TYPE event, void *pEventData)
{
    alogw("clock channel[%d] has some event[0x%x]", pChn->mChnId, event);
    return SUCCESS;
}

void config_AIO_ATTR_S_by_AOConfig(AIO_ATTR_S *dst, AOConfig *src)
{
    memset(dst, 0, sizeof(AIO_ATTR_S));
    dst->u32ChnCnt = src->mChannelCnt;
    dst->enSamplerate = (AUDIO_SAMPLE_RATE_E)src->mSampleRate;
    dst->enBitwidth = (AUDIO_BIT_WIDTH_E)(src->mBitWidth/8-1);
}


int create_ao(AO_Params* pAOParams)
{
	int result = 0;

	initAOContext(pAOParams);
#if 1	
   	//open pcm file
    pAOParams->mFpPcmFile = fopen(pAOParams->mConfigPara.mPcmFilePath, "rb");
    if(!pAOParams->mFpPcmFile)
    {
        aloge("fatal error! can't open pcm file[%s]", pAOParams->mConfigPara.mPcmFilePath);
        result = -1;
        //goto _exit;
    }
    else
    {
        fseek(pAOParams->mFpPcmFile, 44, SEEK_SET);  // 44: size(WavHeader)
    }
#endif
    //init frame manager
    initAOFrameManager(&pAOParams->mFrameManager, 5, &pAOParams->mConfigPara);

    //enable ao dev
    pAOParams->mAODev = 0;
    config_AIO_ATTR_S_by_AOConfig(&pAOParams->mAIOAttr, &pAOParams->mConfigPara);
    AW_MPI_AO_SetPubAttr(pAOParams->mAODev, &pAOParams->mAIOAttr);
    //embedded in AW_MPI_AO_EnableChn
    //AW_MPI_AO_Enable(pAOParams->mAODev);

    //create ao channel and clock channel.
    ERRORTYPE ret;
    BOOL bSuccessFlag = FALSE;
    pAOParams->mAOChn = 0;
    while(pAOParams->mAOChn < AIO_MAX_CHN_NUM)
    {
        ret = AW_MPI_AO_EnableChn(pAOParams->mAODev, pAOParams->mAOChn);
        if(SUCCESS == ret)
        {
            bSuccessFlag = TRUE;
            alogd("create ao channel[%d] success!", pAOParams->mAOChn);
            break;
        }
        else if (ERR_AO_EXIST == ret)
        {
            alogd("ao channel[%d] exist, find next!", pAOParams->mAOChn);
            pAOParams->mAOChn++;
        }
        else if(ERR_AO_NOT_ENABLED == ret)
        {
            aloge("audio_hw_ao not started!");
            break;
        }
        else
        {
            aloge("create ao channel[%d] fail! ret[0x%x]!", pAOParams->mAOChn, ret);
            break;
        }
    }
    if(FALSE == bSuccessFlag)
    {
        pAOParams->mAOChn = MM_INVALID_CHN;
        aloge("fatal error! create ao channel fail!");
    }
    MPPCallbackInfo cbInfo;
    cbInfo.cookie = (void*)pAOParams;
    cbInfo.callback = (MPPCallbackFuncType)&AOCallbackWrapper;
    AW_MPI_AO_RegisterCallback(pAOParams->mAODev, pAOParams->mAOChn, &cbInfo);

    bSuccessFlag = FALSE;
    pAOParams->mClockChnAttr.nWaitMask = 0;
    pAOParams->mClockChnAttr.nWaitMask |= 1<<CLOCK_PORT_INDEX_AUDIO;
    pAOParams->mClockChn = 0;
    while(pAOParams->mClockChn < CLOCK_MAX_CHN_NUM)
    {
        ret = AW_MPI_CLOCK_CreateChn(pAOParams->mClockChn, &pAOParams->mClockChnAttr);
        if(SUCCESS == ret)
        {
            bSuccessFlag = TRUE;
            alogd("create clock channel[%d] success!", pAOParams->mClockChn);
            break;
        }
        else if(ERR_CLOCK_EXIST == ret)
        {
            alogd("clock channel[%d] is exist, find next!", pAOParams->mClockChn);
            pAOParams->mClockChn++;
        }
        else
        {
            alogd("create clock channel[%d] ret[0x%x]!", pAOParams->mClockChn, ret);
            break;
        }
    }
    if(FALSE == bSuccessFlag)
    {
        pAOParams->mClockChn = MM_INVALID_CHN;
        aloge("fatal error! create clock channel fail!");
    }
    cbInfo.cookie = (void*)pAOParams;
    cbInfo.callback = (MPPCallbackFuncType)&AO_CLOCKCallbackWrapper;
    AW_MPI_CLOCK_RegisterCallback(pAOParams->mClockChn, &cbInfo);
    //bind clock and ao
    MPP_CHN_S ClockChn = {MOD_ID_CLOCK, 0, pAOParams->mClockChn};
    MPP_CHN_S AOChn = {MOD_ID_AO, pAOParams->mAODev, pAOParams->mAOChn};
    AW_MPI_SYS_Bind(&ClockChn, &AOChn);

    //start ao and clock.
    AW_MPI_CLOCK_Start(pAOParams->mClockChn);
    AW_MPI_AO_StartChn(pAOParams->mAODev, pAOParams->mAOChn);
    AW_MPI_AO_SetDevVolume(pAOParams->mAODev, 100);	// 90

    return 0;
}


int destroy_ao(AO_Params* pAOParams)
{
    //stop ao channel, clock channel
    AW_MPI_AO_StopChn(pAOParams->mAODev, pAOParams->mAOChn);
    AW_MPI_CLOCK_Stop(pAOParams->mClockChn);
    AW_MPI_AO_DisableChn(pAOParams->mAODev, pAOParams->mAOChn);
    pAOParams->mAODev = MM_INVALID_DEV;
    pAOParams->mAOChn = MM_INVALID_CHN;
    AW_MPI_CLOCK_DestroyChn(pAOParams->mClockChn);
    pAOParams->mClockChn = MM_INVALID_CHN;
    destroyAOFrameManager(&pAOParams->mFrameManager);
#if 1
    //close pcm file
    fclose(pAOParams->mFpPcmFile);
    pAOParams->mFpPcmFile = NULL;
#endif
    return 0;
}


