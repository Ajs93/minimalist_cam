//
//  XMMute.cpp
//  testMute
//
//  Created by Ethan on 2018/7/12.
//  Copyright © 2018年 Ethan. All rights reserved.
//

#include "XMMute.h"

XMMutex::XMMutex(void)
{
    pthread_mutex_init(&m_mutex, NULL);
}

XMMutex::~XMMutex(void)
{
    pthread_mutex_destroy(&m_mutex);
}

bool XMMutex::Enter(void)
{
    pthread_mutex_lock(&m_mutex);
    return true;
}

bool XMMutex::Leave(void)
{
    pthread_mutex_unlock(&m_mutex);
    return true;
}
