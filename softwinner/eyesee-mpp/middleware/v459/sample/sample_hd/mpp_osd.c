#include "mpp_osd.h"
#include "sample_config.h"

static isCreate = 0;
// #define ENABLE_DRAW_CHARACTER
typedef struct _fontInfo {
	// ascii
    char 	fontPath[64];
    int 	fontWidth;
    int 	fontHeight;
	// gb2312
	char 	gbfontPath[64];
    int 	gbfontWidth;
    int 	gbfontHeight;
} FontInfo;
typedef struct _rgb8888Info {
    char alpha;
    char red;
    char green;
    char blue;
} RGB8888Info;
typedef struct _bmpInfo {
    RGB8888Info *pBmpData;
    int bmpDataLen;
    int bmpWidth;
    int bmpHeight;
    int bmpMode;
} BMPInfo;

int Utf8ToGb2312(char *src_str, size_t src_len, char *dst_str, size_t dst_len)
{
    #ifdef USE_SYSTEM_CHAR_CONVERT
	iconv_t cd;
	char **pin = &src_str;
	char **pout = &dst_str;
    printf("%d----\n", __LINE__);
	cd = iconv_open("GB2312", "UTF-8");
	if ((iconv_t)-1 == cd)
    {
        perror("iconv_open failed!!\n");
        printf("ERR:iconv_open failed!\n");
        return -1;
    }
	printf("%d----cd = %d, dst_len = %d\n", __LINE__, cd, dst_len);
	memset(dst_str, 0, dst_len);
    printf("%d----\n", __LINE__);
	if (-1 == iconv(cd, pin, &src_len, pout, &dst_len))
    {
        printf("ERR:iconv  failed!\n");
        return -1;
    }
    printf("%d----\n", __LINE__);
	iconv_close(cd);
    printf("%d----\n", __LINE__);
	*pout = '\0';
    printf("%d----\n", __LINE__);
    #else
    //printf("%d----\n", __LINE__);
    utf8ToGb2312(dst_str, dst_len, src_str, src_len);
    #endif
	return 0;
}

void convertEngString2Bmpdata(char *textStr, int textLength, FontInfo *pFontInfo, BMPInfo *pBmpInfo)
{
    if (!textStr || !pFontInfo || !pBmpInfo || !pBmpInfo->pBmpData) {
        printf("convertString2Bmpdata Invaild param, param can not NULL!!!!\n");
        return;
    }

    FILE * ap;
    char mat[textLength][pFontInfo->fontWidth/8*pFontInfo->fontHeight];
    if((ap=fopen(pFontInfo->fontPath, "rb")) == NULL) {
        printf("Can't Open ASC %s\n", pFontInfo->fontPath);
        return;
    }

	// GB2312
	FILE * gp;
    if((gp=fopen(pFontInfo->gbfontPath, "rb")) == NULL) {
        printf("Can't Open GB %s\n", pFontInfo->gbfontPath);
        return;
    }	

    if (pBmpInfo->pBmpData) {
        memset(pBmpInfo->pBmpData, 0, pBmpInfo->bmpDataLen);
    }	
	
    int strip = pBmpInfo->bmpWidth;
    for (int m_count = 0; m_count < textLength; m_count++) {
		// GB2312
		if (textStr[m_count] & 0x80) {
			int 	fontW, fontH;
			char*	pFont;
			int fontOffset = 0;
			char chinese[4] = {0};
            chinese[0] = textStr[m_count++];
            chinese[1] = textStr[m_count++];
            chinese[2] = textStr[m_count];
			
			//printf("chinese : %s\n", chinese);
			int perCharSize = pFontInfo->gbfontWidth * pFontInfo->gbfontHeight / 8;
		    unsigned char buffer[perCharSize];
			char word[4] = {0};
			Utf8ToGb2312(chinese, strlen(chinese), word, sizeof(word));
		    unsigned char qh = word[0] - 0xa0;
		    unsigned char wh = word[1] - 0xa0;
			unsigned int offset = (94 * (unsigned int)(qh-1) + (unsigned int)(wh-1)) * perCharSize;
		    fseek(gp, offset, SEEK_SET);
		    fread(buffer, perCharSize, 1, gp);

			fontW = pFontInfo->gbfontWidth;
			fontH = pFontInfo->gbfontHeight;
			pFont = buffer;

	        for (int bitH = 0; bitH < fontH; bitH++) {
	            for (int byteW = 0; byteW < fontW / 8; byteW++) {
	                unsigned char byteValue = pFont[bitH*fontW / 8 + byteW];
	                for (int b = 0; b<8; b++) {						
	                    if (byteValue & (0x80>>b)) {
							//fontOffset = bitH * strip + m_count * pFontInfo->fontWidth + byteW * 8 + b;
							fontOffset = bitH * strip + m_count * 11 + byteW * 8 + b;
							//fontOffset	= (osdY+bitH)*pYchOSDParams->imgWidth + osdX + font_width + byteW * 8 + b;
							//printf("fontOffset = %d, bitH:%d m_count:%d byteW:%d b:%d bmpWidth = %d\n", fontOffset, bitH, m_count, byteW, b, pBmpInfo->bmpWidth);
							RGB8888Info *pRGB = (RGB8888Info *)(pBmpInfo->pBmpData + fontOffset);
	                        pRGB->alpha = 1;
	                        pRGB->red     = 0x00;    // 0x8f;
	                        pRGB->green = 0xFF;    // 0x13;
	                        pRGB->blue     = 0xFF;    // 0xb1;
	                    }
	                }
	            }
				//printf("\n");
	        }
		}else {		
	        int word = textStr[m_count];
	        char mat2[pFontInfo->fontWidth/8*pFontInfo->fontHeight];
	        fseek(ap, word*pFontInfo->fontWidth/8*pFontInfo->fontHeight, SEEK_SET);
	        fread(mat2, pFontInfo->fontWidth/8*pFontInfo->fontHeight, 1, ap);

	        for (int bitH = 0; bitH < pFontInfo->fontHeight; bitH++) {
	            for (int byteW = 0; byteW < pFontInfo->fontWidth/8; byteW++) {
	                unsigned char byteValue = mat2[bitH*pFontInfo->fontWidth/8 + byteW];
	                for (int b = 0; b<8; b++) {
	                    if (byteValue & (1<<b)) { //判断点阵中的各个点是否为1
	                        int index = bitH*strip + m_count*pFontInfo->fontWidth + byteW*8 + (8 - b - 1);
							//printf("index = %d, bitH:%d m_count:%d byteW:%d b:%d bmpWidth = %d\n", index, bitH, m_count, byteW, b, pBmpInfo->bmpWidth);
	                        RGB8888Info *pRGB = (RGB8888Info *)(pBmpInfo->pBmpData + index);
	                        pRGB->alpha = 1;
	                        pRGB->red     = 0x00;    // 0x8f;
	                        pRGB->green = 0xFF;    // 0x13;
	                        pRGB->blue     = 0xFF;    // 0xb1;
	                    }
	                }
	            }
	        }
		}

    }

    if (ap) {
        fclose(ap);
    }
	if (gp) {
        fclose(gp);
    }
}



int		fontWidth 		= 8; //16;			// 32;
int 	fontHeight 		= 16; //32;			// 48;
char 	fontPath[64] 	= "/mnt/UDISK/asc-font";	// "./32x48";
int 	gbfontWidth 	= 32;
int 	gbfontHeight 	= 32;
char 	gbfontPath[64] 	= "/mnt/UDISK/hzk32";
int 	strNum 	= 32;
int sl_create_osd(OSD_Params* pOSDParams, int layerId)
{
    // 初始化区域参数
    int textBufLen = gbfontWidth * gbfontHeight * strNum;
    pOSDParams->pTextBuffer = (unsigned char*)malloc(textBufLen);
    memset(pOSDParams->pTextBuffer, 0, textBufLen);

    // 创建区域对象
    int date_region_w = strNum * gbfontWidth;
    int date_region_h = gbfontHeight;
    pOSDParams->stRegionDate.enType                        = OVERLAY_RGN;
    pOSDParams->stRegionDate.unAttr.stOverlay.mPixelFmt    = MM_PIXEL_FORMAT_RGB_8888;
    pOSDParams->stRegionDate.unAttr.stOverlay.mSize.Width  = date_region_w;
    pOSDParams->stRegionDate.unAttr.stOverlay.mSize.Height = date_region_h;
    AW_MPI_RGN_Create(pOSDParams->mOverlayHandle, &pOSDParams->stRegionDate);

    // 将该区域叠加到viDev和ViCh绑定的通道
    RGN_CHN_ATTR_S stRgnChnAttr2;
    memset(&stRgnChnAttr2, 0, sizeof(RGN_CHN_ATTR_S));
    stRgnChnAttr2.bShow                            = TRUE;
    stRgnChnAttr2.enType                           = pOSDParams->stRegionDate.enType;
    stRgnChnAttr2.unChnAttr.stOverlayChn.stPoint.X = pOSDParams->x;    // 32;
    stRgnChnAttr2.unChnAttr.stOverlayChn.stPoint.Y = pOSDParams->y;    // 32;
    stRgnChnAttr2.unChnAttr.stOverlayChn.mLayer    = layerId;
    AW_MPI_RGN_AttachToChn(pOSDParams->mOverlayHandle,  &pOSDParams->mMppChn, &stRgnChnAttr2);

    // 配置区域对应的位图属性
    memset(&pOSDParams->stBmpDate, 0, sizeof(BITMAP_S));
    pOSDParams->stBmpDate.mPixelFormat = pOSDParams->stRegionDate.unAttr.stOverlay.mPixelFmt;
    pOSDParams->stBmpDate.mWidth       = pOSDParams->stRegionDate.unAttr.stOverlay.mSize.Width;
    pOSDParams->stBmpDate.mHeight      = pOSDParams->stRegionDate.unAttr.stOverlay.mSize.Height;
    int nSizeDate                      = BITMAP_S_GetdataSize(&pOSDParams->stBmpDate);
    pOSDParams->stBmpDate.mpData       = malloc(nSizeDate);
    if (!pOSDParams->stBmpDate.mpData) {
        printf("malloc Bmpbuffer failed!!!!\n");
        abort();
    }

    return 0;
}
int sl_update_osd(OSD_Params* pOSDParams, char* osdString, int len, int layerId)
{
     char str[32] = "AB";
     int textLength = strlen(str);



    FontInfo m_fontInfo;
    memcpy(m_fontInfo.fontPath, fontPath, strlen(fontPath)+1);
    m_fontInfo.fontWidth = fontWidth;
    m_fontInfo.fontHeight = fontHeight;

	memcpy(m_fontInfo.gbfontPath, gbfontPath, strlen(gbfontPath)+1);
    m_fontInfo.gbfontWidth	= gbfontWidth;
    m_fontInfo.gbfontHeight	= gbfontHeight;

    BMPInfo m_BmpInfo;
    m_BmpInfo.bmpWidth = pOSDParams->stBmpDate.mWidth;
    m_BmpInfo.bmpHeight = pOSDParams->stBmpDate.mHeight;
    m_BmpInfo.bmpDataLen = BITMAP_S_GetdataSize(&pOSDParams->stBmpDate);
    m_BmpInfo.pBmpData = pOSDParams->stBmpDate.mpData;
    if (!m_BmpInfo.pBmpData) {
        printf("ERR:m_BmpInfo.pBmpData == null!!\n");
    }
    //convertEngString2Bmpdata(str, textLength, &m_fontInfo, &m_BmpInfo);
    convertEngString2Bmpdata(osdString, len, &m_fontInfo, &m_BmpInfo);
    AW_MPI_RGN_SetBitMap(pOSDParams->mOverlayHandle, &pOSDParams->stBmpDate);
    return 0;
}


int RotationRight90(char * src, int srcW, int srcH, int channel)
{
	char * tempSrc = NULL;
	int mSize = srcW * srcH * sizeof(char) * channel;
	int i = 0;
	int j = 0;
	int k = 0;
	int desW = 0;
	int desH = 0;

	desW = srcH;
	desH = srcW;

	tempSrc = (unsigned char *)malloc(sizeof(char) * srcW * srcH * channel);
	memcpy(tempSrc, src, mSize);

	for(i = 0; i < desH; i ++)
	{
		for(j = 0; j < desW; j ++)
		{
			for(k = 0; k < channel; k ++)
			{
				src[(i * desW + j) * channel + k] = tempSrc[((srcH - 1 - j) * srcW + i) * channel + k];
			
			}			
		}
	}

	free(tempSrc);
	return 0;
}



int sl_update_osd_y(Ych_OSD_Params* pYchOSDParams, char* osdString, int lenString)
{
    FontInfo	m_fontInfo;
    memcpy(m_fontInfo.fontPath, fontPath, strlen(fontPath)+1);
    m_fontInfo.fontWidth	= fontWidth;
    m_fontInfo.fontHeight 	= fontHeight;
	memcpy(m_fontInfo.gbfontPath, gbfontPath, strlen(gbfontPath)+1);
    m_fontInfo.gbfontWidth	= gbfontWidth;
    m_fontInfo.gbfontHeight	= gbfontHeight;

	// ASCII
    FILE * ap;
    if((ap=fopen(m_fontInfo.fontPath, "rb")) == NULL) {
        printf("Can't Open ASC %s\n", m_fontInfo.fontPath);
        return;
    }
	// GB2312
	FILE * gp;
    if((gp=fopen(m_fontInfo.gbfontPath, "rb")) == NULL) {
        printf("Can't Open GB %s\n", m_fontInfo.gbfontPath);
        return;
    }

	// if the end position of string > imgWidth, 
	// x start position move left to make sure NOT outof memory
    unsigned int 	osdX, osdY, tmp;	
	#ifdef ROTATE
	osdX = pYchOSDParams->x;
	osdY = pYchOSDParams->y;
	tmp = osdY * pYchOSDParams->imgWidth + osdX;
	#else	
    if ((pYchOSDParams->x + fontWidth*lenString)<=pYchOSDParams->imgWidth) {
        osdX = pYchOSDParams->x;
    } else {
        osdX = pYchOSDParams->x - ((pYchOSDParams->x + fontWidth*lenString) - pYchOSDParams->imgWidth);
    }
	// if the vertical position of string > imgHeight, 
	// y start position move up to make sure NOT outof memory
    if ((pYchOSDParams->y + fontHeight)<=pYchOSDParams->imgHeight) {
        osdY = pYchOSDParams->y;
    } else {
        osdY = pYchOSDParams->y - ((pYchOSDParams->y + fontHeight) - pYchOSDParams->imgHeight);
    }
	#endif

	int	font_width = 0;
	int fontOffset = 0;
    for (int m_count = 0; m_count < lenString; m_count++) {
		int 	fontW, fontH;
		char*	pFont;	
		// GB2312
		if (osdString[m_count] & 0x80) {
			char chinese[4] = {0};
            chinese[0] = osdString[m_count++];
            chinese[1] = osdString[m_count++];
            chinese[2] = osdString[m_count];
			
			//printf("chinese : %s\n", chinese);
			int perCharSize = m_fontInfo.gbfontWidth*m_fontInfo.gbfontHeight/8;
		    unsigned char buffer[perCharSize];
			char word[4] = {0};
			Utf8ToGb2312(chinese, strlen(chinese), word, sizeof(word));
		    unsigned char qh = word[0] - 0xa0;
		    unsigned char wh = word[1] - 0xa0;
			unsigned int offset = (94 * (unsigned int)(qh-1) + (unsigned int)(wh-1)) * perCharSize;
		    fseek(gp, offset, SEEK_SET);
		    fread(buffer, perCharSize, 1, gp);

			fontW = m_fontInfo.gbfontWidth;
			fontH = m_fontInfo.gbfontHeight;
			pFont = buffer;

	        for (int bitH = 0; bitH < fontH; bitH++) {
	            for (int byteW = 0; byteW < fontW/8; byteW++) {
	                unsigned char byteValue = pFont[bitH*fontW/8 + byteW];
	                for (int b = 0; b<8; b++) {
						fontOffset	= (osdY+bitH)*pYchOSDParams->imgWidth + osdX + font_width + byteW*8+b;
	                    if (byteValue & (0x80>>b)) { 
	                        *(char *)(pYchOSDParams->imgSrc + fontOffset) = pYchOSDParams->ftWeight;
							//printf("%s", "*");
	                    }
						else {
							if (pYchOSDParams->bkEnable) {	
								*(char *)(pYchOSDParams->imgSrc + fontOffset) = pYchOSDParams->bkWeight;
							}
							//printf("%s", " ");
						}
	                }
	            }
				//printf("\n");
	        }
			font_width += fontW;
		}
		// ASCII
		else {
			int word = osdString[m_count];
	        char mat2[m_fontInfo.fontWidth / 8 * m_fontInfo.fontHeight];
	        fseek(ap, word*m_fontInfo.fontWidth / 8 * m_fontInfo.fontHeight, SEEK_SET);
	        fread(mat2, m_fontInfo.fontWidth / 8 * m_fontInfo.fontHeight, 1, ap);
						

			#ifdef ROTATE
			
			fontW = m_fontInfo.fontWidth;
			fontH = m_fontInfo.fontHeight;
			for(int bitH=0;bitH<fontH;bitH++) {    //0--31
			  int tmp2 = tmp - bitH;
			  for(int byteW=0;byteW<fontW/8;byteW++) {  //0--1
			    unsigned char byteValue = mat2[bitH*fontW/8+byteW];
				int tmp1 = (font_width + byteW * 8);
			    for(int b=0;b<8;b++) {
			      	//fontOffset = (osdY*pYchOSDParams->imgWidth+osdX-bitH)+(byteW*8+b+font_width)*pYchOSDParams->imgWidth;
			      	fontOffset = tmp2 + (tmp1 + b) * pYchOSDParams->imgWidth;
				  	//printf("fontOffset %d, osdY %d, bitH %d, pYchOSDParams->imgWidth %d, osdX %d, font_width %d byteW %d, b %d\n", \
				  	//			fontOffset, osdY, bitH, pYchOSDParams->imgWidth, osdX, font_width, byteW, b);
				  	if (byteValue & (1<<b)) { //判断点阵中的各个点是否为1
	               		*(char *)(pYchOSDParams->imgSrc + fontOffset) = pYchOSDParams->ftWeight;
	              	}	else {
				  	if (pYchOSDParams->bkEnable) {	
						*(char *)(pYchOSDParams->imgSrc + fontOffset) = pYchOSDParams->bkWeight;
						}
					}
			    }
			  }
			}
			font_width += fontW;
			
			#else

			fontW = m_fontInfo.fontWidth;
			fontH = m_fontInfo.fontHeight;
	        for (int bitH = 0; bitH < fontH; bitH++) {
	            for (int byteW = 0; byteW < fontW / 8; byteW++) {
	                unsigned char byteValue = mat2[bitH * fontW / 8 + byteW];
	                for (int b = 0; b<8; b++) {
						fontOffset	= (osdY+bitH)*pYchOSDParams->imgWidth + osdX + font_width + byteW * 8 + b;
						//printf("fontOffset %d, osdY %d, bitH %d, pYchOSDParams->imgWidth %d, osdX %d, font_width %d byteW %d, b %d\n", \
						//	fontOffset, osdY, bitH, pYchOSDParams->imgWidth, osdX, font_width, byteW, b);
	                    if (byteValue & (1<<b)) { //判断点阵中的各个点是否为1
	                        *(char *)(pYchOSDParams->imgSrc + fontOffset) = pYchOSDParams->ftWeight;
	                    }
						else {
							if (pYchOSDParams->bkEnable) {	
								*(char *)(pYchOSDParams->imgSrc + fontOffset) = pYchOSDParams->bkWeight;
							}
						}
	                }
	            }
	        }
			font_width += fontW;

			#endif
		}
    }

    if (ap) {
        fclose(ap);
    }
	if (gp) {
        fclose(gp);
    }
}



int sl_update_pos(OSD_Params* pOSDParams, int layerId)
{
    RGN_CHN_ATTR_S stRgnChnAttr2;
    memset(&stRgnChnAttr2, 0, sizeof(RGN_CHN_ATTR_S));
    stRgnChnAttr2.bShow                            = TRUE;
    stRgnChnAttr2.enType                           = pOSDParams->stRegionDate.enType;
    stRgnChnAttr2.unChnAttr.stOverlayChn.stPoint.X = pOSDParams->x;    // 32;
    stRgnChnAttr2.unChnAttr.stOverlayChn.stPoint.Y = pOSDParams->y;    // 32;
    stRgnChnAttr2.unChnAttr.stOverlayChn.mLayer    = layerId;
    AW_MPI_RGN_SetDisplayAttr(pOSDParams->mOverlayHandle, &pOSDParams->mMppChn, &stRgnChnAttr2);

    return 0;
}

int create_osd(OSD_Params* pOSDParams, int layerId)
{

    // 初始化区域参数
    strcpy((char*)pOSDParams->szText, "XXXX XX XX XX:XX:XXXXXX XX XX XX:XX:XX");
    pOSDParams->pTextBuffer = (unsigned char*)malloc(FONTSIZE * FONTSIZE / 2 / 8 * strlen((char*)pOSDParams->szText));
    memset(pOSDParams->pTextBuffer, 0, FONTSIZE * FONTSIZE / 2 / 8 * strlen((char*)pOSDParams->szText));

    // 创建区域对象
    int date_region_w = strlen((char*)pOSDParams->szText) * FONTSIZE / 2;
    int date_region_h = FONTSIZE;
    pOSDParams->stRegionDate.enType                        = OVERLAY_RGN;
    pOSDParams->stRegionDate.unAttr.stOverlay.mPixelFmt    = MM_PIXEL_FORMAT_RGB_8888;
    pOSDParams->stRegionDate.unAttr.stOverlay.mSize.Width  = date_region_w;
    pOSDParams->stRegionDate.unAttr.stOverlay.mSize.Height = date_region_h;
    AW_MPI_RGN_Create(pOSDParams->mOverlayHandle, &pOSDParams->stRegionDate);
#if 0
    // 将该区域叠加到viDev和ViCh绑定的通道
    RGN_CHN_ATTR_S stRgnChnAttr2;
    memset(&stRgnChnAttr2, 0, sizeof(RGN_CHN_ATTR_S));
    stRgnChnAttr2.bShow                            = TRUE;
    stRgnChnAttr2.enType                           = pOSDParams->stRegionDate.enType;
    stRgnChnAttr2.unChnAttr.stOverlayChn.stPoint.X = 32;
    stRgnChnAttr2.unChnAttr.stOverlayChn.stPoint.Y = 32;
    stRgnChnAttr2.unChnAttr.stOverlayChn.mLayer    = layerId;
    AW_MPI_RGN_AttachToChn(pOSDParams->mOverlayHandle,  &pOSDParams->mMppChn, &stRgnChnAttr2);

    // 配置区域对应的位图属性
    memset(&pOSDParams->stBmpDate, 0, sizeof(BITMAP_S));
    pOSDParams->stBmpDate.mPixelFormat = pOSDParams->stRegionDate.unAttr.stOverlay.mPixelFmt;
    pOSDParams->stBmpDate.mWidth       = pOSDParams->stRegionDate.unAttr.stOverlay.mSize.Width;
    pOSDParams->stBmpDate.mHeight      = pOSDParams->stRegionDate.unAttr.stOverlay.mSize.Height;
    int nSizeDate                      = BITMAP_S_GetdataSize(&pOSDParams->stBmpDate);
    pOSDParams->stBmpDate.mpData       = malloc(nSizeDate);
#endif
    return 0;
}

int update_osd(OSD_Params* pOSDParams, void* pTime, int len, int layerId)
{
    memset(pOSDParams->szText, 0, sizeof(pOSDParams->szText));
    if(len > 0) {
        strcpy((char*)pOSDParams->szText, pTime);
        sprintf(pOSDParams->szText, "Name: %s", pTime);
    } else {
        sprintf(pOSDParams->szText, "Name: ");
    }

    int count = strlen((char*)pOSDParams->szText);
    if (count % 16 != 0) {
        int offset = count % 16;
        int i = 0;
        for (i = count; i < count + 16 - offset; i++) {
            pOSDParams->szText[i] = ' ';
        }
        pOSDParams->szText[i] = '\0';
    }

    //修改 区域对象
    int date_region_w = strlen((char*)pOSDParams->szText) * FONTSIZE / 2;
    int date_region_h = FONTSIZE;
    pOSDParams->stRegionDate.enType                        = OVERLAY_RGN;
    pOSDParams->stRegionDate.unAttr.stOverlay.mPixelFmt    = MM_PIXEL_FORMAT_RGB_8888;
    pOSDParams->stRegionDate.unAttr.stOverlay.mSize.Width  = date_region_w;
    pOSDParams->stRegionDate.unAttr.stOverlay.mSize.Height = date_region_h;
    if (1 == isCreate) {
        int rgnRet = AW_MPI_RGN_DetachFromChn(pOSDParams->mOverlayHandle,  &pOSDParams->mMppChn);
        if (SUCCESS != rgnRet) {
            printf("AW_MPI_RGN_Destroy failed, layerId = %d, rgnRet = x0%x\n", layerId, rgnRet);
        }
    } else {
        isCreate = 1;
    }
    // RGN_CHN_ATTR_S rngChnAttr = {0};
    // rngChnAttr.enType = OVERLAY_RGN;
    // rngChnAttr.unChnAttr.stOverlayChn.mLayer =
    // AW_MPI_RGN_SetDisplayAttr(pOSDParams->mOverlayHandle, )
    AW_MPI_RGN_SetAttr(pOSDParams->mOverlayHandle, &pOSDParams->stRegionDate);

    // 将该区域叠加到viDev和ViCh绑定的通道
    RGN_CHN_ATTR_S stRgnChnAttr2;
    memset(&stRgnChnAttr2, 0, sizeof(RGN_CHN_ATTR_S));
    stRgnChnAttr2.bShow                            = TRUE;
    stRgnChnAttr2.enType                           = pOSDParams->stRegionDate.enType;
    stRgnChnAttr2.unChnAttr.stOverlayChn.stPoint.X = 32;
    stRgnChnAttr2.unChnAttr.stOverlayChn.stPoint.Y = 32;
    stRgnChnAttr2.unChnAttr.stOverlayChn.mLayer    = layerId;
    stRgnChnAttr2.unChnAttr.stOverlayChn.mBgAlpha  = 0;
    AW_MPI_RGN_AttachToChn(pOSDParams->mOverlayHandle,  &pOSDParams->mMppChn, &stRgnChnAttr2);


    if (pOSDParams->stBmpDate.mpData) {
        free(pOSDParams->stBmpDate.mpData);
        pOSDParams->stBmpDate.mpData = NULL;
    }
    memset(&pOSDParams->stBmpDate, 0, sizeof(BITMAP_S));
    pOSDParams->stBmpDate.mPixelFormat = pOSDParams->stRegionDate.unAttr.stOverlay.mPixelFmt;
    pOSDParams->stBmpDate.mWidth       = pOSDParams->stRegionDate.unAttr.stOverlay.mSize.Width;
    pOSDParams->stBmpDate.mHeight      = pOSDParams->stRegionDate.unAttr.stOverlay.mSize.Height;
    int nSizeDate                      = BITMAP_S_GetdataSize(&pOSDParams->stBmpDate);
    pOSDParams->stBmpDate.mpData       = malloc(nSizeDate);

    GenTextBuffer((char *)pOSDParams->szText, (char *)pOSDParams->pTextBuffer, pOSDParams->stBmpDate.mpData);
    AW_MPI_RGN_SetBitMap(pOSDParams->mOverlayHandle, &pOSDParams->stBmpDate);

    return 0;
}

int destroy_osd(OSD_Params*     pOSDParams)
{
    // 销毁OSD
    AW_MPI_RGN_DetachFromChn(pOSDParams->mOverlayHandle, &pOSDParams->mMppChn);
    AW_MPI_RGN_Destroy(pOSDParams->mOverlayHandle);
    free(pOSDParams->pTextBuffer);
    free(pOSDParams->stBmpDate.mpData);

    return 0;
}

#if 0
printf("%s,---%d\n", __func__, __LINE__);
for (int h = 0; h < pFontInfo->fontHeight; h++)
{
    for (int m_count = 0; m_count < pBmpInfo->bmpWidth; m_count++) {
        for (int w = 0; w < pFontInfo->fontWidth/8; w++) {
            if (m_count < textLength) {
                unsigned char byte = mat[m_count][h*pFontInfo->fontWidth/8+w];
                for (int b = 0; b<8; b++) {
                    int index = h * ((m_count+1)*pFontInfo->fontWidth) + m_count*pFontInfo->fontWidth + w*8 + b;
                    if (byte & (1<<b)) { //判断点阵中的各个点是否为1
                        //printf("@");
                        RGB8888Info *pRGB = (RGB8888Info *)(pBmpInfo->pBmpData + index);
                        pRGB->alpha = 0xff;
                        pRGB->red = 0xff;
                        pRGB->green = 0;
                        pRGB->blue = 0;
                        printf("~~~~offset = %d, h:%d m_count:%d bmpWidth:%d fontWidth:%d b:%d-------------------------1\n", index, h, m_count, pBmpInfo->bmpWidth, pFontInfo->fontWidth, b);

                    } else {
                        printf("~~~~offset = %d, h:%d m_count:%d bmpWidth:%d fontWidth:%d b:%d-------------------------1\n", index, h, m_count, pBmpInfo->bmpWidth, pFontInfo->fontWidth, b);
                        //printf("-");
                    }
                }
            } else {
                for (int b = 0; b<8; b++) {
                    //printf("-");
                }
            }
        }
    }
    printf("%s,---%d\n", __func__, __LINE__);
    //printf("\n");
}
#endif
