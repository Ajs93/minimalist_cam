#include <stdio.h>
#include "sample_nna.h"
#include <mpi_region.h>
#include <sys/ioctl.h>
#include <sys/prctl.h>
#include <signal.h>
#include <unistd.h>
#include <mqueue.h>
#include <limits.h>
#include "plat_log.h"
#include "mpp_osd.h"
#include "decoder.h"
#include "confparser.h"
#include <dirent.h>


pthread_mutex_t g_stResult_lock;// = PTHREAD_MUTEX_INITIALIZER;
static float g_bitrate = 1.0;
CODEC_FORMAT g_codecFormat = CODEC_H264;
static int g_codecToggle = 1;
RESOLUTION_TYPE g_resolutionType = RESOLUTION_FHD;
static int g_resolutionToggle = 1;
static int g_frameRate = 25;
static mqd_t g_mqId = 0;
static int g_face_pic_flag = 0;
pthread_mutex_t g_mq_lock;// = PTHREAD_MUTEX_INITIALIZER;
static int g_stop = 0;
static int fd_pwm = 0, fd_period = 0, fd_duty = 0, fd_enable = 0;

static int VI_CAPTURE_WIDTH        = (1920);
static int VI_CAPTURE_HEIGHT        = (1080);
static int NNA_CALC_WIDTH          = (1920);
static int NNA_CALC_HEIGHT          = (1080);


OSD_XYBOX_T*	g_nnaDetectResult = NULL;
NNA_MODE 		g_nnaDetectMode = NNA_MODE_FR;	//初始化为人脸识别模式
SLNNA_Info 		g_nnaInfo;
volatile int 	net_select 	= 0;

version_t version;

load_insi_Config insi_Config;
#define MAX_FRAME_BUF_SiZE  1920*1080*3


#include "xop/RtspServer.h"
#include "net/NetInterface.h"
#include <thread>
#include <memory>
#include <iostream>
#include <string>
static unsigned char g_stream_buf[MAX_FRAME_BUF_SiZE] = {0};

#include <unistd.h>
#include <fcntl.h>
#include "tiny_jpeg.h"
#include "ETList.h"
#include "XMMute.h"
#include "mpp_osd.h"

typedef struct Person_info_t {
    char name[64];
} Person_Info;

Person_Info personinfo;

unsigned char *m_rgb = (unsigned char *)malloc(MAX_FRAME_BUF_SiZE);
unsigned char *m_yuv = (unsigned char *)malloc(MAX_FRAME_BUF_SiZE);
unsigned char *m_cutyuv = (unsigned char *)malloc(MAX_FRAME_BUF_SiZE);


#include <linux/g2d_driver.h>
static int g_iG2dFd = -1;
static void open_g2d_device()
{
    // Initial g2d device
    g_iG2dFd = open("/dev/g2d", O_RDWR);
    assert(g_iG2dFd >= 0);
}
static void close_g2d_device()
{
    // close g2d device
    close(g_iG2dFd);
}

static ERRORTYPE loadSampleInsiConfig(load_insi_Config *pConfig, const char *conf_path)
{
    int ret;
    CONFPARSER_S stConfParser;

    ret = createConfParser(conf_path, &stConfParser);
    if(ret < 0)
    {
        aloge("load conf fail");
        return FAILURE;
    }
    memset(pConfig, 0, sizeof(load_insi_Config));	

    pConfig->insi_min_score = GetConfParaDouble(&stConfParser, SAMPLE_INSI_MIN_SCORE, 0);
    pConfig->mode_select = GetConfParaInt(&stConfParser, SAMPLE_INSI_MODE_SELECT, 0);
    pConfig->en_265 = GetConfParaInt(&stConfParser, SAMPLE_INSI_ENCODE_265, 0);
    pConfig->resolution = GetConfParaInt(&stConfParser, SAMPLE_INSI_RESOLUTION, 0);
    pConfig->bitrate = GetConfParaInt(&stConfParser, SAMPLE_INSI_BITRATE, 0);
    pConfig->frame_rate = GetConfParaInt(&stConfParser, SAMPLE_INSI_FRAME_RATE, 0);  
	pConfig->cap_pic_recg = GetConfParaInt(&stConfParser, SAMPLE_INSI_PIC_RECG, 0);
	//pConfig->img_rotate = GetConfParaInt(&stConfParser, SAMPLE_INSI_IMG_ROTATE, 0);
	pConfig->support_rtsp = GetConfParaInt(&stConfParser, SAMPLE_INSI_SUPPORT_RTSP, 0);
	pConfig->save_h26x_file = GetConfParaInt(&stConfParser, SAMPLE_INSI_H26x_FILE, 0);
	//pConfig->use_rng_attr = GetConfParaInt(&stConfParser, SAMPLE_INSI_USE_RNG_ATTR, 0);
	pConfig->face_upload = GetConfParaInt(&stConfParser, SAMPLE_INSI_FACE_UPLOAD, 0);
	//pConfig->save_mp4_file = GetConfParaInt(&stConfParser, SAMPLE_INSI_MP4_FILE, 0);
	//pConfig->enable_ise = GetConfParaInt(&stConfParser, SAMPLE_INSI_ENABLE_ISE, 0);
	//pConfig->support_keyboard = GetConfParaInt(&stConfParser, SAMPLE_INSI_SUPPORT_KEYBOARD, 0);
	//pConfig->pwm_test = GetConfParaInt(&stConfParser, SAMPLE_INSI_PWM_TEST, 0);
	pConfig->time_print = GetConfParaInt(&stConfParser, SAMPLE_INSI_TIME_PRINT, 0);
	pConfig->rect_keep = GetConfParaInt(&stConfParser, SAMPLE_INSI_RECT_KEEP, 0);
	pConfig->fd_no_fr = GetConfParaInt(&stConfParser, SAMPLE_INSI_FD_NO_FR, 0);
	pConfig->is_blackwhite = GetConfParaInt(&stConfParser, SAMPLE_INSI_IS_BLACKWHITE, 0);

	pConfig->conf_thres = GetConfParaDouble(&stConfParser, SAMPLE_INSI_IS_CONF_THRES, 0);
	pConfig->nms_thres = GetConfParaDouble(&stConfParser, SAMPLE_INSI_IS_NMS_THRES, 0);
	pConfig->y_add_num = GetConfParaInt(&stConfParser, SAMPLE_INSI_Y_ADD_NUM, 0);
	pConfig->detect_type = GetConfParaInt(&stConfParser, SAMPLE_INSI_DETECT_TYPE, 0);
	pConfig->save_argb = GetConfParaInt(&stConfParser, SAMPLE_INSI_SAVE_ARGB_BIN, 0);
	
    destroyConfParser(&stConfParser);

	printf("insi_min_score = %.3f, mode_select = %d, en_265 = %d, resolution = %d, bitrate = %d, frame_rate = %d\n", \
			pConfig->insi_min_score, pConfig->mode_select, pConfig->en_265, pConfig->resolution, pConfig->bitrate, pConfig->frame_rate);

	printf("cap_pic_recg = %d, support_rtsp = %d, face_upload = %d, save_h26x_file = %d, time_print = %d, rect_keep = %d\n", \
			pConfig->cap_pic_recg, pConfig->support_rtsp, pConfig->face_upload, pConfig->save_h26x_file, pConfig->time_print, pConfig->rect_keep);

	printf("fd_no_fr = %d, is_blackwhite = %d, detect_type = %d\n", pConfig->fd_no_fr, pConfig->is_blackwhite, pConfig->detect_type);
	
	printf("conf_thres = %.3f, nms_thres = %.3f, y_add_num = %d\n", pConfig->conf_thres, pConfig->nms_thres, pConfig->y_add_num);

	printf("save_argb = %d\n", pConfig->save_argb);

    return SUCCESS;
}


xop::RtspServer *g_rtspServer = NULL;
xop::MediaSessionId g_sessionId = 0;
static std::shared_ptr<xop::EventLoop> g_sEventLoop = NULL;

void SendFrame2Rtsp(unsigned int frameSize, void *frameBuf, xop::FrameType frameType)
{
    xop::AVFrame videoFrame = {0};
    videoFrame.type = frameType;
    videoFrame.size = frameSize;
    videoFrame.buffer.reset(new uint8_t[videoFrame.size]);
    memcpy(videoFrame.buffer.get(), frameBuf, videoFrame.size);
    if (g_codecFormat == CODEC_H264)
        videoFrame.timestamp = xop::H264Source::getTimeStamp();
    else if (g_codecFormat == CODEC_H265)
        videoFrame.timestamp = xop::H265Source::getTimeStamp();
    g_rtspServer->pushFrame(g_sessionId, xop::channel_0, videoFrame);
}

void SendAudFrame2Rtsp(unsigned int frameSize, void *frameBuf, xop::FrameType frameType)
{
    xop::AVFrame audioFrame = {0};
    audioFrame.type = frameType;
    audioFrame.size = frameSize;
    audioFrame.buffer.reset(new uint8_t[audioFrame.size]);
    memcpy(audioFrame.buffer.get(), frameBuf, audioFrame.size);
    audioFrame.timestamp = xop::AACSource::getTimeStamp(8000);
    g_rtspServer->pushFrame(g_sessionId, xop::channel_1, audioFrame);
}

void StartRtspService()
{
    // std::string ip = "127.0.0.1";//xop::NetInterface::getLocalIPAddress(); //获取网卡ip地址
    std::string ip = xop::NetInterface::getLocalIPAddress(); //获取网卡ip地址
    std::string rtspUrl;

    std::shared_ptr<xop::EventLoop> eventLoop(new xop::EventLoop());
    g_sEventLoop = eventLoop;
    // xop::RtspServer server(eventLoop.get(), ip, 8890);
    g_rtspServer = new xop::RtspServer(eventLoop.get(), ip, 8890);

    xop::MediaSession *session = xop::MediaSession::createNew("senslab");
    rtspUrl = "rtsp://" + ip + ":8890/" + session->getRtspUrlSuffix();
    //session->startMulticast();

    if (g_codecFormat == CODEC_H264)
        session->addMediaSource(xop::channel_0, xop::H264Source::createNew());
    else if (g_codecFormat == CODEC_H265)
        session->addMediaSource(xop::channel_0, xop::H265Source::createNew());
    session->addMediaSource(xop::channel_1, xop::AACSource::createNew(8000,1));
    g_sessionId = g_rtspServer->addMeidaSession(session);

    // std::thread t1(snedFrameThread, g_rtspServer, sessionId, &h264File);
    // t1.detach();
    std::cout << "Play URL: " <<rtspUrl << std::endl;

    sleep(2);

    eventLoop->loop();
}

void StopRtspService()
{
    g_rtspServer->removeMeidaSession(g_sessionId);
    delete g_rtspServer;
    g_sEventLoop = NULL;
}

static void *RTSP_Proc(void* arg)
{
    StartRtspService();
    StopRtspService();
    printf("RTSP_Proc_exit!!\n");

    return NULL;
}

static void *CMD_Proc(void* arg)
{
    char cmdBuf[20] = {0};
    printf("CMD_Proc enter!!\n");
    struct mq_attr mqAttr;
    mq_getattr(g_mqId, &mqAttr);
    char *msgBuf = (char *)malloc(mqAttr.mq_msgsize);
    while (1) {
        if (g_stop)
            break;
        memset(cmdBuf, 0, 20);
        gets(cmdBuf);
        //printf("Received cmd:%s\n\n\n\n\n", cmdBuf);
        if(memcmp(cmdBuf, "QG-", 3) == 0) {
            memset(msgBuf, 0, mqAttr.mq_msgsize);
            memcpy(msgBuf, &cmdBuf[3], strlen(&cmdBuf[3]));
            pthread_mutex_lock(&g_mq_lock);
            if (mq_send(g_mqId, msgBuf, strlen(msgBuf), 0) < 0) {
                printf("send message failed.\n");
                printf("error info:%s", strerror(errno));
            }
            pthread_mutex_unlock(&g_mq_lock);
            printf("send msg success:%s.\n", &cmdBuf[3]);
        } else {
            printf("Unsupported cmd: %s, directly run in shell\n", cmdBuf);
            system(cmdBuf);
        }
        usleep(100*1000);
    }

    printf("CMD_Proc_exit!!\n");
    free(msgBuf);
    return NULL;
}

void resolution2Str(RESOLUTION_TYPE type, char * string)
{
    switch(type) {
    case RESOLUTION_SD:
        strcpy(string, "SD");
        break;
    case RESOLUTION_HD:
        strcpy(string, "HD");
        break;
    case RESOLUTION_FHD:
        strcpy(string, "FHD");
        break;
    default:
        strcpy(string, "SD");
        break;
    }
}

MSG_TYPE msg2Type(char * string)
{
    if (!strcmp(string, "H264"))
        return MSG_H264;
    else if (!strcmp(string, "H265"))
        return MSG_H265;
    else if (!strcmp(string, "SD"))
        return MSG_SD;
    else if (!strcmp(string, "HD"))
        return MSG_HD;
    else if (!strcmp(string, "FHD"))
        return MSG_FHD;
    else if (!strcmp(string, "LIGHT"))
        return MSG_LIGHT;
    else if (!strcmp(string, "HFLIP"))
        return MSG_HFLIP;
    else if (!strcmp(string, "VFLIP"))
        return MSG_VFLIP;
	else if(!strncmp(string, "CAP", 3))
		return MSG_CAP;
    else if (!strcmp(string, "STOP"))
        return MSG_STOP;
	else
		return MSG_UNKNOWN;

}

static void *MSG_Proc(void* arg)
{
    printf("MSG_Proc enter!!\n");
    struct mq_attr mqAttr;
    mq_getattr(g_mqId, &mqAttr);
    char *msgBuf = (char *)malloc(mqAttr.mq_msgsize);
    char string[20] = {0};
    MSG_TYPE type = MSG_H264;
    int light = 0, hFlip = 0, vFlip = 0;

    while (1) {
        usleep(100*1000);
        if (g_stop)
            break;
        if (mq_getattr(g_mqId, &mqAttr) < 0) {
            printf("get the message queue attribute error\n");
            continue;
        }
        if (mqAttr.mq_curmsgs == 0) {
            continue;
        } else {
            memset(msgBuf, 0, mqAttr.mq_msgsize);
            pthread_mutex_lock(&g_mq_lock);
            mq_receive(g_mqId, msgBuf, mqAttr.mq_msgsize, NULL);
            pthread_mutex_unlock(&g_mq_lock);
            printf("Received msg:%s\n\n\n\n\n", msgBuf);
            type = msg2Type(msgBuf);
            switch(type) {
            case MSG_H264: {
                if (g_codecFormat != CODEC_H264) {
                    g_codecFormat = CODEC_H264;
                    g_codecToggle = 1;
                    printf("Codec toggle !!!!!!!!!!!!!!!!!!!!\n");
                    printf("Codec H265 -> H264\n");
                } else
                    printf("Codec H264 -> H264\n");
            }
            break;
            case MSG_H265: {
                if (g_codecFormat != CODEC_H265) {
                    g_codecFormat = CODEC_H265;
                    g_codecToggle = 1;
                    printf("Codec toggle !!!!!!!!!!!!!!!!!!!!\n");
                    printf("Codec H264 -> H265\n");
                } else
                    printf("Codec H265 -> H265\n");
            }
            break;
            case MSG_SD: {
                if (g_resolutionType != RESOLUTION_SD) {
                    resolution2Str(g_resolutionType, string);
                    g_resolutionType = RESOLUTION_SD;
                    g_resolutionToggle = 1;
                    VI_CAPTURE_WIDTH        = (800);
                    VI_CAPTURE_HEIGHT        = (480);
                    NNA_CALC_WIDTH          = (800);
                    NNA_CALC_HEIGHT          = (480);
                    printf("Resolution toggle !!!!!!!!!!!!!!!!!!!!\n");
                    printf("Resolution %s -> SD\n", string);
                } else
                    printf("Resolution SD -> SD\n");
            }
            break;
            case MSG_HD: {
                if (g_resolutionType != RESOLUTION_HD) {
                    resolution2Str(g_resolutionType, string);
                    g_resolutionType = RESOLUTION_HD;
                    g_resolutionToggle = 1;
                    VI_CAPTURE_WIDTH        = (1280);
                    VI_CAPTURE_HEIGHT        = (720);
                    NNA_CALC_WIDTH          = (1280);
                    NNA_CALC_HEIGHT          = (720);
                    printf("Resolution toggle !!!!!!!!!!!!!!!!!!!!\n");
                    printf("Resolution %s -> HD\n", string);
                } else
                    printf("Resolution HD -> HD\n");
            }
            break;
            case MSG_FHD: {
                if (g_resolutionType != RESOLUTION_FHD) {
                    resolution2Str(g_resolutionType, string);
                    g_resolutionType = RESOLUTION_FHD;
                    g_resolutionToggle = 1;
                    VI_CAPTURE_WIDTH        = (1920);
                    VI_CAPTURE_HEIGHT        = (1080);
                    NNA_CALC_WIDTH          = (1920);
                    NNA_CALC_HEIGHT          = (1080);
                    printf("Resolution toggle !!!!!!!!!!!!!!!!!!!!\n");
                    printf("Resolution %s -> FHD\n", string);
                } else
                    printf("Resolution FHD -> FHD\n");
            }
            break;
            
            case MSG_HFLIP: {
                printf("H flip !!!!!!!!!!!!!!!!!!!!\n");
                AW_MPI_VI_GetVippMirror(VIPP_0, &hFlip);
                hFlip ^= 0x1;
                AW_MPI_VI_SetVippMirror(VIPP_0, hFlip);
                AW_MPI_VI_SetVippMirror(VIPP_1, hFlip);
                AW_MPI_VI_SetVippMirror(VIPP_2, hFlip);
            }
            break;
            case MSG_VFLIP: {
                printf("V flip !!!!!!!!!!!!!!!!!!!!\n");
                AW_MPI_VI_GetVippFlip(VIPP_0, &vFlip);
                vFlip ^= 0x1;
                AW_MPI_VI_SetVippFlip(VIPP_0, vFlip);
                AW_MPI_VI_SetVippFlip(VIPP_1, vFlip);
                AW_MPI_VI_SetVippFlip(VIPP_2, vFlip);
            }
            break;
			case MSG_CAP: {
				if(insi_Config.face_upload == 1) {
					g_face_pic_flag = 1;
					if(strlen(msgBuf) > 4) {
						memcpy(personinfo.name, &msgBuf[4], strlen(msgBuf) - 4);
					}
					printf("capture msgBuf len %d, %d\n", strlen(msgBuf), strlen(personinfo.name));
				}
			}
				break;
            case MSG_STOP: {
                printf("Stop !!!!!!!!!!!!!!!!!!!!\n");
                g_stop = 1;				
            }
            break;
            default: {
                printf("Unsupported cmd\n");
                break;
            }
            }
        }
    }
    free(msgBuf);
    printf("MSG_Proc_exit!!\n");

    return NULL;
}

int mpp_init()
{
    // 初始化MPP
    int ret = 0;
    MPP_SYS_CONF_S mSysConf;
    memset(&mSysConf, 0, sizeof(MPP_SYS_CONF_S));
    mSysConf.nAlignWidth = 32;
    AW_MPI_SYS_SetConf(&mSysConf);
    AW_MPI_SYS_Init();
    if (ret < 0) {
        printf("mpp init failed!\n");
        return -1;
    }
    printf("mpp init success!\n");
    return 0;
}

int mpp_destroy()
{
    AW_MPI_SYS_Exit();
    return 0;
}

//分配一段内存，用于存储裁剪后的Y分量
unsigned char *tmpY = (unsigned char *)malloc(MAX_FRAME_BUF_SiZE);
//分配一段内存，用于存储裁剪后的UV分量
unsigned char *tmpUV = (unsigned char *)malloc(MAX_FRAME_BUF_SiZE);

void cutYuv(unsigned char *tarYuv, unsigned char *srcYuv, int startW,
            int startH, int cutW, int cutH, int srcW, int srcH)
{
    int i;
    int j = 0;
    int k = 0;


    for(i=startH; i<cutH+startH; i++) {
        // 逐行拷贝Y分量，共拷贝cutW*cutH
        memcpy(tmpY+j*cutW, srcYuv+startW+i*srcW, cutW);
        j++;
    }

    for(i=startH/2; i<(cutH+startH)/2; i++) {
        //逐行拷贝UV分量，共拷贝cutW*cutH/2
        memcpy(tmpUV+k*cutW, srcYuv+startW+srcW*srcH+i*srcW, cutW);
        k++;
    }

    //将拷贝好的Y，UV分量拷贝到目标内存中
    memcpy(tarYuv, tmpY, cutW*cutH);
    memcpy(tarYuv+cutW*cutH, tmpUV, cutW*cutH/2);
}

void NV21_T_RGB(unsigned char *yuyv , unsigned char *rgb, unsigned int width , unsigned int height)
{
    const int nv_start = width * height ;
    uint32_t  i, j, index = 0, rgb_index = 0;
    uint8_t y, u, v;
    int r, g, b, nv_index = 0;

    for(i = 0; i < height; i++) {
        for(j = 0; j < width; j ++) {
            nv_index = i / 2  * width + j - j % 2;

            y = yuyv[rgb_index];
            u = yuyv[nv_start + nv_index ];
            v = yuyv[nv_start + nv_index + 1];

            r = y + (140 * (v-128))/100;  //r
            g = y - (34 * (u-128))/100 - (71 * (v-128))/100; //g
            b = y + (177 * (u-128))/100; //b

            if(r > 255)   r = 255;
            if(g > 255)   g = 255;
            if(b > 255)   b = 255;
            if(r < 0)     r = 0;
            if(g < 0)     g = 0;
            if(b < 0)     b = 0;

            index = rgb_index % width + (height - i - 1) * width;
            //rgb[index * 3+0] = b;
            //rgb[index * 3+1] = g;
            //rgb[index * 3+2] = r;

            //颠倒图像
            //rgb[height * width * 3 - i * width * 3 - 3 * j - 1] = b;
            //rgb[height * width * 3 - i * width * 3 - 3 * j - 2] = g;
            //rgb[height * width * 3 - i * width * 3 - 3 * j - 3] = r;

            //正面图像
            rgb[i * width * 3 + 3 * j + 0] = b;
            rgb[i * width * 3 + 3 * j + 1] = g;
            rgb[i * width * 3 + 3 * j + 2] = r;

            rgb_index++;
        }
    }
    return ;
}


void splitpath(const char *path, char *drv, char *dir, char *name, char *ext)
{
    const char *end;
    const char *p;
    const char *s;
    if (path[0] && path[1] == ':') {
        if (drv) {
            *drv++ = *path++;
            *drv++ = *path++;
            *drv = '\0';
        }
    } else if (drv)
        *drv = '\0';
    for (end = path; *end && *end != ':';)
        end++;
    for (p = end; p > path && *--p != '\\' && *p != '/';)
        if (*p == '.') {
            end = p;
            break;
        }
    if (ext)
        for (s = end; (*ext = *s++);)
            ext++;
    for (p = end; p > path;)
        if (*--p == '\\' || *p == '/') {
            p++;
            break;
        }
    if (name) {
        for (s = p; s < end;)
            *name++ = *s++;
        *name = '\0';
    }
    if (dir) {
        for (s = path; s < p;)
            *dir++ = *s++;
        *dir = '\0';
    }
}

void saveImage(const char *filename, const char *suffix, int Width, int Height,
               int Channels, unsigned char *Output, int frameIndex, int subIndex, int score)
{
#ifndef _MAX_DRIVE
#define _MAX_DRIVE 3
#endif
#ifndef _MAX_FNAME
#define _MAX_FNAME 256
#endif
#ifndef _MAX_EXT
#define _MAX_EXT 256
#endif
#ifndef _MAX_DIR
#define _MAX_DIR 256
#endif
    char saveFile[_MAX_FNAME];

    char drive[_MAX_DRIVE];
    char dir[_MAX_DIR];
    char fname[_MAX_FNAME];
    char ext[_MAX_EXT];
	
    splitpath(filename, drive, dir, fname, ext);
	if(strlen(personinfo.name) > 0) {
    	sprintf(saveFile, "./pic/%s%s", personinfo.name, ext);
	}else {
		sprintf(saveFile, "./pic/%s_%d%s", fname, frameIndex, ext);
	}
    printf("save file :%s, %d\n", saveFile, strlen(personinfo.name));
	memset(personinfo.name, 0, 64);
    if (!tje_encode_to_file(saveFile, Width, Height, Channels, true, Output)) {
        fprintf(stderr, "save JPEG fail.\n");
        return;
    }
}


#ifdef ROTATE
static void rotate_frame(VIDEO_FRAME_INFO_S* pSrcFrameInfo, VIDEO_FRAME_INFO_S* pDstFrameInfo)
{
    unsigned int WidthDst = pDstFrameInfo->VFrame.mWidth;
    unsigned int HeightDst = pDstFrameInfo->VFrame.mHeight;
    unsigned int WidthSrc = pSrcFrameInfo->VFrame.mWidth;
    unsigned int HeightSrc = pSrcFrameInfo->VFrame.mHeight;

    g2d_blt_h info;
    memset(&info, 0, sizeof(g2d_blt_h));
    info.flag_h = G2D_ROT_90;

    info.dst_image_h.use_phy_addr = 1;
#if DEMO_PIC_DATA_FORMAT == DEMO_PIC_DATA_FORMAT_NV21
    info.dst_image_h.format = G2D_FORMAT_YUV420UVC_U1V1U0V0;
#else
    info.dst_image_h.format = G2D_FORMAT_YUV420_PLANAR;
#endif
    info.dst_image_h.width = WidthDst;
    info.dst_image_h.height = HeightDst;
    info.dst_image_h.clip_rect.w = WidthDst;
    info.dst_image_h.clip_rect.h = HeightDst;

    memcpy(&info.src_image_h, &info.dst_image_h, sizeof(g2d_image_enh));
    info.src_image_h.width = WidthSrc;
    info.src_image_h.height = HeightSrc;
    info.src_image_h.clip_rect.w = WidthSrc;
    info.src_image_h.clip_rect.h = HeightSrc;


    info.dst_image_h.laddr[0] = (unsigned int)pDstFrameInfo->VFrame.mPhyAddr[0];
    info.dst_image_h.laddr[1] = (unsigned int)pDstFrameInfo->VFrame.mPhyAddr[0] + (unsigned int)(WidthDst * HeightDst);
    info.dst_image_h.laddr[2] = (unsigned int)pDstFrameInfo->VFrame.mPhyAddr[0] + (unsigned int)(WidthDst * HeightDst * 5 / 4);

    info.src_image_h.laddr[0] = (unsigned int)pSrcFrameInfo->VFrame.mPhyAddr[0];
    info.src_image_h.laddr[1] = (unsigned int)pSrcFrameInfo->VFrame.mPhyAddr[1];
    info.src_image_h.laddr[2] = (unsigned int)pSrcFrameInfo->VFrame.mPhyAddr[2];

    int iRet = ioctl(g_iG2dFd, G2D_CMD_BITBLT_H, (unsigned long)&info);
    if (iRet != 0) {
        printf("g2d ioctl G2D_CMD_BITBLT err = %d\n", iRet);
        printf("g2d ioctl G2D_CMD_BITBLT %s\n", strerror(errno));
    }
	
    pDstFrameInfo->VFrame.mOffsetBottom = HeightDst;
    pDstFrameInfo->VFrame.mOffsetRight  = WidthDst;
    pDstFrameInfo->VFrame.mPixelFormat = DEMO_MM_PIXEL_FORMAT;
    //must !!!
#if DEMO_PIC_DATA_FORMAT == DEMO_PIC_DATA_FORMAT_NV21
    pDstFrameInfo->VFrame.mpVirAddr[1] = (void *)((unsigned int)pDstFrameInfo->VFrame.mpVirAddr[0] + (unsigned int)(WidthDst * HeightDst));
    pDstFrameInfo->VFrame.mpVirAddr[2] = (void *)0;
    pDstFrameInfo->VFrame.mPhyAddr[1] = (unsigned int)pDstFrameInfo->VFrame.mPhyAddr[0] + (unsigned int)(WidthDst * HeightDst);
    pDstFrameInfo->VFrame.mPhyAddr[2] = (unsigned int)0;
    pDstFrameInfo->VFrame.mStride[0] = (unsigned int)WidthDst * (unsigned int)HeightDst;
    pDstFrameInfo->VFrame.mStride[1] = (unsigned int)WidthDst * (unsigned int)HeightDst/2;
    pDstFrameInfo->VFrame.mStride[2] = 0;
#else
    pDstFrameInfo->VFrame.mpVirAddr[1] = (void *)((unsigned int)pDstFrameInfo->VFrame.mpVirAddr[0] + (unsigned int)(WidthDst * HeightDst));
    pDstFrameInfo->VFrame.mpVirAddr[2] = (void *)((unsigned int)pDstFrameInfo->VFrame.mpVirAddr[0] + (unsigned int)(WidthDst * HeightDst * 5 / 4));
    pDstFrameInfo->VFrame.mPhyAddr[1] = (unsigned int)pDstFrameInfo->VFrame.mPhyAddr[0] + (unsigned int)(WidthDst * HeightDst);
    pDstFrameInfo->VFrame.mPhyAddr[2] = (unsigned int)pDstFrameInfo->VFrame.mPhyAddr[0] + (unsigned int)(WidthDst * HeightDst * 5 / 4);
    pDstFrameInfo->VFrame.mStride[0] = (unsigned int)WidthDst * (unsigned int)HeightDst;
    pDstFrameInfo->VFrame.mStride[1] = (unsigned int)WidthDst * (unsigned int)HeightDst/4;
    pDstFrameInfo->VFrame.mStride[2] = (unsigned int)WidthDst * (unsigned int)HeightDst/4;
#endif
}
#endif


static int nna_save_buf1(char *addr, int size, char *name)
{
    FILE *fd;

    fd = fopen(name, "wb");
    if(fd == NULL) {
        printf("nna_save buf1 open file err\n");
        return -1;
    }
    if(fwrite(addr, size, 1, fd) != 1) {
        printf("nna_save buf1 write failure\n");
        fclose(fd);
        return -1;
    }
    fclose(fd);
    return 0;
}


static void get_all_files1(char *pathname, OSD_XYBOX_T *tmp_nnaDetectResult)
{
    struct dirent **namelist;
    int n;
	
    n = scandir(pathname, &namelist, 0, alphasort);
    if (n < 0)
        printf("get_all_files1 scandir\n");
    else
    {
        while(n--)
        {
            printf("%s\n", namelist[n]->d_name);
            free(namelist[n]);
        }
        free(namelist);
    }
}


static int count = 1, recg = 0;
static void get_all_files(char *pathname, OSD_XYBOX_T *tmp_nnaDetectResult)
{
	DIR *dir;
	struct dirent *ptr;
	char name_d[NAME_MAX_VALUE] = {0, };
	int Width, Height, Size, Channels;
	char *pic_src;
	double starttime;
	
	starttime = 1000000.0 * now();
	printf("get all files start time %.5f\n", starttime);

	#if 0
	sprintf(name_d, "./log_%.5f", starttime);
	FILE* fd = fopen(name_d, "wb");
	if(fd == NULL) {
		printf("fopen log file failure\n");
		return;
	}
	#endif

	if ((dir = opendir(pathname)) == NULL)
	{
		printf("Open dir error......\n");
		g_stop = 1;
		return;
	}
	memset(name_d, 0, NAME_MAX_VALUE);
	while ((ptr = readdir(dir)) != NULL)
	{																			
		if(strcmp(ptr->d_name,".") == 0 || strcmp(ptr->d_name,"..") == 0) {  //current dir OR parent dir
			//printf("d_name .  ..\n");
			continue;
		}else if(ptr->d_type == 8 /*file*/) {
			memset(name_d, 0, NAME_MAX_VALUE);
			memset(tmp_nnaDetectResult->insi_name, 0, NAME_MAX_VALUE);
			sprintf(name_d, "%s%s", pathname, ptr->d_name);
			//printf("11111 %s\n", ptr->d_name);
			if(strstr(name_d, ".jpg") != NULL) {			
				pic_src = (char *)DecodeImageFromFile(name_d, &Width, &Height, &Channels);		// inline include malloc fun, need free 
				if(pic_src == NULL) {
					printf("pic_src == NULL, file name %s\n", name_d);
					continue;
				}
				printf("00000 Width %d, Height %d, Channels %d\n", Width, Height, Channels);
				//nna_save_buf((char *)pic_src, Width*Height*3, "./yuantu_rgb.bin");
				memcpy(tmp_nnaDetectResult->insi_name, ptr->d_name, strlen(ptr->d_name));
				//int kkk = 0;
				//char aaaa[256] = {0, };
				//kkk = sprintf(&aaaa[kkk], "./wt/%s_", ptr->d_name);
				//sprintf(&aaaa[kkk], "%s", "after_wt.bin");				
				#if 1
				tmp_nnaDetectResult->c = 'J';
				memcpy(g_nnaInfo.mem_img.addr_vir, pic_src, Width * Height * Channels);
				#else
				tmp_nnaDetectResult->c = 'Y';
				nna_rgb2yuv((unsigned char *)g_nnaInfo.mem_img.addr_vir, (unsigned char *)pic_src, Width, Height);		//yuv420planer
				//nna_save_buf((char *)g_nnaInfo.mem_img.addr_vir, Width*Height*3/2, "./rgb2yuv420planer.bin");
				#if 0
				Size = Width * Height;
				memset((char *)(g_nnaInfo.mem_img.addr_vir + Size), 0x80, Size / 2);		//black white
				#else
				//*
				for(int i=0;i<Width * Height;i++) {
					// *((char *)(g_nnaInfo.mem_img.addr_vir + i)) += insi_Config.y_add_num;
					int tmp = *((char *)(g_nnaInfo.mem_img.addr_vir + i));
					tmp += insi_Config.y_add_num;
					tmp = tmp > 255 ? 255 : tmp;
					tmp = tmp < 0 ? 0 : tmp;
					*((char *)(g_nnaInfo.mem_img.addr_vir + i)) = tmp;
				}
				// */
				#endif
				#endif
				//printf("1111 Width %d, Height %d, Channels %d\n", Width, Height, Channels);
				//memcpy(g_nnaInfo.mem_img.addr_vir, yuv_buf, Width * Height * 3 / 2);
				SL_RunNNA(&g_nnaInfo, (char *)g_nnaInfo.mem_img.addr_phy, Width, Height, tmp_nnaDetectResult, g_nnaDetectMode);
				//printf("22222 %s, %s\n", tmp_nnaDetectResult->insi_name, ptr->d_name);
				free(pic_src);
			}else if(strstr(name_d, ".bin") != NULL) {
				FILE *fd1 = fopen(name_d, "r");
				if(fd1 == NULL) {
					printf("fopen file failure......\n");
					continue;
				}
				if(strstr(name_d, "RGB") != NULL) {	
					tmp_nnaDetectResult->c = 'R';
					Width = 288;		//NNA_CALC_WIDTH;
					Height = 288;		//NNA_CALC_HEIGHT;
					Size = Width * Height * 3;
				}else if(strstr(name_d, "yuv") != NULL) {
					memcpy(tmp_nnaDetectResult->insi_name, ptr->d_name, strlen(ptr->d_name));
					tmp_nnaDetectResult->c = 'W';
					Width = 640;		//NNA_CALC_WIDTH;
					Height = 360;		//NNA_CALC_HEIGHT;
					if(insi_Config.is_blackwhite != 1) {
						Size = Width * Height * 3 / 2;
					}else {
						Size = Width * Height;
						memset((char *)(g_nnaInfo.mem_img.addr_vir + Size), 0x80, Size / 2);		//black white
					}
				}else if(strstr(name_d, "BGR") != NULL) {
					tmp_nnaDetectResult->c = 'B';
					Width = 112;
					Height = 112;
					Size = Width * Height * 3;
				}else if(strstr(name_d, "_1") != NULL) {				//
					tmp_nnaDetectResult->c = 'W';					// == V , // ԭͼ
					Width = 640;
					Height = 360;
					Size = Width * Height * 3 / 2;
				}else {
					printf("format error, support RGB/BGR/YUV.bin file\n");
					fclose(fd1);
					continue;
				}
				{
					if(fread((char *)g_nnaInfo.mem_img.addr_vir, Size, 1, fd1) != 1) {
						printf("fread file failure......\n");
						fclose(fd1);
						continue;
					}
					fclose(fd1);
					SL_RunNNA(&g_nnaInfo, (char *)g_nnaInfo.mem_img.addr_phy, Width, Height, tmp_nnaDetectResult, g_nnaDetectMode);
				}
			}else {
				printf("format error, support .jpg and .bin file\n");
				continue;
			}
			char temp[NAME_MAX_VALUE] = {0, }, len = 0;
			len = strlen(tmp_nnaDetectResult->insi_name);
			if(len == 0) 	len = 1;
			if(tmp_nnaDetectResult->insi_name[len-1] == '\n') 	len--;			//delete \n
			
			strncpy(temp, tmp_nnaDetectResult->insi_name, len);		// \n
			strcat(temp, ".jpg");
			//printf("----%s\n", temp);
			char *ptr, log_buf[NAME_MAX_VALUE] = {0, };
			//if(strstr(temp, ptr->d_name) != NULL)
			ptr = strstr(temp, "PD ");
			if(ptr != NULL && atoi(ptr+3) != 0)
			{
				temp[len] = 0;		// .jpg
				Size = sprintf(log_buf, "count %d, file name %s, Time %.3f, %s, ==\n\n", count, name_d, tmp_nnaDetectResult->roundTime, temp);
				recg++;
			}else {
				temp[len] = 0;
				Size = sprintf(log_buf, "count %d, file name %s, Time %.3f, %s, !=\n\n", count, name_d, tmp_nnaDetectResult->roundTime, temp);
			}
			printf("%s", log_buf);
			#if 0
			if(fwrite(log_buf, Size, 1, fd) != 1) {
				printf("fwrite log file err\n");
				fclose(fd);
				closedir(dir);
				return;
			}
			#endif
			count++;
		}else if(ptr->d_type == 10 /*link file*/) {
			printf("d_name dir: %s\n", ptr->d_name);
			continue;
		}else if(ptr->d_type == 4 /*dir*/) {
			//char dir_name[256] = {0, };
			//sprintf(dir_name, "%s%s/", pathname, ptr->d_name);
			//get_all_files(dir_name, tmp_nnaDetectResult);
		}
		//usleep(5 * 1000);
	}	
	printf("------------------------recognation total num %d--------------------------\n", recg);
	closedir(dir);
	//fclose(fd);
}

static void *NNA_Proc(void* pThreadData)
{
    UserConfig* pUserCfg = (UserConfig*)pThreadData;
    assert(pUserCfg != NULL);

    VirVi_Params *pVirViParam = (VirVi_Params*)&pUserCfg->stVirViParams[VirVi_NNA];
    assert(pVirViParam != NULL);

    ISE_Params *pISEParams = (ISE_Params*)&pUserCfg->stISEParams;
    assert(pISEParams != NULL);

    NNA_Params *pNNAParams = (NNA_Params*)&pUserCfg->stNNAParams;
    assert(pNNAParams != NULL);

    VO_Params *pVOParams = (VO_Params*)&pUserCfg->stVOParams;
    assert(pVOParams != NULL);

    printf("Start NNA Thread!  mViDev[%d] mViChn[%d]\n",pVirViParam->iViDev,pVirViParam->iViChn);

    int iYSize = pNNAParams->iPicWidth * pNNAParams->iPicHeight;
    unsigned int iFrameIdx = 0;

    prctl(PR_SET_NAME,"NNA_Proc");
    int box_num = 0;

    OSD_XYBOX_T * tmp_nnaDetectResult = (OSD_XYBOX_T *)malloc(sizeof(OSD_XYBOX_T));
    memset(tmp_nnaDetectResult, 0, sizeof(OSD_XYBOX_T));

		
    while (1) {
        if(g_codecToggle || g_resolutionToggle) {
            printf("NNA Proc received toggle flag\n");
            break;
        }
        if(g_stop) {
            printf("NNA Proc received stop\n");
            break;
        }

		if(insi_Config.cap_pic_recg != 0) {
			nna_save_buf(g_nnaInfo.net_hd.wt_mem.addr_vir, g_nnaInfo.net_hd.wt_mem.size, "./before_wt.bin");
			
			get_all_files(PIC_DIR_LIST, tmp_nnaDetectResult);
			//get_all_files1(PIC_DIR_LIST, tmp_nnaDetectResult);
			
			nna_save_buf(g_nnaInfo.net_hd.wt_mem.addr_vir, g_nnaInfo.net_hd.wt_mem.size, "./after_wt.bin");
			g_stop = 1;
			continue;
		}

        // 获取一帧YUV数据
        VIDEO_FRAME_INFO_S stFrameInfo;

		#ifdef ENABLE_ISE
        if (SUCCESS ==AW_MPI_ISE_GetData(pISEParams->iseGroupCap[1].ISE_Group, pISEParams->iseGroupCap[1].PortCap_S[0].ISE_Port, &stFrameInfo, 500))
		#else
        if (SUCCESS == AW_MPI_VI_GetFrame(pVirViParam->iViDev, pVirViParam->iViChn, &stFrameInfo, 500))
		#endif
        {
            iFrameIdx++;
            if (iFrameIdx % 1 == 0)
			{
				tmp_nnaDetectResult->c = 'V';
                // 获取VirVi数据成功
                // Y,UV  
                int plan_size = stFrameInfo.VFrame.mWidth * stFrameInfo.VFrame.mHeight;
				//printf("[NNA]Get VFrame ID:%d plan_size:%d [%d %d] mPixelFormat:%d, mField %d, mVideoFormat %d, mCompressMode %d\n", stFrameInfo.mId, plan_size, \
				//			stFrameInfo.VFrame.mWidth,stFrameInfo.VFrame.mHeight,stFrameInfo.VFrame.mPixelFormat, stFrameInfo.VFrame.mField, \
				//			stFrameInfo.VFrame.mVideoFormat, stFrameInfo.VFrame.mCompressMode);

				memcpy((char *)g_nnaInfo.mem_img.addr_vir, (void *)stFrameInfo.VFrame.mpVirAddr[0], plan_size);
				if(insi_Config.is_blackwhite != 1) {
					memcpy((char *)(g_nnaInfo.mem_img.addr_vir + plan_size), (void *)stFrameInfo.VFrame.mpVirAddr[1], plan_size/2);		//color
				}else {				
					memset((char *)(g_nnaInfo.mem_img.addr_vir + plan_size), 0x80, plan_size/2);			//black white
				}
				
				#ifdef ENABLE_ISE
				AW_MPI_ISE_ReleaseData(pISEParams->iseGroupCap[1].ISE_Group, pISEParams->iseGroupCap[1].PortCap_S[0].ISE_Port, &stFrameInfo);
				#else
				AW_MPI_VI_ReleaseFrame(pVirViParam->iViDev, pVirViParam->iViChn, &stFrameInfo);
				#endif

				box_num = SL_RunNNA(&g_nnaInfo, (char *)g_nnaInfo.mem_img.addr_phy, pNNAParams->iPicWidth, pNNAParams->iPicHeight, tmp_nnaDetectResult, g_nnaDetectMode);
				//printf("\n\n %s, box_num = %d, osd_box_num = %d!!!\n\n", __func__, box_num, g_nnaDetectResult->osd_box_num);

                pthread_mutex_lock(&g_stResult_lock);
                memcpy(g_nnaDetectResult, tmp_nnaDetectResult, sizeof(OSD_XYBOX_T));
                pthread_mutex_unlock(&g_stResult_lock);

				if(insi_Config.face_upload == 1) {
	                if(g_nnaDetectMode == 1 && g_face_pic_flag == 1) {
	                    draw_box*    detectBox = g_nnaDetectResult->boxRect;
	                    for(int k=0; k<box_num; k++) {
	                        memcpy(m_yuv, stFrameInfo.VFrame.mpVirAddr[0], iYSize);
	                        memcpy(m_yuv+iYSize,  stFrameInfo.VFrame.mpVirAddr[1],  iYSize/2);
							int wid = detectBox[k].x2 - detectBox[k].x1;
							int hei = detectBox[k].y2 - detectBox[k].y1;
	                        int xCenter = detectBox[k].x1 + wid / 2;
	                        int yCenter = detectBox[k].y1 + hei / 2;
							
	                        if (xCenter > NNA_CALC_WIDTH/4 && xCenter < NNA_CALC_WIDTH *3/4  && yCenter > NNA_CALC_HEIGHT/4 && yCenter < NNA_CALC_HEIGHT *3/4)
							{
								printf("capture jpg start....\n");
	                            // memset(m_cutyuv, 0, MAX_FRAME_BUF_SiZE);
	                            // memset(m_rgb, 0, MAX_FRAME_BUF_SiZE);
	                            cutYuv(m_cutyuv, m_yuv, detectBox[k].x1, detectBox[k].y1, wid, hei, pNNAParams->iPicWidth, pNNAParams->iPicHeight);
	                            NV21_T_RGB((unsigned char*)m_cutyuv, m_rgb, wid, hei);
	                            saveImage("./b.jpg","_box", wid, hei, 3, (unsigned char *)m_rgb, iFrameIdx, k, g_nnaDetectResult->boxScore[k]);
	                        }
	                    }
						g_face_pic_flag = 0;
	                }
				}
            }
			else {
				#ifdef ENABLE_ISE
				AW_MPI_ISE_ReleaseData(pISEParams->iseGroupCap[1].ISE_Group, pISEParams->iseGroupCap[1].PortCap_S[0].ISE_Port, &stFrameInfo);
				#else
				AW_MPI_VI_ReleaseFrame(pVirViParam->iViDev, pVirViParam->iViChn, &stFrameInfo);
				#endif
			}
        } else {
            // 获取VirVi数据失败
            alogw("dev = %d  chn = %d GetFrame Failed!", pVirViParam->iViDev, pVirViParam->iViChn);
            usleep(20 * 1000);
        }
        //usleep(100*1000);
        // usleep(1000*1000/pNNAParams->iFrmRate);
    }

    free(tmp_nnaDetectResult);

    printf("NNA_Proc_exit!!\n");
	if(insi_Config.support_rtsp == 1) {
		g_sEventLoop->quit();
	}

    return NULL;
}

#define LAYER_OFF	300

static void *VENC_Proc(void* pThreadData)
{
    int res_boxnum = 0;
	FILE* fd_out;
    VencHeaderData pH26xSpsPpsInfo= {0};
	VencHeaderData pH26xSpsPpsInfo_rtsp= {0};

    UserConfig* pUserCfg = (UserConfig*)pThreadData;
    assert(pUserCfg != NULL);

    VirVi_Params *pVirViParam = (VirVi_Params*)&pUserCfg->stVirViParams[VirVi_VENC];
    assert(pVirViParam != NULL);

    ISE_Params *pISEParams = (ISE_Params*)&pUserCfg->stISEParams;
    assert(pISEParams != NULL);

    VENC_Params *pVENCParams = (VENC_Params*)&pUserCfg->stVENCParams;
    assert(pVENCParams != NULL);

    MUX_Params *pMUXParams = (MUX_Params*)&pUserCfg->stMUXParams;
    assert(pMUXParams != NULL);

    VO_Params *pVOParams = (VO_Params*)&pUserCfg->stVOParams;
    assert(pVOParams != NULL);

    printf("Start VEnc Thread!  mViDev[%d] mViChn[%d]\n",pVirViParam->iViDev,pVirViParam->iViChn);

    VENC_CHN mVeChnH26x = pVENCParams->VeChnH26x; // VENC_CHN_0
    create_venc(pVENCParams, mVeChnH26x, pVENCParams->mVideoEncoderFmt);
    AW_MPI_VENC_StartRecvPic(mVeChnH26x);

	if(insi_Config.save_h26x_file == 1) {
	    // 打开文件，写入SPS，PPS头
	    fd_out = fopen(pVENCParams->szOutputFile, "wb");
	    if (NULL == fd_out) {
	        printf("OPEN %s failed!!\n", pVENCParams->szOutputFile);
	        exit(0);
	    }

	    if (g_codecFormat == CODEC_H264) {
	        AW_MPI_VENC_GetH264SpsPpsInfo(mVeChnH26x, &pH26xSpsPpsInfo);
	    } else if (g_codecFormat == CODEC_H265) {
	        AW_MPI_VENC_GetH265SpsPpsInfo(mVeChnH26x, &pH26xSpsPpsInfo);
	    }

	    if (pH26xSpsPpsInfo.nLength) {
	        fwrite(pH26xSpsPpsInfo.pBuffer, 1, pH26xSpsPpsInfo.nLength, fd_out);
	    }
	}
	if(insi_Config.support_rtsp == 1) {
	    if (g_codecFormat == CODEC_H264) {
	        AW_MPI_VENC_GetH264SpsPpsInfo(mVeChnH26x, &pH26xSpsPpsInfo_rtsp);
	    } else if (g_codecFormat == CODEC_H265) {
	        AW_MPI_VENC_GetH265SpsPpsInfo(mVeChnH26x, &pH26xSpsPpsInfo_rtsp);
	    }
	    if (0 >= pH26xSpsPpsInfo_rtsp.nLength) {
	        if (g_codecFormat == CODEC_H264) {
	            printf("AW_MPI_VENC_GetH264SpsPpsInfo for rtsp failed!!\n");
	        } else if (g_codecFormat == CODEC_H265) {
	            printf("AW_MPI_VENC_GetH265SpsPpsInfo for rtsp failed!!\n");
	        }
	        exit(0);
	    }
	}
    VENC_PACK_S mpPack;
    VENC_STREAM_S vencFrame;
    memset(&vencFrame, 0, sizeof(VENC_STREAM_S));
    vencFrame.mpPack = &mpPack;
    vencFrame.mPackCount = 1;
    int iFrameIdx = 0;
    prctl(PR_SET_NAME,"VENC_Proc");
    // while (iFrameIdx < pVENCParams->iFrameNum)
#ifdef USE_RNG_ATTR
    RGN_ATTR_S stRgnAttr;
    RGN_CHN_ATTR_S stRgnChnAttr;
    memset(&stRgnAttr, 0, sizeof(RGN_ATTR_S));
    memset(&stRgnChnAttr, 0, sizeof(RGN_CHN_ATTR_S));
    MPP_CHN_S viChn = {MOD_ID_VIU, pVirViParam->iViDev,pVirViParam->iViChn};
    stRgnAttr.enType = ORL_RGN;
#endif

    OSD_Params stOSDParams;
    memset(&stOSDParams, 0 , sizeof(OSD_Params));
    stOSDParams.mOverlayHandle  = 100;
    MPP_CHN_S VeChn = {MOD_ID_VENC, 0, mVeChnH26x};
    memcpy(&stOSDParams.mMppChn, &VeChn, sizeof(MPP_CHN_S));
    destroy_osd(&stOSDParams);
    stOSDParams.x = 32;		//if chinese, 16*2=32
    stOSDParams.y = 32;
    sl_create_osd(&stOSDParams, 1);
	
    // show frame time
    OSD_Params fmOSDParams;
    memset(&fmOSDParams, 0 , sizeof(OSD_Params));
    fmOSDParams.mOverlayHandle  = 101;
    memcpy(&fmOSDParams.mMppChn, &VeChn, sizeof(MPP_CHN_S));
    destroy_osd(&fmOSDParams);
    fmOSDParams.x = 32;
    fmOSDParams.y = 80;
    sl_create_osd(&fmOSDParams, 2);

#ifdef SAVE_MP4_FILE
    VENC_CHN mVeChnMux = pVENCParams->VeChnMux; // VENC_CHN_1
    create_venc(pVENCParams, mVeChnMux, pVENCParams->mVideoEncoderFmt);

    MUX_GRP mMuxGrp = 0;
    int fd_mux = open(pMUXParams->szOutputFile, O_RDWR | O_CREAT, 0666);
    if (fd_mux < 0) {
        printf("OPEN %s failed!!\n", pMUXParams->szOutputFile);
        exit(0);
    }
    create_mux(pVENCParams, mMuxGrp, mVeChnMux, fd_mux);

    MPP_CHN_S MuxGrp = {MOD_ID_MUX, 0, mMuxGrp};
    MPP_CHN_S VeMuxChn = {MOD_ID_VENC, 0, mVeChnMux};
    AW_MPI_SYS_Bind(&VeMuxChn, &MuxGrp);
    AW_MPI_VENC_StartRecvPic(mVeChnMux);
    AW_MPI_MUX_StartGrp(mMuxGrp);
#endif

    while (1) {
        if(g_codecToggle || g_resolutionToggle) {
            printf("VENC Proc received toggle flag\n");
            break;
        }
        if(g_stop) {
            printf("VENC Proc received stop\n");
            break;
        }
	
        // 获取一帧YUV数据
        VIDEO_FRAME_INFO_S stFrameInfo;
#ifdef ENABLE_ISE
        if (SUCCESS ==AW_MPI_ISE_GetData(pISEParams->iseGroupCap[0].ISE_Group, pISEParams->iseGroupCap[0].PortCap_S[0].ISE_Port, &stFrameInfo, 500))
#else
        if (SUCCESS == AW_MPI_VI_GetFrame(pVirViParam->iViDev, pVirViParam->iViChn, &stFrameInfo, 500))
#endif
        {
            stFrameInfo.VFrame.mWidth           = pVENCParams->srcWidth;
            stFrameInfo.VFrame.mHeight          = pVENCParams->srcHeight;
            stFrameInfo.VFrame.mOffsetLeft      = 0;
            stFrameInfo.VFrame.mOffsetTop       = 0;
            stFrameInfo.VFrame.mOffsetRight     = stFrameInfo.VFrame.mOffsetLeft + stFrameInfo.VFrame.mWidth;
            stFrameInfo.VFrame.mOffsetBottom    = stFrameInfo.VFrame.mOffsetTop + stFrameInfo.VFrame.mHeight;
            stFrameInfo.VFrame.mField           = VIDEO_FIELD_FRAME;
            stFrameInfo.VFrame.mVideoFormat     = VIDEO_FORMAT_LINEAR;
            stFrameInfo.VFrame.mCompressMode    = COMPRESS_MODE_NONE;

#ifdef USE_RNG_ATTR
            int rgnRet = 0;
            //先destroy上一次创建的RNG
            for(int index = 0; index < res_boxnum; index++) {
                rgnRet = AW_MPI_RGN_Destroy(index);
                if(SUCCESS != rgnRet) {
                    printf("AW_MPI_RGN_Destroy failed, program abort!! index = %d, rgnRet = %d\n", index, rgnRet);
                }
            }
            res_boxnum = 0;
#endif
            pthread_mutex_lock(&g_stResult_lock);
            if(g_nnaDetectResult->osd_box_num > 0) {
                res_boxnum = g_nnaDetectResult->osd_box_num;
                for (int i = 0; i < res_boxnum; i++) {
                    int iBodyLeft   = (g_nnaDetectResult->boxRect[i].x1 * VI_CAPTURE_WIDTH / NNA_CALC_WIDTH) & 0xfffe;
                    int iBodyTop    = (g_nnaDetectResult->boxRect[i].y1 * VI_CAPTURE_HEIGHT / NNA_CALC_HEIGHT) & 0xfffe;
                    int iBodyRight  = (g_nnaDetectResult->boxRect[i].x2 * VI_CAPTURE_WIDTH / NNA_CALC_WIDTH) & 0xfffe;
                    int iBodyBottom = (g_nnaDetectResult->boxRect[i].y2 * VI_CAPTURE_HEIGHT / NNA_CALC_HEIGHT) & 0xfffe;
                    int iBodyWidth  = iBodyRight - iBodyLeft;
                    int iBodyHeight = iBodyBottom - iBodyTop;
					int color =  g_nnaDetectResult->clrRect[i];
					#ifdef USE_RNG_ATTR
					//AW_MPI_RGN_SetDisplayAttr(RGN_HANDLE Handle, const MPP_CHN_S *pstChn, const RGN_CHN_ATTR_S *pstChnAttr);

					//新建新的RNG
                    rgnRet = AW_MPI_RGN_Create(i, &stRgnAttr);
                    if(rgnRet != SUCCESS) {
                        printf("AW_MPI_RGN_Create error! why create ORL region fail?[0x%x]\n", rgnRet);
                        break;
                    }
                    stRgnChnAttr.unChnAttr.stOrlChn.stRect.X = iBodyLeft;
                    stRgnChnAttr.unChnAttr.stOrlChn.stRect.Y = iBodyTop;
                    stRgnChnAttr.unChnAttr.stOrlChn.stRect.Width = iBodyWidth;
                    stRgnChnAttr.unChnAttr.stOrlChn.stRect.Height = iBodyHeight;
                    stRgnChnAttr.bShow = TRUE;
                    stRgnChnAttr.enType = ORL_RGN;
                    stRgnChnAttr.unChnAttr.stOrlChn.enAreaType = AREA_RECT;
                    stRgnChnAttr.unChnAttr.stOrlChn.mColor = g_nnaDetectResult->clrRect[i];
                    stRgnChnAttr.unChnAttr.stOrlChn.mThick = 1;
                    stRgnChnAttr.unChnAttr.stOrlChn.mLayer = i;
                    // printf("=====>RNG box:%d[X:%d Y:%d W:%d H:%d]\n",res_boxnum,iBodyLeft,iBodyTop,iBodyWidth,iBodyHeight);
                    rgnRet = AW_MPI_RGN_AttachToChn(i, &viChn, &stRgnChnAttr);
                    if(SUCCESS != rgnRet) {
                        printf("AW_MPI_RGN_AttachToChn error! when attach to vi channel[%d,%d] fail? rgnRet %d\n",pVirViParam->iViDev,pVirViParam->iViChn, rgnRet);
                    }
					#else
                    //int64_t time1 = CDX_GetSysTimeUsMonotonic();
                    if(insi_Config.rect_keep == 1) {
                    	DrawRect_Nv21((char *)stFrameInfo.VFrame.mpVirAddr[0], (char *)stFrameInfo.VFrame.mpVirAddr[1], stFrameInfo.VFrame.mWidth, \
                    				stFrameInfo.VFrame.mHeight, iBodyLeft, iBodyTop, iBodyWidth, iBodyHeight, 3, color);
                    }
					#endif
                }
            } 
			{
                char bitRate[64] = {0};
                int osdRet;
                // show bitRate
                sprintf(bitRate, "Face Detection:%d,  %.3f",g_nnaDetectResult->osd_box_num, g_nnaDetectResult->roundTime);
                osdRet = sl_update_osd(&stOSDParams, bitRate, strlen(bitRate), 1);
                if(SUCCESS != osdRet) {
                    printf("[rate] update osd error! vi channel[%d,%d] fail?\n",pVirViParam->iViDev,pVirViParam->iViChn);
                }
                // show frame time
                sprintf(bitRate, "%s", g_nnaDetectResult->insi_name);
                osdRet = sl_update_osd(&fmOSDParams, bitRate, strlen(bitRate), 2);
                if(SUCCESS != osdRet) {
                    printf("[frame] update osd error! vi channel[%d,%d] fail?\n",pVirViParam->iViDev,pVirViParam->iViChn);
                }                
            }

            pthread_mutex_unlock(&g_stResult_lock);

            // printf("==>VENC iFrameIdx:%d\n\n",iFrameIdx);
            // 送入编码通道
            AW_MPI_VENC_SendFrame(mVeChnH26x, &stFrameInfo, 60);
#ifdef SAVE_MP4_FILE
            AW_MPI_VENC_SendFrame(mVeChnMux, &stFrameInfo, 60);
#endif
            // 读取编码结果
            int ret = AW_MPI_VENC_GetStream(mVeChnH26x, &vencFrame, 20);
            if (SUCCESS == ret) {
				if(insi_Config.save_h26x_file == 1) {
	                //保存H264 or H265文件
	                if (vencFrame.mpPack->mLen0) {
	                    fwrite(vencFrame.mpPack->mpAddr0, 1, vencFrame.mpPack->mLen0, fd_out);
	                }
	                if (vencFrame.mpPack->mLen1) {
	                    fwrite(vencFrame.mpPack->mpAddr1, 1, vencFrame.mpPack->mLen1, fd_out);
	                }
				}
				if(insi_Config.support_rtsp == 1) {
	                if (NULL != g_rtspServer && vencFrame.mpPack != NULL && vencFrame.mpPack->mLen0 > 0) {
	                    uint64_t pts = vencFrame.mpPack->mPTS;
	                    xop::FrameType iFrame_type = xop::VIDEO_FRAME_P;
	                    int len = 0;
	                    // VencHeaderData head_info;
	                    unsigned char *buf = g_stream_buf;
	                    if ( (g_codecFormat == CODEC_H264 && H264E_NALU_ISLICE == vencFrame.mpPack->mDataType.enH264EType) \
	                         || (g_codecFormat == CODEC_H265 && H265E_NALU_ISLICE == vencFrame.mpPack->mDataType.enH265EType)
	                       ) {
	                        if (NULL == pH26xSpsPpsInfo_rtsp.pBuffer) {
	                            printf("pH26xSpsPpsInfo_rtsp.pBuffer = NULL!!\n");
	                        }
	                        /* Get sps/pps first */
	                        memcpy(g_stream_buf, pH26xSpsPpsInfo_rtsp.pBuffer, pH26xSpsPpsInfo_rtsp.nLength);
	                        len += pH26xSpsPpsInfo_rtsp.nLength;
	                        iFrame_type = xop::VIDEO_FRAME_I;
	                    }

	                    if (MAX_FRAME_BUF_SiZE > len + vencFrame.mpPack->mLen0) {
	                        memcpy(g_stream_buf + len, vencFrame.mpPack->mpAddr0, vencFrame.mpPack->mLen0);
	                        len += vencFrame.mpPack->mLen0;
	                    }

	                    if (vencFrame.mpPack->mLen1 > 0) {
	                        if (MAX_FRAME_BUF_SiZE > (len + vencFrame.mpPack->mLen1)) {
	                            memcpy(g_stream_buf + len, vencFrame.mpPack->mpAddr1, vencFrame.mpPack->mLen1);
	                            len += vencFrame.mpPack->mLen1;
	                        }
	                    }

	                    SendFrame2Rtsp(len, buf, xop::VIDEO_FRAME_I);
	                }
				}
                AW_MPI_VENC_ReleaseStream(mVeChnH26x, &vencFrame);
            }
            // 释放YUV数据
#ifdef ENABLE_ISE
            AW_MPI_ISE_ReleaseData(pISEParams->iseGroupCap[0].ISE_Group, pISEParams->iseGroupCap[0].PortCap_S[0].ISE_Port, &stFrameInfo);
#else
            AW_MPI_VI_ReleaseFrame(pVirViParam->iViDev, pVirViParam->iViChn, &stFrameInfo);
#endif
        } else {
            // 获取VirVi数据失败
            alogw("dev = %d  chn = %d GetFrame Failed!", pVirViParam->iViDev, pVirViParam->iViChn);
            usleep(20 * 1000);
        }
        //iFrameIdx++;
    }
#ifdef SAVE_MP4_FILE
    AW_MPI_VENC_StopRecvPic(mVeChnMux);
    AW_MPI_MUX_StopGrp(mMuxGrp);
    destroy_mux(mMuxGrp);
    close(fd_mux);
    destroy_venc(mVeChnMux);
#endif
    AW_MPI_VENC_StopRecvPic(mVeChnH26x);
    destroy_venc(mVeChnH26x);
	if(insi_Config.save_h26x_file == 1) {
    	fclose(fd_out);
	}
#ifdef USE_RNG_ATTR
    destroy_osd(&stOSDParams);
    destroy_osd(&tmOSDParams);
    destroy_osd(&nmOSDParams);
#endif
    printf("Venc_Proc_exit!!\n");
    return NULL;
}


static void* LCD_Proc(void* pThreadData)
{
	OSD_XYBOX_T osd_sybox_tmp;
    int res_boxnum=0;
    UserConfig* pUserCfg = (UserConfig*)pThreadData;
    assert(pUserCfg != NULL);
    VirVi_Params *pVirViParam = (VirVi_Params*)&pUserCfg->stVirViParams[VirVi_LCD];
    assert(pVirViParam != NULL);

    VO_Params *pVOParams = (VO_Params*)&pUserCfg->stVOParams;
    assert(pVOParams != NULL);

    printf("g2d open\n");
    open_g2d_device();
    
#ifdef ROTATE
    MallocVideoFrame(&pVOParams->mVoFrame, DEMO_MM_PIXEL_FORMAT, NNA_CALC_HEIGHT, NNA_CALC_WIDTH);
#endif
#ifdef USE_RNG_ATTR
    RGN_ATTR_S stRgnAttr;
    RGN_CHN_ATTR_S stRgnChnAttr;
    memset(&stRgnAttr, 0, sizeof(RGN_ATTR_S));
    memset(&stRgnChnAttr, 0, sizeof(RGN_CHN_ATTR_S));
    MPP_CHN_S viChn = {MOD_ID_VIU, pVirViParam->iViDev,pVirViParam->iViChn};
    stRgnAttr.enType = ORL_RGN;
#endif
    printf("LCD_Proc_enter!!\n");

    while(1) {
        if(g_codecToggle || g_resolutionToggle) {
            printf("LCD Proc received toggle flag\n");
            break;
        }
        if(g_stop) {
            printf("LCD Proc received stop\n");
            break;
        }
		//keyboard_check();

		/*
		int vl = 10;
		AW_MPI_ISP_GetBrightness(0, &vl);
		printf("---AW_MPI_ISP_GetBrightness %d\n", vl);
		
		vl--;
		AW_MPI_ISP_SetBrightness(0, vl);
		
		AW_MPI_ISP_GetBrightness(0, &vl);
		printf("+++AW_MPI_ISP_GetBrightness %d\n", vl);
		// */
		
		VIDEO_FRAME_INFO_S stFrameInfo;				
        if (SUCCESS == AW_MPI_VI_GetFrame(pVirViParam->iViDev, pVirViParam->iViChn, &stFrameInfo, 200)) {
			//printf("lcd AW_MPI_VI GetFrame success\n");
			#ifdef ROTATE
			rotate_frame(&stFrameInfo, &pVOParams->mVoFrame);
			#endif

			#ifdef USE_RNG_ATTR
            // --------------------------------------------------------------------------------
            // clean previous boxes
            // --------------------------------------------------------------------------------
            int rgnRet = 0;
            // destroy created RNG
            for(int index = 0; index < res_boxnum; index++) {
                rgnRet = AW_MPI_RGN_Destroy(index + LAYER_OFF);
                if(SUCCESS != rgnRet) {
                    printf("AW_MPI_RGN_Destroy failed, program abort!! index = %d, rgnRet = %d\n", index, rgnRet);
                }
            }
            res_boxnum = 0;
			#endif
            // --------------------------------------------------------------------------------
            // draw current boxes
            // --------------------------------------------------------------------------------
            pthread_mutex_lock(&g_stResult_lock);
			memcpy(&osd_sybox_tmp, g_nnaDetectResult, sizeof(OSD_XYBOX_T));
			pthread_mutex_unlock(&g_stResult_lock);
			char title[64] = {0};
			Ych_OSD_Params	stYchOSDParams;
			
			if(osd_sybox_tmp.osd_box_num > 0) {
				res_boxnum = osd_sybox_tmp.osd_box_num;
				for (int i = 0; i < res_boxnum; i++) {
					int iBodyLeft	= (osd_sybox_tmp.boxRect[i].x1 * VI_CAPTURE_WIDTH / NNA_CALC_WIDTH) & 0xfffe;
					int iBodyTop	= (osd_sybox_tmp.boxRect[i].y1 * VI_CAPTURE_HEIGHT / NNA_CALC_HEIGHT) & 0xfffe;
					int iBodyRight	= (osd_sybox_tmp.boxRect[i].x2 * VI_CAPTURE_WIDTH / NNA_CALC_WIDTH) & 0xfffe;
					int iBodyBottom = (osd_sybox_tmp.boxRect[i].y2 * VI_CAPTURE_HEIGHT / NNA_CALC_HEIGHT) & 0xfffe;
					int iBodyWidth	= iBodyRight - iBodyLeft;
					int iBodyHeight = iBodyBottom - iBodyTop;
					int color =  osd_sybox_tmp.clrRect[i];
					
					#ifdef USE_RNG_ATTR
					// create new RNG
					rgnRet = AW_MPI_RGN_Create(i + LAYER_OFF, &stRgnAttr);
					if(rgnRet != SUCCESS) {
						printf("AW_MPI_RGN_Create error! why create ORL region fail?[0x%x]\n", rgnRet);
						break;
					}

					stRgnChnAttr.unChnAttr.stOrlChn.stRect.X		= iBodyLeft;
					stRgnChnAttr.unChnAttr.stOrlChn.stRect.Y		= iBodyTop;
					stRgnChnAttr.unChnAttr.stOrlChn.stRect.Width	= iBodyWidth;
					stRgnChnAttr.unChnAttr.stOrlChn.stRect.Height	= iBodyHeight;
					stRgnChnAttr.bShow = TRUE;
					stRgnChnAttr.enType = ORL_RGN;
					stRgnChnAttr.unChnAttr.stOrlChn.enAreaType = AREA_RECT;
					stRgnChnAttr.unChnAttr.stOrlChn.mColor = osd_sybox_tmp.clrRect[i];
					stRgnChnAttr.unChnAttr.stOrlChn.mThick = 1;
					stRgnChnAttr.unChnAttr.stOrlChn.mLayer = i;
					// printf("=====>RNG box:%d[X:%d Y:%d W:%d H:%d]\n",res_boxnum,iBodyLeft,iBodyTop,iBodyWidth,iBodyHeight);
					rgnRet = AW_MPI_RGN_AttachToChn(i + LAYER_OFF, &viChn, &stRgnChnAttr);
					//rgnRet = AW_MPI_RGN_AttachToChn(i + LAYER_OFF, &voChn, &stRgnChnAttr);
					if(SUCCESS != rgnRet) {
						printf("AW_MPI_RGN_AttachToChn error! when attach to vi channel[%d,%d] fail? rgnRet %d\n",pVirViParam->iViDev,pVirViParam->iViChn, rgnRet);
					}
					#else
					if(iBodyWidth > 0 && iBodyHeight > 0) {
						DrawRect_Nv21((char *)stFrameInfo.VFrame.mpVirAddr[0], (char *)stFrameInfo.VFrame.mpVirAddr[1], \
                      			stFrameInfo.VFrame.mWidth, stFrameInfo.VFrame.mHeight, iBodyLeft, iBodyTop, iBodyWidth, iBodyHeight, 2, color);

						stYchOSDParams.x		= iBodyLeft;							
						stYchOSDParams.y		= (iBodyTop - 32) > 80 ? (iBodyTop - 32) : 80;
						sprintf(title, "%d", osd_sybox_tmp.frame_cnt);
						//printf("osd_sybox_tmp.frame_cnt %d\n", osd_sybox_tmp.frame_cnt);
						sl_update_osd_y(&stYchOSDParams, title, strlen(title));	
					}				
					#endif
				}
			}
			// --------------------------------------------------------------------------------
			// show OSD
			// --------------------------------------------------------------------------------			
			stYchOSDParams.bkEnable = FALSE;	//TRUE;
			stYchOSDParams.bkWeight = 0x00;		//0xFF;
			stYchOSDParams.ftWeight = 0xFF;		//0x80;
			#ifdef ROTATE
			stYchOSDParams.imgSrc	= (char *)pVOParams->mVoFrame.VFrame.mpVirAddr[0];
			stYchOSDParams.imgWidth = VI_CAPTURE_HEIGHT;	//VI_CAPTURE_WIDTH;
			stYchOSDParams.imgHeight= VI_CAPTURE_WIDTH;		//VI_CAPTURE_HEIGHT;
			#else
			stYchOSDParams.imgSrc	= (char *)stFrameInfo.VFrame.mpVirAddr[0];
			stYchOSDParams.imgWidth = VI_CAPTURE_WIDTH;
			stYchOSDParams.imgHeight= VI_CAPTURE_HEIGHT;
			#endif						
			
			switch(g_nnaDetectMode) {

			case NNA_MODE_FD:		// human face detection
				stYchOSDParams.x		= 16;							
				stYchOSDParams.y		= 48;
				sl_update_osd_y(&stYchOSDParams, osd_sybox_tmp.insi_name, strlen(osd_sybox_tmp.insi_name));	

				sprintf(title, "Face Num:%d  %.2f  Ver:%d.%02d.%02d--%d.%02d.%02d.%02d", osd_sybox_tmp.osd_box_num, \
								osd_sybox_tmp.roundTime, version.main_v, version.sample_v, version.nna_v, \
								version.nna_m, version.nna_fir, version.nna_sec, version.nna_thr);
				break;
				
			default:
				break;
			}
			#ifdef ROTATE
			stYchOSDParams.x		= 400;
			#else
			stYchOSDParams.x		= 16;							
			#endif
			stYchOSDParams.y		= 16;
			sl_update_osd_y(&stYchOSDParams, title, strlen(title));	
			//pthread_mutex_unlock(&g_stResult_lock);

#ifdef ROTATE
            SendFrame2VO(pVOParams, &pVOParams->mVoFrame, 50);
#else
            SendFrame2VO(pVOParams, &stFrameInfo, 50);
#endif

            AW_MPI_VI_ReleaseFrame(pVirViParam->iViDev, pVirViParam->iViChn, &stFrameInfo);
			usleep(10 * 1000);
        }else {
			alogw("dev = %d  chn = %d GetFrame Failed!", pVirViParam->iViDev, pVirViParam->iViChn);
            usleep(20 * 1000);
        }
    }

#ifdef ROTATE
    FreeVideoFrame(&pVOParams->mVoFrame);
#endif

	printf("g2d close\n");
    close_g2d_device();
    printf("LCD_Proc_exit!!\n");

    return NULL;
}

// audio out thread, used to driver mic
static void* AO_Proc(void* pThreadData)
{
	int res_boxnum=0;
    UserConfig* pUserCfg = (UserConfig*)pThreadData;
    assert(pUserCfg != NULL);

    AO_Params *pAOParams = (AO_Params*)&pUserCfg->stAOParams;
    assert(pAOParams != NULL);

    printf("AO_Proc_enter!!\n");
	//create_ao(pAOParams);

    int nFrameSize 			= pAOParams->mConfigPara.mFrameSize;
    int nSampleRate 		= pAOParams->mConfigPara.mSampleRate;
    uint64_t nFrameInterval = 1000000*nFrameSize/nSampleRate; //128000us
    AUDIO_FRAME_S 	*pFrameInfo;
    int 			nReadLen;
	ERRORTYPE 		ret;
	int i = 0;

    while(1) {
        if(g_codecToggle || g_resolutionToggle) {
            printf("AO Proc received toggle flag\n");
            break;
        }
        if(g_stop) {
            printf("AO Proc received stop\n");
            break;
        }

#if NET_SUSPECT_USED
		//pthread_mutex_lock(&g_stResult_lock);
		if (g_nnaDetectMode != NNA_MODE_SUSPECT) {
			usleep(20 * 1000);
			continue;
		}
		else {
			res_boxnum = g_nnaDetectResult->osd_box_num;
			for (i = 0; i < res_boxnum; i++) {
				if (g_nnaDetectResult->susName[i]!=NULL)
					break;
			}
		}
#else
		usleep(20 * 1000);
		continue;

#endif		
		if (i == res_boxnum) {
			usleep(20 * 1000);
			continue;
		}
		//pthread_mutex_unlock(&g_stResult_lock);

		create_ao(pAOParams);
		uint64_t 	nPts 	= 0;   //unit:us

        while(1)
	    {
	        //request idle frame
	        pFrameInfo = pAOParams->mFrameManager.PrefetchFirstIdleFrame(&pAOParams->mFrameManager);
	        if(NULL == pFrameInfo)
	        {
	            pthread_mutex_lock(&pAOParams->mWaitFrameLock);
	            pFrameInfo = pAOParams->mFrameManager.PrefetchFirstIdleFrame(&pAOParams->mFrameManager);
	            if(pFrameInfo!=NULL)
	            {
	                pthread_mutex_unlock(&pAOParams->mWaitFrameLock);
	            }
	            else
	            {
	                pAOParams->mbWaitFrameFlag = 1;
	                pthread_mutex_unlock(&pAOParams->mWaitFrameLock);
	                cdx_sem_down_timedwait(&pAOParams->mSemFrameCome, 500);
	                continue;
	            }
	        }

	        //read pcm to idle frame
	        int nWantedReadLen = nFrameSize * pAOParams->mConfigPara.mChannelCnt * (pAOParams->mConfigPara.mBitWidth/8);
	        nReadLen = fread(pFrameInfo->mpAddr, 1, nWantedReadLen, pAOParams->mFpPcmFile);
	        if(nReadLen < nWantedReadLen)
	        {
	            int bEof = feof(pAOParams->mFpPcmFile);
	            if(bEof)
	            {
	                //alogd("read file finish!");
	                printf("AO Proc read file finish! ------\n");
	            }
	            break;
	        }
	        pFrameInfo->mTimeStamp = nPts;
	        nPts += nFrameInterval;
	        pFrameInfo->mId = pFrameInfo->mTimeStamp / nFrameInterval;
	        pAOParams->mFrameManager.UseFrame(&pAOParams->mFrameManager, pFrameInfo);

	        //send pcm to ao
	        ret = AW_MPI_AO_SendFrame(pAOParams->mAODev, pAOParams->mAOChn, pFrameInfo, 0);
	        if(ret != SUCCESS)
	        {
	            alogd("impossible, send frameId[%d] fail?", pFrameInfo->mId);
	            pAOParams->mFrameManager.ReleaseFrame(&pAOParams->mFrameManager, pFrameInfo->mId);
	        }
	    }
	    AW_MPI_AO_SetStreamEof(pAOParams->mAODev, pAOParams->mAOChn, 1, 1);
	    //cdx_sem_wait(&pAOParams->mSemEofCome);

		destroy_ao(pAOParams);
    }

	//destroy_ao(pAOParams);

    printf("AO_Proc_exit!!\n");

    return NULL;
}

static void* AUDIO_Proc(void* pThreadData)
{
    UserConfig* pUserCfg = (UserConfig*)pThreadData;
    assert(pUserCfg != NULL);
    AUDIO_Params *pAudioParams = (AUDIO_Params*)&pUserCfg->stAUDIOParams;
    assert(pAudioParams != NULL);
    AUDIO_STREAM_S stream;
    FILE* fd = fopen("./audio.aac", "wb");

    while(1) {
        if(g_codecToggle || g_resolutionToggle) {
            printf("AUDIO Proc received toggle flag\n");
            break;
        }
        if(g_stop) {
            printf("AUDIO Proc received stop\n");
            break;
        }
        if (SUCCESS == AW_MPI_AENC_GetStream(pAudioParams->mAencChn, &stream, 100)) {
			if(insi_Config.support_rtsp == 1) {
	            if (NULL != g_rtspServer && stream.pStream != NULL && stream.mLen > 0) {
	                //uint64_t pts = stream.mTimeStamp;
	                SendAudFrame2Rtsp(stream.mLen, stream.pStream, xop::AUDIO_FRAME);
	                //printf("send audio frame len 0x%x, pStream 0x%x\n", stream.mLen, stream.pStream);
	                fwrite(stream.pStream, 1, stream.mLen, fd);
	            }
			}
            AW_MPI_AENC_ReleaseStream(pAudioParams->mAencChn, &stream);
        }
    }
    fclose(fd);
    printf("AUDIO_Proc_exit!!\n");

    return NULL;
}

int audioconifg_init(UserConfig* pUserCfg)
{
    //Init AI and AENC Params
    pUserCfg->stAUDIOParams.mAiAttr.enSamplerate = (AUDIO_SAMPLE_RATE_E)8000;
    pUserCfg->stAUDIOParams.mAiAttr.u32ChnCnt = 1;
    pUserCfg->stAUDIOParams.mAiAttr.enBitwidth = (AUDIO_BIT_WIDTH_E)(16/8-1);
    pUserCfg->stAUDIOParams.mAiDev = 0;
    pUserCfg->stAUDIOParams.mAiChn= 0;
    pUserCfg->stAUDIOParams.mAencAttr.sampleRate = 8000;
    pUserCfg->stAUDIOParams.mAencAttr.channels = 1;
    pUserCfg->stAUDIOParams.mAencAttr.bitsPerSample = 16;
    pUserCfg->stAUDIOParams.mAencAttr.attachAACHeader = 1;
    pUserCfg->stAUDIOParams.mAencAttr.Type = PT_AAC;
    pUserCfg->stAUDIOParams.mAencChn = 0;
	#ifdef RUN_IN_A_CHIP
    pUserCfg->stThreadData[PROC_AUDIO].ProcessFunc  = NULL;
    #else
	pUserCfg->stThreadData[PROC_AUDIO].ProcessFunc	= AUDIO_Proc;
	#endif
    pUserCfg->stThreadData[PROC_AUDIO].mThreadID    = 0;
    pUserCfg->stThreadData[PROC_AUDIO].pPrivateData = NULL;
	
    return 0;
}

int videoconfig_init(UserConfig* pUserCfg)
{
    int count = 0;
    // Initial global vipp map
    g_VIPP_map[VIPP_0].bInit      = 0;
    g_VIPP_map[VIPP_0].width      = VI_CAPTURE_WIDTH;
    g_VIPP_map[VIPP_0].height     = VI_CAPTURE_HEIGHT;
    g_VIPP_map[VIPP_0].eformat    = DEMO_MM_PIXEL_FORMAT;
    g_VIPP_map[VIPP_0].frame_rate = g_frameRate;
    g_VIPP_map[VIPP_0].bMirror    = 0;
    g_VIPP_map[VIPP_0].bFlip      = 0;
	g_VIPP_map[VIPP_0].use 		  = 1;
	
    pUserCfg->stVirViParams[VirVi_VENC].iViDev = VIPP_0;
    pUserCfg->stVirViParams[VirVi_VENC].iViChn = VirViChn_0;

    g_VIPP_map[VIPP_1].bInit      = 0;
    g_VIPP_map[VIPP_1].width      = NNA_CALC_WIDTH;
    g_VIPP_map[VIPP_1].height     = NNA_CALC_HEIGHT;
    g_VIPP_map[VIPP_1].eformat    = DEMO_MM_PIXEL_FORMAT;
    g_VIPP_map[VIPP_1].frame_rate = g_frameRate;
    g_VIPP_map[VIPP_1].bMirror    = 0;
    g_VIPP_map[VIPP_1].bFlip      = 0;
	g_VIPP_map[VIPP_1].use        = 1;
	
    // Initial VirVi params
    pUserCfg->stVirViParams[VirVi_NNA].iViDev  = VIPP_1;
    pUserCfg->stVirViParams[VirVi_NNA].iViChn  = VirViChn_0;

    g_VIPP_map[VIPP_2].bInit      = 0;
    g_VIPP_map[VIPP_2].width      = NNA_CALC_WIDTH;
    g_VIPP_map[VIPP_2].height     = NNA_CALC_HEIGHT;
    g_VIPP_map[VIPP_2].eformat    = DEMO_MM_PIXEL_FORMAT;
    g_VIPP_map[VIPP_2].frame_rate = g_frameRate;
    g_VIPP_map[VIPP_2].bMirror    = 0;
    g_VIPP_map[VIPP_2].bFlip      = 0;
	g_VIPP_map[VIPP_2].use        = 1;
    // Initial VirVi params
    
    pUserCfg->stVirViParams[VirVi_LCD].iViDev  = VIPP_2;
    pUserCfg->stVirViParams[VirVi_LCD].iViChn  = VirViChn_0;

#ifdef ENABLE_ISE
    // Init ISE Params
    for (int i = 0; i < VirVi_MAX; i++) {
        pUserCfg->stISEParams.iseGroupCap[i].ISE_Group = i;  //Group ID
        pUserCfg->stISEParams.iseGroupCap[i].pGrpAttr.iseMode = ISEMODE_ONE_FISHEYE;
        pUserCfg->stISEParams.iseGroupCap[i].pGrpAttr.mPixelFormat = MM_PIXEL_FORMAT_YVU_SEMIPLANAR_420;
        /*ise group bind channel attr*/
#if 0
        pUserCfg->stISEParams.iseGroupCap[i].ISE_Group_S.mChnId = 0;
        pUserCfg->stISEParams.iseGroupCap[i].ISE_Group_S.mDevId = i;
        pUserCfg->stISEParams.iseGroupCap[i].ISE_Group_S.mModId = MOD_ID_ISE;
        pUserCfg->stISEParams.iseGroupCap[i].ISE_Port[0] = 0;   //Port ID
#endif
    }
#endif

    //Init NNA Params
    pUserCfg->stNNAParams.ePixFmt    = DEMO_MM_PIXEL_FORMAT;
    pUserCfg->stNNAParams.iPicWidth  = NNA_CALC_WIDTH;
    pUserCfg->stNNAParams.iPicHeight = NNA_CALC_HEIGHT;
    pUserCfg->stNNAParams.iFrmRate   = g_frameRate;
    pUserCfg->stNNAParams.iFrameNum  = TEST_FRAME_NUM;

    // Inital VENC params
    pUserCfg->stVENCParams.iFrameNum        = TEST_FRAME_NUM;//测试帧数

    if (g_codecFormat == CODEC_H264) {		
		if(insi_Config.save_h26x_file == 1) {
			FILE *fd_out = fopen(H26X_FILE_COUNT, "r+");
		    if (NULL == fd_out) {
		        printf("OPEN H26X_FILE_COUNT failed!!\n");
				return -1;
		    }
			fseek(fd_out, 0, SEEK_SET);  
			if(fread(&count, 4, 1, fd_out) != 1) 
			{
				printf("fread H26X_FILE_COUNT failed!!\n");
				count = 0;
				//return -1;
			}
			printf("save file count %d\n", count);
			count++;
			fseek(fd_out, 0, SEEK_SET); 
	        if(fwrite(&count, 4, 1, fd_out) != 1) {
				printf("fwrite H26X_FILE_COUNT failed!!\n");
				return -1;
	        }
			fclose(fd_out);
		}
        //strcpy(pUserCfg->stVENCParams.szOutputFile, "./nna.h264");  //输出文件
        sprintf(pUserCfg->stVENCParams.szOutputFile, "./nna_%d_%d_%d.h264", VI_CAPTURE_WIDTH, VI_CAPTURE_HEIGHT, count);
        pUserCfg->stVENCParams.mVideoEncoderFmt = PT_H264;
    } else if (g_codecFormat == CODEC_H265) {
        //strcpy(pUserCfg->stVENCParams.szOutputFile, "./nna.h265");  //输出文件
        sprintf(pUserCfg->stVENCParams.szOutputFile, "./nna_%d_%d_%d.h265", VI_CAPTURE_WIDTH, VI_CAPTURE_HEIGHT, count);
        pUserCfg->stVENCParams.mVideoEncoderFmt = PT_H265;
    }
    pUserCfg->stVENCParams.srcWidth         = VI_CAPTURE_WIDTH;
    pUserCfg->stVENCParams.srcHeight        = VI_CAPTURE_HEIGHT;
    pUserCfg->stVENCParams.srcSize          = VI_CAPTURE_WIDTH;
    pUserCfg->stVENCParams.srcPixFmt        = DEMO_MM_PIXEL_FORMAT; //NV21

    pUserCfg->stVENCParams.dstWidth         = VI_CAPTURE_WIDTH;
    pUserCfg->stVENCParams.dstHeight        = VI_CAPTURE_HEIGHT;
    pUserCfg->stVENCParams.dstSize          = VI_CAPTURE_WIDTH;
    pUserCfg->stVENCParams.dstPixFmt        = DEMO_MM_PIXEL_FORMAT; //NV21

    pUserCfg->stVENCParams.mField           = VIDEO_FIELD_FRAME;
    pUserCfg->stVENCParams.mVideoBitRate    = g_bitrate * 1024 * 1024;
    pUserCfg->stVENCParams.mVideoFrameRate  = 25;
    pUserCfg->stVENCParams.maxKeyFrame      = 25;

    pUserCfg->stVENCParams.mEncUseProfile   = 2; // profile 0-low 1-mid 2-high
    pUserCfg->stVENCParams.mRcMode          = 0;
    pUserCfg->stVENCParams.rotate           = ROTATE_NONE;	//ROTATE_NONE;
    pUserCfg->stVENCParams.VeDev= 0;
    pUserCfg->stVENCParams.VeChnH26x = 0;
    pUserCfg->stVENCParams.VeChnMux = 1;
	

    pUserCfg->stVOParams.mDisplayX = 0;
    pUserCfg->stVOParams.mDisplayY = 0;
    pUserCfg->stVOParams.mDisplayWidth = DISPLAY_WITDH;
    pUserCfg->stVOParams.mDisplayHeight = DISPLAY_HEIGHT;
    memset(pUserCfg->stVOParams.pStrDispType, 0, sizeof(pUserCfg->stVOParams.pStrDispType));
    strcpy(pUserCfg->stVOParams.pStrDispType, "lcd");

    //Init Mux Params
    sprintf(pUserCfg->stMUXParams.szOutputFile, "./nna_%d.mp4", count);

	//Init AO Params
	// TODO
	// audio params of AO_Proc thread used.
	// NEED to follow the using wav file.
	memset(&pUserCfg->stAOParams, 0, sizeof(AO_Params));
	sprintf(pUserCfg->stAOParams.mConfigPara.mPcmFilePath, "./WARN.WAV");
	pUserCfg->stAOParams.mConfigPara.mSampleRate	= 22050;
	pUserCfg->stAOParams.mConfigPara.mChannelCnt	= 2;
	pUserCfg->stAOParams.mConfigPara.mBitWidth		= 16;
	pUserCfg->stAOParams.mConfigPara.mFrameSize		= 1024;

    // Config UserCfg
    pUserCfg->stThreadData[PROC_NNA].ProcessFunc   = NNA_Proc;
    pUserCfg->stThreadData[PROC_NNA].mThreadID     = 0;
    pUserCfg->stThreadData[PROC_NNA].pPrivateData  = NULL;
	
	if(insi_Config.cap_pic_recg == 0) {
		if(g_VIPP_map[VIPP_0].use 		  != 0) {
	    	pUserCfg->stThreadData[PROC_VENC].ProcessFunc  = VENC_Proc;
		}else {
			pUserCfg->stThreadData[PROC_VENC].ProcessFunc  = NULL;
		}
	    pUserCfg->stThreadData[PROC_VENC].mThreadID    = 0;
	    pUserCfg->stThreadData[PROC_VENC].pPrivateData = NULL;

		if(g_VIPP_map[VIPP_2].use 		  != 0) {
			#ifdef RUN_IN_A_CHIP
			pUserCfg->stThreadData[PROC_LCD].ProcessFunc  = NULL;
			g_VIPP_map[VIPP_2].use 		  = 0;
			#else
	    	pUserCfg->stThreadData[PROC_LCD].ProcessFunc  = LCD_Proc;
			#endif
		}else {
	    	pUserCfg->stThreadData[PROC_LCD].ProcessFunc  = NULL;
		}
	    pUserCfg->stThreadData[PROC_LCD].mThreadID    = 0;
	    pUserCfg->stThreadData[PROC_LCD].pPrivateData = NULL;

		#ifdef RUN_IN_A_CHIP
		pUserCfg->stThreadData[PROC_AO].ProcessFunc  = NULL;
		#else
		pUserCfg->stThreadData[PROC_AO].ProcessFunc  = AO_Proc;
		#endif
	    pUserCfg->stThreadData[PROC_AO].mThreadID    = 0;
	    pUserCfg->stThreadData[PROC_AO].pPrivateData = NULL;
	}	
    return 0;
}

void SAMPLE_Usage(char *sPrgNm)
{
    printf("Usage : %s <Detect type> <Bitrate> <Codec> <Resolution> <Framerate>\n", sPrgNm);

    printf("Detect type:\n");
    //printf("\t 0) run human detect \n");
    printf("\t 1) run face detect \n");

    printf("Bitrate:\n");
    printf("\t 10) 1.0M \n");
    printf("\t 15) 1.5M \n");
    printf("\t 20) 2.0M \n");
    printf("\t 25) 2.5M \n");
    printf("\t 30) 3.0M \n");
    printf("\t 35) 3.5M \n");
    printf("\t 40) 4.0M \n");

    printf("Codec:\n");
    printf("\t 0) H264 \n");
    printf("\t 1) H265 \n");

    printf("Resolution:\n");
    printf("\t 0) SD 640*480 \n");
    printf("\t 1) HD 1280*720 \n");
    printf("\t 2) FHD 1920*1080 \n");

    printf("Framerate:\n");
    printf("\t 20) 20 \n");
    printf("\t 25) 25 \n");
    printf("\t 30) 30 \n");
    return;
}

float confirmBitrate(int bitrateType)
{
    float type = 1;
    switch (bitrateType) {
    case 10:
        type = 1;
        break;
    case 15:
        type = 1.5;
        break;
    case 20:
        type = 2;
        break;
    case 25:
        type = 2.5;
        break;
    case 30:
        type = 3;
        break;
    case 35:
        type = 3.5;
        break;
    case 40:
        type = 4;
        break;

    default:
        type = -1;
        break;
    }

    return type;
}

void get_sample_version()
{
	version.main_v = 1;
	version.sample_v = 9;	//1;
	version.nna_v = 6;

	version.nna_m = 2;
	version.nna_fir = 1;
	version.nna_sec = 0;
	version.nna_thr = 2;
}

int main(int argc,char**argv)
{
	int temp;
	if(loadSampleInsiConfig(&insi_Config, SAMPLE_INSI_CONF_PATH) != SUCCESS) {
		printf("loadSample InsiConfig failure\n");
		return -1;
	}

    temp = insi_Config.mode_select;
	if ((temp >= 1) && (temp < NNA_MODE_MAX)) {
        g_nnaDetectMode = NNA_MODE(temp);
		net_select = g_nnaDetectMode;
        printf("NNA Mode : %d\n", g_nnaDetectMode);
    } else {
        SAMPLE_Usage(argv[0]);
        return -1;
    }

    temp = insi_Config.bitrate;
    if (confirmBitrate(temp) < 0.0) {
        SAMPLE_Usage(argv[0]);
        return -1;
    }

    temp = insi_Config.en_265;
    if (temp == 0) {
        g_codecFormat = CODEC_H264;
        printf("Codec H264 \n");
    } else if (temp == 1) {
        g_codecFormat = CODEC_H265;
        printf("Codec H265 \n");
    } else {
        SAMPLE_Usage(argv[0]);
        return -1;
    }

    temp = insi_Config.resolution;
    switch (temp) {
    case 0:
        printf("SD 640*480\n");
        g_resolutionType = RESOLUTION_SD;
        break;
    case 1:
        printf("HD 1280*720\n");
        g_resolutionType = RESOLUTION_HD;
        break;
    case 2:
        printf("FHD 1920*1080\n");
        g_resolutionType = RESOLUTION_FHD;
        break;
    default:
        SAMPLE_Usage(argv[0]);
        return -1;
    }
    switch(g_resolutionType) {
    case RESOLUTION_SD:
        VI_CAPTURE_WIDTH        = (800);
        VI_CAPTURE_HEIGHT        = (480);
        NNA_CALC_WIDTH          = (800);
        NNA_CALC_HEIGHT          = (480);
        break;
    case RESOLUTION_HD:
        VI_CAPTURE_WIDTH        = (1280);
        VI_CAPTURE_HEIGHT        = (720);
        NNA_CALC_WIDTH          = (1280);
        NNA_CALC_HEIGHT          = (720);
        break;
    case RESOLUTION_FHD:
        VI_CAPTURE_WIDTH        = (1920);
        VI_CAPTURE_HEIGHT        = (1080);
        NNA_CALC_WIDTH          = (1920);
        NNA_CALC_HEIGHT          = (1080);
    default:
        break;
    }

    temp = insi_Config.frame_rate;
    if ((temp != 20) && (temp != 25) && (temp != 30)) {
        SAMPLE_Usage(argv[0]);
        return -1;
    } else {
        g_frameRate = temp;
        printf("Framerate %d\n",g_frameRate);
    }

    char version[64] = {0};
    nna_get_version(version);
    printf("%s\n\n", version);
	get_sample_version();

    GLogConfig stGLogConfig = {
        .FLAGS_logtostderr = 1,
        .FLAGS_colorlogtostderr = 1,
        .FLAGS_stderrthreshold = _GLOG_ERROR,
        .FLAGS_minloglevel = _GLOG_ERROR,
        .FLAGS_logbuflevel = -1,
        .FLAGS_logbufsecs = 0,
        .FLAGS_max_log_size = 1,
        .FLAGS_stop_logging_if_full_disk = 1,
    };
    //log_init(argv[0], &stGLogConfig);

#if DEMO_PIC_DATA_FORMAT == DEMO_PIC_DATA_FORMAT_NV21
    printf("DEMO_PIC_DATA_FORMAT_NV21\r\n");
#else
    printf("DEMO_PIC_DATA_FORMAT_YU12\r\n");
#endif
    mq_unlink("/testmQueue");
    struct mq_attr mqSetAttr;
    mqSetAttr.mq_msgsize = 1024;
    mqSetAttr.mq_maxmsg = 10;
    g_mqId = mq_open("/testmQueue", O_RDWR | O_CREAT, 0644, &mqSetAttr);
    if (g_mqId < 0) {
        printf("open message queue error...%s\n", strerror(errno));
        return -1;
    } else {
        struct mq_attr mqAttr;
        if (mq_getattr(g_mqId, &mqAttr) < 0) {
            printf("get the message queue attribute error\n");
            return -1;
        }
        printf("mq_flags:%ld\n", mqAttr.mq_flags);
        printf("mq_maxmsg:%ld\n", mqAttr.mq_maxmsg);
        printf("mq_msgsize:%ld\n", mqAttr.mq_msgsize);
        printf("mq_curmsgs:%ld\n", mqAttr.mq_curmsgs);
    }

    pthread_mutex_init(&g_stResult_lock, 0);
    pthread_mutex_init(&g_mq_lock, 0);

    int ret = 0;
    //SLNNA_Info nnaInfo = {0};
    ret = SL_AllocNNA(&g_nnaInfo);
    if (0 > ret) {
        printf("SL_AllocNNA failed.\n");
        goto NNA_EXIT;
    }
    printf("NNA MEM ALLOC SUCCESS!!\n\n");

    g_nnaInfo.nna_clk = NNA_CLK_400M;
    ret = SL_InitNNA(&g_nnaInfo);
    if (0 > ret) {
        printf("SL_InitNNA failed.\n");
        goto NNA_EXIT;
    }
    printf("SL_InitNNA SUCCESS!!\n\n");

    g_nnaDetectResult = (OSD_XYBOX_T *)malloc(sizeof(OSD_XYBOX_T));
    memset(g_nnaDetectResult, 0, sizeof(OSD_XYBOX_T));

    /*pthread_t cmd_thread_id;
    ret = pthread_create(&cmd_thread_id ,NULL ,CMD_Proc, NULL);
    if (ret) {
        printf("CMD_Proc Thread exit! ret[%d]",ret);
        abort();
    }*/

    pthread_t msg_thread_id;
    ret = pthread_create(&msg_thread_id ,NULL ,MSG_Proc, NULL);
    if (ret) {
        printf("MSG_Proc Thread exit! ret[%d]",ret);
        abort();
    }
	if(insi_Config.cap_pic_recg == 0) {
    	mpp_init();
	}	

    UserConfig stUserCfg;
    memset(&stUserCfg, 0, sizeof(UserConfig));
	
	if(insi_Config.cap_pic_recg == 0) {
		#ifndef RUN_IN_A_CHIP
		audioconifg_init(&stUserCfg);
		create_ai(&stUserCfg.stAUDIOParams);
	    create_aenc(&stUserCfg.stAUDIOParams);
	    bind_audio(&stUserCfg.stAUDIOParams);
		#endif
	}	
    while(1) {
        if (g_codecToggle || g_resolutionToggle) {
            g_codecToggle = 0;
            g_resolutionToggle = 0;			
			pthread_t rtsp_thread_id;
			if(insi_Config.support_rtsp == 1) {
	            ret = pthread_create(&rtsp_thread_id ,NULL ,RTSP_Proc, NULL);
	            if (ret) {
	                printf("RTSP_Proc Thread exit! ret[%d]\n",ret);
	                abort();
	            }
	            printf("SUPPORT_RTSP ALLOC SUCCESS!!\n\n");
			}

            videoconfig_init(&stUserCfg);

			if(insi_Config.cap_pic_recg == 0) {
				create_vi(stUserCfg.stVirViParams);
				#ifndef RUN_IN_A_CHIP
	            create_vo(&stUserCfg.stVOParams);
				#endif
			}			
            //PrepareTestFrame4VO(&stUserCfg.stVOParams, &stUserCfg.stVOParams.mVoFrame);
            //SendFrame2VO(&stUserCfg.stVOParams, &stUserCfg.stVOParams.mVoFrame, 60);
#ifdef ENABLE_ISE
            create_ise(stUserCfg.stVirViParams, &stUserCfg.stISEParams);
            start_vi_ise(stUserCfg.stVirViParams, &stUserCfg.stISEParams);
#endif

            for (int ProcIndex = 0; ProcIndex < PROC_MAX; ProcIndex++) {
                // 启动数据采集线程
                if( ProcIndex==PROC_NNA ) {
                    pthread_attr_t attr;
                    if ((ret = pthread_attr_init(&attr)) != 0) {
                        perror("\npthread fail to attr init!");
                        return ret;
                    }
                    pthread_attr_setstacksize(&attr, 1 * 1024 * 1024);
					
					//pthread_attr_setschedpolicy(&attr, SCHED_RR);					
					//struct sched_param param;									
					//pthread_attr_getschedparam(&attr, &param);
					//printf("getschedparam %d\n", param.sched_priority);
					//param.sched_priority = 51; //�������ȼ�
					//pthread_attr_setschedparam(&attr, &param);	
					
                    ret = pthread_create(&stUserCfg.stThreadData[ProcIndex].mThreadID
                                         ,&attr
                                         ,stUserCfg.stThreadData[ProcIndex].ProcessFunc
                                         ,(void*)&stUserCfg
                                        );
                } else {
                	if(stUserCfg.stThreadData[ProcIndex].ProcessFunc != NULL) {
	                    ret = pthread_create(&stUserCfg.stThreadData[ProcIndex].mThreadID
	                                         ,NULL
	                                         ,stUserCfg.stThreadData[ProcIndex].ProcessFunc
	                                         ,(void*)&stUserCfg
	                                        );
                	}
                }
                if (ret) {
                    printf("chn 0 Save YUV Thread exit! ret[%d]",ret);
                    abort();
                }
            }
            // 等待线程结束
            for (int ProcIndex = 0; ProcIndex < PROC_MAX; ProcIndex++) {
				if(stUserCfg.stThreadData[ProcIndex].ProcessFunc != NULL) {
	                pthread_join(stUserCfg.stThreadData[ProcIndex].mThreadID, NULL);
	                printf("Thread %d Exit!\n", ProcIndex);
				}
            }

			if(insi_Config.support_rtsp == 1) {
            	pthread_join(rtsp_thread_id, NULL);
				printf("Thread rtsp_thread_id Exit!\n");
			}

#ifdef ENABLE_ISE
            stop_vi_ise(stUserCfg.stVirViParams, &stUserCfg.stISEParams);
            destroy_ise(&stUserCfg.stISEParams);
#endif
			if(insi_Config.cap_pic_recg == 0) {
	            destroy_vi(stUserCfg.stVirViParams);
				#ifndef RUN_IN_A_CHIP
	            destroy_vo(&stUserCfg.stVOParams);
	            stop_ai_aenc(&stUserCfg.stAUDIOParams);
				#endif
			}
        }
        if (g_stop) {
			if(insi_Config.cap_pic_recg == 0) {
				#ifndef RUN_IN_A_CHIP
	            //stop_ai_aenc(&stUserCfg.stAUDIOParams);
	            destroy_aenc(&stUserCfg.stAUDIOParams);
	            destroy_ai(&stUserCfg.stAUDIOParams);
				#endif
			}
            break;
        }
    }

    // 销毁MPP
    //pthread_join(cmd_thread_id, NULL);
    pthread_join(msg_thread_id, NULL);
	printf("msg_thread_id exit\n");
	
    mpp_destroy();
    pthread_mutex_destroy(&g_stResult_lock);
    pthread_mutex_destroy(&g_mq_lock);
    mq_unlink("/testmQueue");
NNA_EXIT:
    SL_CloseNNA(&g_nnaInfo);
    SL_FreeNNA(&g_nnaInfo);

    free(g_nnaDetectResult);
    log_quit();
    printf("sample_mtv2 exit!\n");
    return 0;
}

