#include "ETList.h"

ETList::ETList(unsigned int blocksize)
{
    if( blocksize == 0 )
        blocksize = 10;
    m_blocksize = blocksize;
    m_head        = NULL;
    m_tail        = NULL;
    m_free        = NULL;
    m_block        = NULL;
    m_count        = 0;
    m_autodel    = true;
}

ETList::~ETList(void)
{
    RemoveAll();
}

void ETList::OnDataFree(void* data)
{
    if( m_autodel )
        free(data);
}

int ETList::AllocFreeNode(void)
{
    PLISTBLOCK    block;
    PLISTNODE    node;
    unsigned int    idx;
    if( m_free != NULL )
        return 0;

    block = (LISTBLOCK*)malloc(sizeof(LISTBLOCK) + m_blocksize* sizeof(LISTNODE) );
    assert(block!=NULL);

    node = (PLISTNODE)(block+1);
    node+= (m_blocksize - 1);
    for( idx = 0; idx < m_blocksize; idx++) {
        node->next    = m_free;
        node->prev    = NULL;
        node->data    = NULL;
        m_free        = node;
        node--;
    }

    block->next    = m_block;
    m_block        = block;
    return 0;
}


void ETList::RemoveAll(void)
{
    PLISTNODE    node;
    PLISTBLOCK    block;
    for( node = m_head; node!=NULL; node = node->next)
        OnDataFree(node->data);
    m_count = 0;
    m_head    = NULL;
    m_tail    = NULL;
    m_free    = NULL;
    while( m_block != NULL ) {
        block    = m_block;
        m_block    = m_block->next;
        free(block);
    }
}

ETList::PLISTNODE ETList::NewNode(PLISTNODE prev, PLISTNODE next)
{
    PLISTNODE node = NULL;
    AllocFreeNode();
    assert(m_free!=NULL);
    node    = m_free;
    m_free    = node->next;
    node->data    = NULL;
    node->prev    = prev;
    node->next    = next;
    m_count++;
    return node;
}

void ETList::FreeNode(PLISTNODE node)
{
    if( node == NULL )
        return;
    OnDataFree(node->data);
    node->next    = m_free;
    m_free        = node;
    m_count--;
    if( m_count == 0 )
        RemoveAll();
}

ETLISTPOS ETList::AddHead(void* data)
{
    PLISTNODE node = NewNode(NULL, m_head);
    node->data = data;
    if( m_head != NULL )
        m_head->prev = node;
    else
        m_tail    = node;
    m_head = node;
    return node;
}

ETLISTPOS ETList::AddTail(void* data)
{
    PLISTNODE node = NewNode(m_tail, NULL);
    node->data = data;
    if( m_tail != NULL )
        m_tail->next = node;
    else
        m_head = node;
    m_tail = node;
    return node;
}

ETLISTPOS ETList::InsertNext(ETLISTPOS pos, void* data)
{
    PLISTNODE prev = (PLISTNODE)pos;
    PLISTNODE node = NULL;
    if( prev == NULL )
        return AddTail(data);
    node = NewNode( prev, prev->next);
    node->data = data;
    if( prev->next != NULL )
        prev->next->prev = node;
    else
        m_tail = node;
    prev->next = node;
    return node;
}

ETLISTPOS ETList::InsertPrev(ETLISTPOS pos, void* data)
{
    PLISTNODE next = (PLISTNODE)pos;
    PLISTNODE node = NULL;
    if( next == NULL )
        return AddHead(data);
    node = NewNode( next->prev, next);
    node->data = data;

    if( next->prev != NULL )
        next->prev->next = node;
    else
        m_head = node;
    next->prev = node;
    return node;
}

void* ETList::RemoveHead(void)
{
    void*        data;
    PLISTNODE    node = m_head;
    if( m_head == NULL )
        return NULL;
    data = node->data;
    node->data = NULL;
    m_head = node->next;
    if( m_head != NULL )
        m_head->prev = NULL;
    else
        m_tail = NULL;

    FreeNode(node);
    return data;
}

void* ETList::RemoveTail(void)
{
    void*        data;
    PLISTNODE    node = m_tail;
    if( m_tail == NULL )
        return NULL;
    data = node->data;
    node->data = NULL;
    m_tail = node->prev;
    if( m_tail != NULL )
        m_tail->next = NULL;
    else
        m_head = NULL;
    FreeNode(node);
    return data;
}

void* ETList::RemoveAt(ETLISTPOS pos)
{
    void*        data = NULL;
    PLISTNODE    node = (PLISTNODE)pos;
    if( node == NULL )
        return NULL;
    data = node->data;
    node->data = NULL;
    if( node == m_head )
        m_head = node->next;
    else
        node->prev->next = node->next;
    if( node == m_tail )
        m_tail = node->prev;
    else
        node->next->prev = node->prev;
    FreeNode(node);
    return data;
}

unsigned int ETList::RemoveFromTailNum(unsigned int num)
{
    unsigned int removeNum = 0;
    for(removeNum = 0; removeNum<num; removeNum++) {
        PLISTNODE    node = m_tail;
        if( m_tail == NULL )
            return removeNum;
        m_tail = node->prev;
        if( m_tail != NULL )
            m_tail->next = NULL;
        else
            m_head = NULL;
        FreeNode(node);
    }
    return removeNum;
}

ETLISTPOS ETList::Find(void* data)
{
    PLISTNODE node = m_head;
    while( node != NULL ) {
        if( node->data == data )
            return node;
        node = node->next;
    }
    return NULL;
}


int ETList::GetIDX(void* data) const
{
    int            idx  = -1;
    PLISTNODE     node = m_head;
    while( node != NULL ) {
        idx++;
        if( node->data == data )
            return idx;
        node = node->next;
    }
    return -1;
}

