/*
 * Allwinner SoCs display driver.
 *
 * Copyright (C) 2016 Allwinner.
 *
 * This file is licensed under the terms of the GNU General Public
 * License version 2.  This program is licensed "as is" without any
 * warranty of any kind, whether express or implied.
 */

/*
lcd_pins_a: rgb@0 {
	allwinner,pins ="PD1", "PD2", "PD3", "PD4",
	"PD5", "PD6", "PD7", "PD8",
	"PD18", "PD19",
	"PD20", "PD21";
	allwinner,pname = "lcdd3", "lcdd4", "lcdd5", "lcdd6",
	"lcdd7", "lcdd10", "lcdd11", "lcdd12",
	"lcdclk", "lcd_de",
	"lcdhs", "lcdvs";
	allwinner,function = "lcd_rgb";
	allwinner,muxsel = <2>;
	allwinner,drive = <1>;
	allwinner,pull = <0>;
};

lcd_pins_b: rgb@1 {
	allwinner,pins = "PD1", "PD2", "PD3", "PD4",
	"PD5", "PD6", "PD7", "PD8",
	"PD18", "PD19",
	"PD20", "PD21";
	allwinner,function = "io_disabled";
	allwinner,muxsel = <7>;
	allwinner,drive = <1>;
	allwinner,pull = <0>;
};
lcd0: lcd0@01c0c000 {
	lcd_used            = <1>;

	lcd_driver_name     = "lcd_ht280l0x2";

	lcd_backlight       = <150>;

	lcd_if              = <1>;
	lcd_x               = <240>;
	lcd_y               = <240>;
	lcd_width           = <36>;
	lcd_height          = <65>;
	lcd_dclk_freq       = <20>;

	lcd_pwm_used        = <1>;
	lcd_pwm_ch          = <9>;
	lcd_pwm_freq        = <50000>;
	lcd_pwm_pol         = <1>;
	lcd_pwm_max_limit   = <255>;


	lcd_hbp             = <22>;
	lcd_ht              = <272>;
	lcd_hspw            = <2>;
	lcd_vbp             = <10>;
	lcd_vt              = <260>;
	lcd_vspw            = <2>;

	lcd_dsi_if          = <0>;
	lcd_dsi_lane        = <2>;
	lcd_dsi_format      = <0>;
	lcd_dsi_te          = <0>;
	lcd_dsi_eotp        = <0>;

	lcd_frm             = <0>;
	lcd_io_phase        = <0x0000>;
	lcd_hv_clk_phase    = <0>;
	lcd_hv_sync_polarity= <0>;
	lcd_gamma_en        = <0>;
	lcd_bright_curve_en = <0>;
	lcd_cmap_en         = <0>;

	lcd_cpu_mode        = <1>;
	lcd_cpu_te          = <1>;
	lcd_cpu_if          = <14>;

	lcdgamma4iep        = <22>;

	lcd_power            = "dldo1";
	lcd_pin_power        = "bldo1";
	lcd_pin_power1       = "dcdc1";

	lcd_gpio_0 = <&pio PD 9 1 1 3 1>;
	lcd_gpio_1 = <&pio PD 10 1 1 3 1>;

	pinctrl-0 = <&lcd_pins_a>;
	pinctrl-1 = <&lcd_pins_b>;
};
*/
#include "cpu_ht280l0x2.h"

#define lcd_ht280l0x2_rs(v) sunxi_lcd_gpio_set_value(0, 0, v)
#define lcd_ht280l0x2_cs(v) sunxi_lcd_gpio_set_value(0, 1, v)

#define lcd_cpu_wr_index_16b sunxi_lcd_cpu_write_index
#define lcd_cpu_wr_data_16b sunxi_lcd_cpu_write_data

static void lcd_power_on(u32 sel);
static void lcd_power_off(u32 sel);
static void lcd_bl_open(u32 sel);
static void lcd_bl_close(u32 sel);

static void lcd_panel_init(u32 sel);
static void lcd_panel_exit(u32 sel);

static void lcd_ht280l0x2_init(__u32 sel);
static void lcd_cfg_panel_info(struct panel_extend_para *info)
{
	u32 i = 0, j = 0;
	u32 items;
	u8 lcd_gamma_tbl[][2] = {
		/*{input value, corrected value} */
		{0, 0},
		{15, 15},
		{30, 30},
		{45, 45},
		{60, 60},
		{75, 75},
		{90, 90},
		{105, 105},
		{120, 120},
		{135, 135},
		{150, 150},
		{165, 165},
		{180, 180},
		{195, 195},
		{210, 210},
		{225, 225},
		{240, 240},
		{255, 255},
	};

	u32 lcd_cmap_tbl[2][3][4] = {
		{
		 {LCD_CMAP_G0, LCD_CMAP_B1, LCD_CMAP_G2, LCD_CMAP_B3},
		 {LCD_CMAP_B0, LCD_CMAP_R1, LCD_CMAP_B2, LCD_CMAP_R3},
		 {LCD_CMAP_R0, LCD_CMAP_G1, LCD_CMAP_R2, LCD_CMAP_G3},
		 },
		{
		 {LCD_CMAP_B3, LCD_CMAP_G2, LCD_CMAP_B1, LCD_CMAP_G0},
		 {LCD_CMAP_R3, LCD_CMAP_B2, LCD_CMAP_R1, LCD_CMAP_B0},
		 {LCD_CMAP_G3, LCD_CMAP_R2, LCD_CMAP_G1, LCD_CMAP_R0},
		 },
	};

	items = sizeof(lcd_gamma_tbl) / 2;
	for (i = 0; i < items - 1; i++) {
		u32 num = lcd_gamma_tbl[i + 1][0] - lcd_gamma_tbl[i][0];

		for (j = 0; j < num; j++) {
			u32 value = 0;

			value = lcd_gamma_tbl[i][1] + ((lcd_gamma_tbl[i + 1][1]
							-
							lcd_gamma_tbl[i][1]) *
						       j) / num;
			info->lcd_gamma_tbl[lcd_gamma_tbl[i][0] + j] =
			    (value << 16) + (value << 8) + value;
		}
	}
	info->lcd_gamma_tbl[255] = (lcd_gamma_tbl[items - 1][1] << 16)
	    + (lcd_gamma_tbl[items - 1][1] << 8) + lcd_gamma_tbl[items - 1][1];

	memcpy(info->lcd_cmap_tbl, lcd_cmap_tbl, sizeof(lcd_cmap_tbl));

}

static s32 lcd_open_flow(u32 sel)
{
	/* open lcd power, and delay 50ms */
	LCD_OPEN_FUNC(sel, lcd_power_on, 30);
	/* open lcd power, than delay 200ms */
	LCD_OPEN_FUNC(sel, lcd_panel_init, 200);
	/* open lcd controller, and delay 100ms */
	LCD_OPEN_FUNC(sel, sunxi_lcd_tcon_enable, 100);
	/* open lcd backlight, and delay 0ms */
	LCD_OPEN_FUNC(sel, lcd_bl_open, 0);

	return 0;
}

static s32 lcd_close_flow(u32 sel)
{
	/* close lcd backlight, and delay 0ms */
	LCD_CLOSE_FUNC(sel, lcd_bl_close, 0);
	/* close lcd controller, and delay 0ms */
	LCD_CLOSE_FUNC(sel, sunxi_lcd_tcon_disable, 0);
	/* open lcd power, than delay 200ms */
	LCD_CLOSE_FUNC(sel, lcd_panel_exit, 200);
	/* close lcd power, and delay 500ms */
	LCD_CLOSE_FUNC(sel, lcd_power_off, 500);

	return 0;
}

static void lcd_power_on(u32 sel)
{
	/* enable lcd power0 */
	sunxi_lcd_power_enable(sel, 0);
	/* enable lcd pin output */
	sunxi_lcd_pin_cfg(sel, 1);
}

static void lcd_power_off(u32 sel)
{
	/* disable lcd pin output */
	sunxi_lcd_pin_cfg(sel, 0);
	/* disable lcd power0 */
	sunxi_lcd_power_disable(sel, 0);
}

static void lcd_bl_open(u32 sel)
{
	sunxi_lcd_pwm_enable(sel);
	/* config lcd_bl_en pin to open lcd backlight */
	sunxi_lcd_backlight_enable(sel);
}

static void lcd_bl_close(u32 sel)
{
	/* config lcd_bl_en pin to close lcd backlight */
	sunxi_lcd_backlight_disable(sel);
	sunxi_lcd_pwm_disable(sel);
}

static void lcd_panel_init(u32 sel)
{
	lcd_ht280l0x2_init(sel);
}

static void lcd_panel_exit(u32 sel)
{
}

static s32 lcd_user_defined_func(u32 sel, u32 para1, u32 para2, u32 para3)
{
	return 0;
}

void lcd_dbi_wr_dcs(__u32 sel, __u8 cmd, __u8 *para, __u32 para_num)
{
	__u8 index = cmd;
	__u8 *data_p = para;
	__u16 i;

	lcd_cpu_wr_index_16b(sel, index);
	for (i = 0; i < para_num; i++)
		lcd_cpu_wr_data_16b(sel, *(data_p++));
}

void lcd_cpu_panel_fr(__u32 sel, __u32 w, __u32 h, __u32 x, __u32 y)
{
	__u8 para[4];
	__u32 para_num;

	para[0] = (x >> 8) & 0xff;
	para[1] = (x >> 0) & 0xff;
	para[2] = ((x + w - 1) >> 8) & 0xff;
	para[3] = ((x + w - 1) >> 0) & 0xff;
	para_num = 4;
	lcd_dbi_wr_dcs(sel, DSI_DCS_SET_COLUMN_ADDRESS, para, para_num);

	para[0] = (y >> 8) & 0xff;
	para[1] = (y >> 0) & 0xff;
	para[2] = ((y + h - 1) >> 8) & 0xff;
	para[3] = ((y + h - 1) >> 0) & 0xff;
	para_num = 4;
	lcd_dbi_wr_dcs(sel, DSI_DCS_SET_PAGE_ADDRESS, para, para_num);

	para_num = 0;
	lcd_dbi_wr_dcs(sel, DSI_DCS_WRITE_MEMORY_START, para, para_num);
}

static void lcd_ht280l0x2_init(__u32 sel)
{
	struct disp_panel_para *panel_info =
	    kmalloc(sizeof(struct disp_panel_para),
		    GFP_KERNEL | __GFP_ZERO);

	if (panel_info == NULL) {
		__wrn("kmalloc for panel_info(%zu bytes) fail!",
		      sizeof(struct disp_panel_para));
		return;
	}

	bsp_disp_get_panel_info(sel, panel_info);

	printk("--------->LCD init \n");
	lcd_ht280l0x2_cs(0);
	lcd_ht280l0x2_rs(1);
	sunxi_lcd_delay_ms(120);
	lcd_ht280l0x2_rs(0);
	sunxi_lcd_delay_ms(50);
	lcd_ht280l0x2_rs(1);

	sunxi_lcd_delay_ms(100);
	lcd_cpu_wr_index_16b(sel, 0xCF);
	lcd_cpu_wr_data_16b(sel, 0x00);
	lcd_cpu_wr_data_16b(sel, 0xD9);
	lcd_cpu_wr_data_16b(sel, 0X30);

	lcd_cpu_wr_index_16b(sel, 0xED);
	lcd_cpu_wr_data_16b(sel, 0x64);
	lcd_cpu_wr_data_16b(sel, 0x03);
	lcd_cpu_wr_data_16b(sel, 0X12);
	lcd_cpu_wr_data_16b(sel, 0X81);

	lcd_cpu_wr_index_16b(sel, 0xE8);
	lcd_cpu_wr_data_16b(sel, 0x85);
	lcd_cpu_wr_data_16b(sel, 0x10);
	lcd_cpu_wr_data_16b(sel, 0x78);

	lcd_cpu_wr_index_16b(sel, 0xCB);
	lcd_cpu_wr_data_16b(sel, 0x39);
	lcd_cpu_wr_data_16b(sel, 0x2C);
	lcd_cpu_wr_data_16b(sel, 0x00);
	lcd_cpu_wr_data_16b(sel, 0x34);
	lcd_cpu_wr_data_16b(sel, 0x02);
	 
	lcd_cpu_wr_index_16b(sel, 0xF7);  
	lcd_cpu_wr_data_16b(sel, 0x20); 
	 
	lcd_cpu_wr_index_16b(sel, 0xEA);  
	lcd_cpu_wr_data_16b(sel, 0x00); 
	lcd_cpu_wr_data_16b(sel, 0x00); 
	 
	lcd_cpu_wr_index_16b(sel, 0xC0);    //Power control 
	lcd_cpu_wr_data_16b(sel, 0x21);   //VRH[5:0] 
	 
	lcd_cpu_wr_index_16b(sel, 0xC1);    //Power control 
	lcd_cpu_wr_data_16b(sel, 0x12);   //SAP[2:0];BT[3:0] 
	 
	lcd_cpu_wr_index_16b(sel, 0xC5);    //VCM control 
	lcd_cpu_wr_data_16b(sel, 0x32); 
	lcd_cpu_wr_data_16b(sel, 0x3C); 
	 
	lcd_cpu_wr_index_16b(sel, 0xC7);    //VCM control2 
	lcd_cpu_wr_data_16b(sel, 0XC1); 
	 
	lcd_cpu_wr_index_16b(sel, 0x36);    // Memory Access Control 
	lcd_cpu_wr_data_16b(sel, 0x08); 
	 
	lcd_cpu_wr_index_16b(sel, 0x3A);   
	lcd_cpu_wr_data_16b(sel, 0x55); 
	
	lcd_cpu_wr_index_16b(sel, 0xB1);   
	lcd_cpu_wr_data_16b(sel, 0x00);   
	lcd_cpu_wr_data_16b(sel, 0x15); 
	 
	lcd_cpu_wr_index_16b(sel, 0xB6);    // Display Function Control 
	lcd_cpu_wr_data_16b(sel, 0x0A); 
	lcd_cpu_wr_data_16b(sel, 0xA2); 
	
	 
	 
	lcd_cpu_wr_index_16b(sel, 0xF2);    // 3Gamma Function Disable 
	lcd_cpu_wr_data_16b(sel, 0x00); 
	 
	lcd_cpu_wr_index_16b(sel, 0x26);    //Gamma curve selected 
	lcd_cpu_wr_data_16b(sel, 0x01); 
	 
	lcd_cpu_wr_index_16b(sel, 0xE0);    //Set Gamma 
	lcd_cpu_wr_data_16b(sel, 0x0F); 
	lcd_cpu_wr_data_16b(sel, 0x20); 
	lcd_cpu_wr_data_16b(sel, 0x1E); 
	lcd_cpu_wr_data_16b(sel, 0x09); 
	lcd_cpu_wr_data_16b(sel, 0x12); 
	lcd_cpu_wr_data_16b(sel, 0x0B); 
	lcd_cpu_wr_data_16b(sel, 0x50); 
	lcd_cpu_wr_data_16b(sel, 0XBA); 
	lcd_cpu_wr_data_16b(sel, 0x44); 
	lcd_cpu_wr_data_16b(sel, 0x09); 
	lcd_cpu_wr_data_16b(sel, 0x14); 
	lcd_cpu_wr_data_16b(sel, 0x05); 
	lcd_cpu_wr_data_16b(sel, 0x23); 
	lcd_cpu_wr_data_16b(sel, 0x21); 
	lcd_cpu_wr_data_16b(sel, 0x00); 
	 
	lcd_cpu_wr_index_16b(sel, 0XE1);    //Set Gamma 
	lcd_cpu_wr_data_16b(sel, 0x00); 
	lcd_cpu_wr_data_16b(sel, 0x19); 
	lcd_cpu_wr_data_16b(sel, 0x19); 
	lcd_cpu_wr_data_16b(sel, 0x00); 
	lcd_cpu_wr_data_16b(sel, 0x12); 
	lcd_cpu_wr_data_16b(sel, 0x07); 
	lcd_cpu_wr_data_16b(sel, 0x2D); 
	lcd_cpu_wr_data_16b(sel, 0x28); 
	lcd_cpu_wr_data_16b(sel, 0x3F); 
	lcd_cpu_wr_data_16b(sel, 0x02); 
	lcd_cpu_wr_data_16b(sel, 0x0A); 
	lcd_cpu_wr_data_16b(sel, 0x08); 
	lcd_cpu_wr_data_16b(sel, 0x25); 
	lcd_cpu_wr_data_16b(sel, 0x2D); 
	lcd_cpu_wr_data_16b(sel, 0x0F);

        //enable te
	lcd_cpu_wr_index_16b(sel, 0x35);
	lcd_cpu_wr_data_16b(sel, 0x00);

	lcd_cpu_wr_index_16b(sel, 0x11);    //Exit Sleep 
	sunxi_lcd_delay_ms(120);
	lcd_cpu_wr_index_16b(sel, 0x29);    //Display on 
	sunxi_lcd_delay_ms(1);
	lcd_cpu_wr_index_16b(sel, 0x2c);

	kfree(panel_info);
}

struct __lcd_panel lcd_ht280l0x2_panel = {
	/*
	 * panel driver name, must mach the name
	 * of lcd_drv_name in sys_config.fex
	 */
	.name = "lcd_ht280l0x2",
	.func = {
		 .cfg_panel_info = lcd_cfg_panel_info,
		 .cfg_open_flow = lcd_open_flow,
		 .cfg_close_flow = lcd_close_flow,
		 .lcd_user_defined_func = lcd_user_defined_func,
		 }
	,
};
