#!/bin/sh
echo "---------- v459 wifitest start ----------"

#mount /dev/mmcblk0p1 /mnt/extsd
echo 0x0300b340 > /sys/class/sunxi_dump/dump
echo 0x0300b340 0x00000160 > /sys/class/sunxi_dump/write
mkdir -p /var/run/wpa_supplicant
insmod /lib/modules/4.9.118/bcmdhd.ko 
echo 'ctrl_interface=/var/run/wpa_supplicant' > /var/run/wpa_supplicant/wpa_supplicant.conf
echo 'update_config=1' >> /var/run/wpa_supplicant/wpa_supplicant.conf

sleep 2

wpa_supplicant -Dnl80211 -iwlan0 -c /var/run/wpa_supplicant/wpa_supplicant.conf &

sleep 3

wpa_cli -iwlan0 scan
wpa_cli -iwlan0 scan_results
wpa_cli -iwlan0 add_network

#wpa_cli -iwlan0 set_network 0 ssid \"Seeeverything\"
#wpa_cli -iwlan0 set_network 0 psk \"Xiaoyi2016\"
#wpa_cli -iwlan0 set_network 0 ssid \"EthanLuoWIFI\"
#wpa_cli -iwlan0 set_network 0 psk \"12345678\"

wpa_cli -iwlan0 set_network 0 ssid \"$1\"
wpa_cli -iwlan0 set_network 0 psk \"$2\"
wpa_cli -iwlan0 enable_network 0

sleep 3

wpa_cli -iwlan0 save_config
udhcpc -i wlan0	&


echo "---------- v459 wifitest end ----------"
