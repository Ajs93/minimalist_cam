#!/bin/bash

# Clear all relevant environment variables
fw_setenv -l /tmp wpa_supplicant_conf
fw_setenv -l /tmp persistent_config
fw_setenv -l /tmp motion_detector
fw_setenv -l /tmp advertiser
fw_setenv -l /tmp video_config
fw_setenv -l /tmp motor_control
fw_setenv -l /tmp motor_home_position
fw_setenv -l /tmp motor_poi

reboot