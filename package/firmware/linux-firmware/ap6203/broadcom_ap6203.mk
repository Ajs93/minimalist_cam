Package/ap6203-firmware = $(call Package/firmware-default,Broadcom AP6203 firmware)
define Package/ap6203-firmware/install
	$(INSTALL_DIR) $(1)/$(FIRMWARE_PATH)
	$(INSTALL_DATA) \
		$(TOPDIR)/package/firmware/linux-firmware/ap6203/*.bin \
		$(1)/$(FIRMWARE_PATH)/
	$(INSTALL_DATA) \
		$(TOPDIR)/package/firmware/linux-firmware/ap6203/clm_bcm43013c1_ag.bin \
		$(1)/$(FIRMWARE_PATH)/clm_bcm43013c1_ag.blob
	$(INSTALL_DATA) \
		$(TOPDIR)/package/firmware/linux-firmware/ap6203/nvram_ap6203bm.txt \
		$(1)/$(FIRMWARE_PATH)/nvram.txt
endef
$(eval $(call BuildPackage,ap6203-firmware))
