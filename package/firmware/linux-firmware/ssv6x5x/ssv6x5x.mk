Package/ssv6x5x-firmware = $(call Package/firmware-default,SSV6X5X firmware)
define Package/ssv6x5x-firmware/install
	$(INSTALL_DIR) $(1)/$(FIRMWARE_PATH)
	$(INSTALL_DATA) \
		$(TOPDIR)/package/firmware/linux-firmware/ssv6x5x/*.cfg \
		$(1)/$(FIRMWARE_PATH)/
endef
$(eval $(call BuildPackage,ssv6x5x-firmware))
