/*
  * Copyright (c) 2019 Allwinner Technology. All Rights Reserved.
  * Author        laumy <liumingyuan@allwinnertech.com>
  * Version       0.0.1
  *
  * Author:        Kevin
  * Version:       1.0.0
  * Update: first version of definition for Allwinner BLE standard APIs
  */

#ifndef __AW_GATT_SERVER_H
#define __AW_GATT_SERVER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>

typedef enum {
	BT_GATT_CHAR_PROPERTY_BROADCAST = 0x01,
	BT_GATT_CHAR_PROPERTY_READ = 0x02,
	BT_GATT_CHAR_PROPERTY_WRITE_NO_RESPONSE = 0x04,
	BT_GATT_CHAR_PROPERTY_WRITE = 0x08,
	BT_GATT_CHAR_PROPERTY_NOTIFY = 0x10,
	BT_GATT_CHAR_PROPERTY_INDICATE = 0x20,
	BT_GATT_CHAR_PROPERTY_AUTH_SIGNED_WRITE = 0x40
}tGattCharProperties;

typedef enum {
	BT_GATT_DESC_PROPERTY_BROADCAST = 0x01,
	BT_GATT_DESC_PROPERTY_READ = 0x02,
	BT_GATT_DESC_PROPERTY_WRITE_NO_RESPONSE = 0x04,
	BT_GATT_DESC_PROPERTY_WRITE = 0x08,
	BT_GATT_DESC_PROPERTY_NOTIFY = 0x10,
	BT_GATT_DESC_PROPERTY_INDICATE = 0x20,
	BT_GATT_DESC_PROPERTY_AUTH_SIGNED_WRITE = 0x40
}tGattDescProperties;

typedef enum {
	BT_GATT_PERM_READ = 0x01,
	BT_GATT_PERM_WRITE = 0x02,
	BT_GATT_PERM_READ_ENCYPT = 0x04,
	BT_GATT_PERM_WRITE_ENCRYPT = 0x08,
	BT_GATT_PERM_ENCRYPT = 0x04 | 0x08,
	BT_GATT_PERM_READ_AUTHEN = 0x10,
	BT_GATT_PERM_WRITE_AUTHEN = 0x20,
	BT_GATT_PERM_AUTHEN = 0x10 | 0x20,
	BT_GATT_PERM_AUTHOR = 0x40,
	BT_GATT_PERM_NONE = 0x80
}tGattPermissions;

typedef enum {
	BT_GATT_CONNECTION,
	BT_GATT_DISCONNECT,
}tGattConnectionEvent;

typedef struct {
	int svc_handle;
}tGattAddSvcMsg;

typedef struct {
	char *uuid;
	int char_handle;
}tGattAddCharMsg;

typedef struct {
	int desc_handle;
}tGattAddDescMsg;

typedef void (*BtGattAddServiceCb)(tGattAddSvcMsg *pData);
typedef void (*BtGattAddCharCb)(tGattAddCharMsg *pData);
typedef void (*BtGattAddDescCb)(tGattAddDescMsg *pData);
typedef void (*BtGattSeviceReadyCb)(int state);
typedef void (*BtGattConnectionEventCb)(char *addr, tGattConnectionEvent event);

typedef struct {
	int trans_id;
	int attr_handle;
	int offset;
	bool is_blog_req;
}tGattCharReadReq;

#define AG_GATT_MAX_ATTR_LEN 600

typedef struct {
	int trans_id;
	int attr_handle;
	int offset;
	char value[AG_GATT_MAX_ATTR_LEN];
	int value_len;
	bool need_rsp;
}tGattCharWriteReq;

typedef struct {
	bool state;
}tGattNotifyReq;

typedef struct {
	int trans_id;
	int attr_handle;
	int offset;
	bool is_blog_req;
}tGattDescReadReq;

typedef struct {
	int trans_id;
	int attr_handle;
	int offset;
	char value[AG_GATT_MAX_ATTR_LEN];
	int value_len;
	bool need_rsp;
}tGattDescWriteReq;

typedef struct {

}tGattSendIndication;

typedef void (*BtGattCharReadReqCb)(tGattCharReadReq *char_read);
typedef void (*BtGattCharWriteReqCb)(tGattCharWriteReq *char_write);
typedef void (*BtGattCharNotifyReqCb)(tGattDescReadReq *char_notify);

typedef void (*BtGattDescReadReqCb)(tGattDescReadReq *desc_read);
typedef void (*BtGattDescWriteReqCb)(tGattDescWriteReq *desc_write);

typedef void (*BtGattSendIndicationCb)(tGattSendIndication *send_ind);

typedef struct {

/*gatt add ... callback*/
	BtGattAddServiceCb GattAddSvcCb;
	BtGattAddCharCb GattAddCharCb;
	BtGattAddDescCb GattAddDescCb;

/*gatt event callback*/
	BtGattConnectionEventCb GattConnectionEventcb;

	BtGattSeviceReadyCb GattServiceReadyCb;

/*gatt characteristic request callback*/
	BtGattCharReadReqCb GattCharReadReqCb;
	BtGattCharWriteReqCb GattCharWriteReqCb;
	BtGattCharNotifyReqCb GattCharNotifyReqCb;

/*gatt descriptor request callback*/
	BtGattDescReadReqCb GattDescReadReqCb;
	BtGattDescWriteReqCb GattDescWriteReqCb;

	BtGattSendIndicationCb GattSendIndicationCb;
}tGattServerCb;


typedef struct {
	char *uuid;      /*128-bit service UUID*/
	bool primary;    /* If true, this GATT service is a primary service */
	int number;
}tGattAddSvc;

typedef struct {
	char *uuid;      /*128-bit characteristic UUID*/
	int properties;        /*The GATT characteristic properties*/
	int permissions;       /*The GATT characteristic permissions*/
	int svc_handle;          /*the service atr handle*/
}tGattAddChar;

typedef struct {
	char *uuid;     /*128-bit descriptor UUID*/
	int properties;       /*The GATT descriptor properties*/
	int permissions;      /*he GATT descriptor  permissions*/
	int svc_handle;
}tGattAddDesc;

typedef struct {
	int svc_handle;
}tGattStarSvc;

typedef struct {
	int svc_handle;
}tGattStopSvc;

typedef struct {
	int svc_handle;
}tGattDelSvc;

typedef struct {
	int trans_id;
	int status;
	int svc_handle;
	char *value;
	int value_len;
	int auth_req;
}tGattSendReadRsp;

typedef enum {
	BT_GATT_REQ_FAILED,
	BT_GATT_REQ_IN_PROGRESS,
	BT_GATT_REQ_PERMITTED,
	BT_GATT_REQ_INVAILD_VALUE_LENGTH,
	BT_GATT_REQ_NOT_AUTHORIZED,
	BT_GATT_REQ_NOT_SUPPORT
}tGattState;

typedef struct {
	tGattState state;
}tGattWriteRsp;

typedef struct {
	int attr_handle;
	char *value;
	int value_len;
}tGattNotifyRsp;

typedef struct {
	int attr_handle;
	char *value;
	int value_len;
}tGattIndicationRsp;

typedef struct aw_adv_data_t {
	uint8_t data[31];
	uint8_t data_len;
} aw_adv_data_t;

typedef struct aw_rsp_data_t {
	uint8_t data[31];
	uint8_t data_len;
} aw_rsp_data_t;

/* LE address type */
typedef enum {
	AW_LE_PUBLIC_ADDRESS = 0x00,
	AW_LE_RANDOM_ADDRESS = 0x01,
	AW_LE_IRK_OR_PUBLIC_ADDRESS = 0x02,
	AW_LE_IRK_OR_RANDOM_ADDRESS = 0x03,
} aw_le_addr_type_t;

/*LE advertising type*/
typedef enum {
	AW_LE_ADV_IND = 0x00, /*connectable and scannable undirected advertising*/
	AW_LE_ADV_DIRECT_HIGH_IND = 0x01, /*connectable high duty cycle directed advertising*/
	AW_LE_ADV_SCAN_IND = 0x02, /*scannable undirected advertising*/
	AW_LE_ADV_NONCONN_IND = 0x03, /*non connectable undirected advertising*/
	AW_LE_ADV_DIRECT_LOW_IND = 0x04, /*connectable low duty cycle directed advertising*/
} aw_le_advertising_type_t;

/*LE advertising filter policy*/
typedef enum {
	AW_LE_PROCESS_ALL_REQ = 0x00, /*process scan and connection requests from all devices*/
	AW_LE_PROCESS_CONN_REQ = 0x01, /*process connection request from all devices and scan request only from white list*/
	AW_LE_PROCESS_SCAN_REQ = 0x02, /*process scan request from all devices and connection request only from white list*/
	AW_LE_PROCESS_WHITE_LIST_REQ = 0x03, /*process requests only from white list*/
} aw_le_advertising_filter_policy_t;

/*LE peer address type*/
typedef enum {
	AW_LE_PEER_PUBLIC_ADDRESS = 0x00, /*public device address(default) or public indentiy address*/
	AW_LE_PEER_RANDOM_ADDRESS = 0x01, /*random device address(default) or random indentiy address*/
}aw_le_peer_addr_type_t;


#define AW_LE_ADV_CHANNEL_NONE 0x00
#define AW_LE_ADV_CHANNEL_37 0x01
#define AW_LE_ADV_CHANNEL_38 (0x01 << 1)
#define AW_LE_ADV_CHANNEL_39 (0x01 << 2)
#define AW_LE_ADV_CHANNEL_ALL (AW_LE_ADV_CHANNEL_NONE | AW_LE_ADV_CHANNEL_37 | AW_LE_ADV_CHANNEL_38 | AW_LE_ADV_CHANNEL_39)

typedef struct {
	uint16_t	min_interval;
	uint16_t	max_interval;
	aw_le_advertising_type_t	adv_type;
	aw_le_addr_type_t		own_addr_type;
	aw_le_peer_addr_type_t	peer_addr_type;
	char		peer_addr[18];
	uint8_t		chan_map;
	aw_le_advertising_filter_policy_t	filter;
} aw_le_advertising_parameters_t;

int aw_gatt_create_service(tGattAddSvc *svc);

int aw_gatt_add_characteristic(tGattAddChar *chr);

int aw_gatt_add_descriptor(tGattAddDesc *desc);

int aw_gatt_start_service(tGattStarSvc *start_svc);

int aw_gatt_stop_service(tGattStopSvc *stop_svc);

int aw_gatt_delete_service(tGattDelSvc *del_svc);

int aw_gatt_send_read_response(tGattSendReadRsp *pData);

int aw_gatt_send_write_response(tGattWriteRsp *pData);

int aw_gatt_send_notification(tGattNotifyRsp *pData);

int aw_gatt_send_indication(tGattIndicationRsp *pData);

int aw_gatt_server_init(tGattServerCb *cb);

void aw_gatt_server_deinit();

int aw_gatt_server_disconnect(unsigned int handle, unsigned char reason);

int aw_gatt_server_clear_white_list();

int aw_gatt_enable_adv(bool enable, aw_le_advertising_parameters_t *adv_param);

int aw_gatt_set_adv_data(aw_adv_data_t *adv_data);

int aw_gatt_set_local_name(int flag,char *ble_name);

int aw_gatt_set_rsp_data(aw_rsp_data_t *rsp_data);

extern tGattServerCb *gatt_server_cb;

#ifdef __cplusplus
}
#endif

#endif
