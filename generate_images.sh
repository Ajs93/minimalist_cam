#!/bin/bash

DEST_DIR="sw-releases"

rm ./$DEST_DIR/*

cp out/v459-smartcam/image/boot0_sdcard.fex $DEST_DIR/boot0_sdcard.fex
cp out/v459-smartcam/image/boot_package.fex $DEST_DIR/boot_package.fex
cp out/v459-smartcam/image/env.fex $DEST_DIR/env.fex
cp out/v459-smartcam/boot.img $DEST_DIR/boot.fex
cp out/v459-smartcam/rootfs.img $DEST_DIR/rootfs.fex
cp out/v459-smartcam/usr.img $DEST_DIR/usr.fex
